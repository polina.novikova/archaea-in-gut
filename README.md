# Functional assignment of gut-specific archaeal proteins in the human gut microbiome

## About 

The code and files deposited in this repository support the paper "Functional assignment of gut-specific archaeal proteins in the human gut microbiome" by P.V. Novikova _et al_. 


## Workflow

- Scripts for the main steps of analysis are placed in 
[main scripts](https://gitlab.lcsb.uni.lu/polina.novikova/archaea-in-gut/-/tree/main/main%20scripts) and include code for for MAGs processing, taxonomy annotation, protein and smash clustering, gene calling, protein assignment, HMMs building and phylogenetic analysis.
- Scripts for supplementary analysis and plots generation are in [auxiliary scripts](https://gitlab.lcsb.uni.lu/polina.novikova/archaea-in-gut/-/tree/main/auxiliary%20scripts).

## Supplementary materials

The [supplementary materials](https://gitlab.lcsb.uni.lu/polina.novikova/archaea-in-gut/-/tree/main/supplementary%20materials) contains:
- [protein sequences](https://gitlab.lcsb.uni.lu/polina.novikova/archaea-in-gut/-/tree/main/supplementary%20materials/protein%20clusters) of archaeal gut-specific unique and homologous clusters
- [synteny plots](https://gitlab.lcsb.uni.lu/polina.novikova/archaea-in-gut/-/tree/main/supplementary%20materials/synteny%20plots) of archaeal gut-specific unique and homologous clusters


## License

This project is available under the [MIT License](https://gitlab.lcsb.uni.lu/polina.novikova/archaea-in-gut/-/blob/main/LICENSE). 

