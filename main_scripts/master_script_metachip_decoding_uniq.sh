#!/bin/bash -l
#SBATCH -J decoding_u
#SBATCH -o decoding_u_%j.log
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --time=12:00:00
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -p batch


dir_main=/work/projects/archaea_neurodeg/coevolution/hgt_analysis
protein_type=uniq
dir_SCs=/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$protein_type/lists
mkdir -p $dir_main/hgt_genes/$protein_type
dir_target=$dir_main/hgt_genes/$protein_type
export dir_main
export protein_type
export dir_SCs
export dir_target


# rm $dir_target/genomes_in_SCs/*.tsv
mkdir -p $dir_target/genomes_in_SCs
mkdir -p $dir_target/output_prodigal_updated
mkdir -p $dir_target/decoded_metachip

for file in $dir_SCs/*.tsv; do
    cluster_name=$(basename "$file" .tsv)
    echo "-------------------"
    echo "SC $cluster_name running"
    echo "-------------------"
    export cluster_name
    # conda activate prodigal
    # cat $file | while read protein; do
        # grep $protein /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv | cut -f2 | sed 's/.fasta//g' | uniq | while read genome; do
        #     echo "prodigal for $genome"
        #     prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/$genome.fasta -f gff -o $dir_main/prodigal/$genome.gff -q
        # done
        # remove 3 first lines
        # sed -i '/^#/d' $dir_main/prodigal/*gff
        # grep $protein /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv | cut -f2 | sed 's/.fasta//g' | uniq >> $dir_target/genomes_in_SCs/genomes_in_$cluster_name.tsv
    # done
    # conda activate jupyter
    python $dir_main/link_metachip_to_prodigal.py
    echo "-----------------"
    echo "SC $cluster_name done"
    echo "-----------------"
done

cd $dir_target/decoded_metachip
for file in *tsv; do tail -n +2 $file ; done |  sed '1 i\Gene_1\tGene_2\tIdentity\tend_match\tfull_length_match\tdirection\tGene1_contig\tGene2_contig\tGene1_coordinate\tGene2_coordinate\tprotein'  > $dir_main/hgt_genes/decoded_metachip_all_clusters_$protein_type.tsv

