# ------------------------------------------
# RUN MMSEQS2
# ------------------------------------------
cd 
mmseqs createdb /scratch/users/pnovikova/archaea/nr_hq_genomes/*fasta nr_hg_genomes
mmseqs taxonomy nr_hg_genomes /scratch/users/pnovikova/archaea/taxonomy/swissprot_db/swissprot assignments tmpFolder --tax-lineage 1 --lca-mode 3  --tax-output-mode 2 --orf-filter 1
mmseqs createtsv nr_hg_genomes assignments taxonomy_nr_hq.tsv
# # fragment report
# mmseqs taxonomyreport /scratch/users/pnovikova/archaea/taxonomy/swissprot_db/swissprot tmpFolder/latest/orfs_tax krona_report_fragm.html --report-mode 1 # krona
# full report
taxonomyreport /scratch/users/pnovikova/archaea/taxonomy/swissprot_db/swissprot assignments krona_report_all.html --report-mode 1
# kraken (for pavian)
mmseqs taxonomyreport /scratch/users/pnovikova/archaea/taxonomy/swissprot_db/swissprot assignments kraken_report.txt --report-mode 0 # kraken 


# ------------------------------------------
# TAXONOMY 
# ------------------------------------------
# conda activate catbat

# contigs_fasta=$gsa/toy_H_pooled_gsa_anonymous.fasta
# database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/2020-03-04_CAT_database
# taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/2020-03-04_taxonomy

# date

# # for low complexity
# CAT contigs -c $contigs_fasta -d $database_folder -t $taxonomy_folder --force --tmpdir $bmkng/toy_taxonomy/diamond.temp
# # for high complexity use the bottom
# # works 15h on bigmem, -N 1 --ntasks-per-node 2 -c 32
# # run on scratch, /scratch/users/pnovikova/lao_datasets/toy_taxonomy5.0
# cd /scratch/users/pnovikova/lao_datasets/toy_taxonomy/
# CAT contigs -c $contigs_fasta -d $database_folder -t $taxonomy_folder

# CAT add_names -i  $bmkng/toy_low/taxonomy/out.CAT.contig2classification.txt -o $bmkng/toy_low/taxonomy/gsa_contig_taxonomy.txt -t $taxonomy_folder --exclude_scores --only_official

# CAT summarise -c $contigs_fasta -i  $bmkng/toy_low/taxonomy/gsa_contig_taxonomy.txt -o  $bmkng/toy_low/taxonomy/out.CAT.summarise.txt

# date


work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'

dataset=gem
genome_folder=$scratch/data/$dataset/genomes
database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20201123/2020-11-23_CAT_database
# database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/*_database
taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20201123/2020-11-23_taxonomy
# taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/*_taxonomy

CAT bins -b $genome_folder -d $database_folder -t $taxonomy_folder -s fasta -o /scratch/users/pnovikova/archaea/analysis/taxonomy/catbat/$dataset --force 



for f in *.fna; do grep -o '>' $f | wc -l; done | sort -nr | head 
v
cut GCF_014969745.1_ASM1496974v1_genomic.fna -f ">"
cut GCF_014969745.1_ASM1496974v1_genomic.fna -f1,2 -d'>' | head
sed 's/-[^>]*//2g'

awk -v n=2 '/>/{n--}; n > 0'
awk -v n=1 '/>/{n--; if (!n) exit}; {print}'

mkdir renamed
for f in *.fna; do 
	name="$(echo ${f} | cut -d '_' -f1,2)"
	awk -v n=2 '/>/{n--; if (!n) exit}; {print}' $f > renamed/$name.fna
	sed -i "s/>.*/>${name}/" renamed/$name.fna
done

# replaced with the script above
for f in *.fna; do name="$(echo ${f} | cut -d '_' -f1,2)"; sed -i "s/>.*/>${name}/" $f; mv $f $name.fna; done

wget https://portal.nersc.gov/GEM/genomes/fna/$f.fna.gz


work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'
dataset=ncbi
genome_folder=$scratch/data/$dataset/genomes

CAT bins -b $genome_folder -d $database_folder -t $taxonomy_folder -s fasta -f 0.5 --force \
--tmpdir /work/projects/ecosystem_biology/archaea/tmp \
--proteins_fasta $scratch/analysis/taxonomy/catbat/$dataset/$dataset.concatenated.predicted_proteins.faa \
--diamond_alignment $scratch/analysis/taxonomy/catbat/$dataset/$dataset.concatenated.alignment.diamond \
-o $scratch/analysis/taxonomy/catbat/$dataset/$dataset

dataset=ncbi
checkm lineage_wf --tab_table -t 16 -r -x fasta \
/scratch/users/pnovikova/archaea/data/$dataset/genomes \
/scratch/users/pnovikova/archaea/analysis/checkm \
--file /work/projects/ecosystem_biology/archaea/analysis/checkm/$dataset_genomes_quality.log

CAT bins -b $genome_folder -d $database_folder -t $taxonomy_folder -s fasta -f 0.5 --force \
--tmpdir /work/projects/ecosystem_biology/archaea/tmp \
-o $scratch/analysis/taxonomy/catbat/ncbi_new_v/$dataset




work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'
dataset=ncbi
genome_folder=$scratch/data/$dataset/genomes

mkdir -p /scratch/users/pnovikova/archaea/analysis/finch/$dataset
finch sketch $genome_folder/*fasta -o /scratch/users/pnovikova/archaea/analysis/finch/$dataset/



# dir='/work/projects/ecosystem_biology/archaea'
# mkdir -p $dir/analysis/smash/sig
# mkdir -p $dir/analysis/smash/out

# conda activate smash 

# declare datasets=("gut" "ncbi" "gem")

# for dataset in ${datasets[@]}; do
# cd $dir/data/$dataset/genomes/
# for f in *.fasta; do echo $f ; sourmash compute --scaled 1000 -k 31 $f -o $dir/analysis/smash/sig_$dataset/$f.sig ; done
# done

# sourmash index -k 31 $dir/analysis/smash/sbt_08 $dir/analysis/smash/sig/*.sig

# # to analyze in R too
# sourmash compare -p 8 $dir/analysis/smash/sig/*.sig -o $dir/analysis/smash/out/08_cmp --csv $dir/analysis/smash/out/out.csv

# cd $dir/analysis/smash/out

# sourmash plot --pdf --labels $dir/analysis/smash/out/08_cmp  

conda activate smash 

work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'
dataset=gem
genome_folder=$scratch/data/$dataset/genomes

mkdir -p $scratch/analysis/smash/$dataset
mkdir -p $work/analysis/smash/$dataset
mkdir -p $scratch/analysis/smash/$dataset/sig
mkdir -p $scratch/analysis/smash/$dataset/out

# cd $scratch/data/$dataset/genomes/
# for f in *.fasta; do echo $f ; sourmash compute --scaled 1000 -k 31 $f -o $scratch/analysis/smash/$dataset/sig/$f.sig ; done

sourmash index -k 31 $scratch/analysis/smash/$dataset/sbt_08 $scratch/analysis/smash/$dataset/sig/*.sig

# to analyze in R too
sourmash compare -p 16 $scratch/analysis/smash/$dataset/sig/*.sig -o $scratch/analysis/smash/$dataset/out/08_cmp \
--csv $scratch/analysis/smash/$dataset/out.csv

cd $scratch/analysis/smash/$dataset/out

sourmash plot --pdf --labels $scratch/analysis/smash/$dataset/out/08_cmp \
--indices \
--output-dir $scratch/analysis/smash/$dataset



# # EXTRACT NON-SIMILAR GENOMESq
# # in jupyter-notebook, the result is in /work/projects/ecosystem_biology/archaea/analysis/nr_genomesnr_genomes.txt
# # TAXONOMY MADE WITH INITIAL GFFs, only available for GUT and GEM
# dir='/work/projects/ecosystem_biology/archaea'
# cat $dir/analysis/nr_genomes/nr_genomes.txt | rev | cut -c7- | rev | while read f; do
# awk '$1 ~ /'${f}'/' $dir/data/gut/genomes-all_metadata.tsv | cut -f1,19
# awk '$1 ~ /'${f}'/' $dir/data/gem/genome_metadata.tsv | cut -f1,15
# done


work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'

folders=( $scratch/analysis/smash/ncbi/sig/ $scratch/analysis/smash/gut/sig/ $scratch/analysis/smash/gem/sig/ )
for d in "${folders[@]}"; do
	echo $d

for FILE in $(find . -maxdepth 3 -type f -name "*.sig"); do
	echo $FILE
	sourmash index -k 31 $scratch/analysis/smash/all/sbt_08 $FILE
done


# to analyze in R too
sourmash compare -p 16 $scratch/analysis/smash/$dataset/sig/*.sig -o $work/analysis/smash/$dataset/out/08_cmp \
--csv $scratch/analysis/smash/$dataset/out.csv

sourmash plot --pdf --labels $scratch/analysis/smash/$dataset/out/08_cmp \
--indices \
--output-dir $scratch/analysis/smash/$dataset

# cat /work/projects/ecosystem_biology/archaea/analysis/checkm/nr_genomes_quality_selected.tsv | cut -f1 | while read name; do
# for file in nr_genomes/*; do
# if [[ $file == *$name* ]]; then
# 	echo $file 
# 	cp $file nr_hq_genomes/;
# fi
# done
# done


cut -f1 nr-hq-all-genomes.csv | tail -n +2
/scratch/users/pnovikova/archaea/data/nr_hq_genomes/genomes


cat archaeal_genomes.txt | while read f; do echo $f.fna.gz; cp fna/$f.fna.gz archaea/; done


dataset=gut
cut -f1 nr-hq-all-genomes.csv | tail -n +2 | cat | while read f; do echo $f; cp /scratch/users/pnovikova/archaea/data/$dataset/genomes/$f /scratch/users/pnovikova/archaea/data/nr_hq_genomes/genomes/; done



perl  ~/scripts/get_gc_content.pl /scratch/users/pnovikova/archaea/data/ncbi/genomes/*.fasta \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/ncbi_gc_content.txt

perl  ~/scripts/get_gc_content.pl /scratch/users/pnovikova/archaea/data/ncbi/genomes/*.fasta \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/ncbi_gc_content.txt




for f in *.fasta; do name="${f::(-6)}"; echo $name; perl ~/scripts/get_gc_content.pl $f $name.GC.txt; done

perl  ~/scripts/get_gc_content.pl GCF_013402815.1.fasta file.txt
head file.txt  


tail -n +2

tmp && mv tmp HQ_genomes_$dataset.tsv


for f in /scratch/users/pnovikova/archaea/data/ncbi/genomes/*.fasta; do 
name="$(basename "$f")";
name="${name::(-6)}"; 
echo $name; 
done



dataset=gem

mkdir /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset

for f in /scratch/users/pnovikova/archaea/data/$dataset/genomes/*.fasta; do 
name="$(basename "$f")";
name="${name::(-6)}"; 
echo $name; 
perl ~/scripts/get_gc_content.pl $f /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp;
sed -i 's/>//g' /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp; 
tail -n +2 /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp > \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset/$name.txt;
done 

rm /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp

cat /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset/*.txt >> /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_gc_content.tsv

awk -v OFS=$'\t' '{ $1=$1".fasta"; print}' ${dataset}_gc_content.tsv > tmp && mv tmp ${dataset}_gc_content.tsv

sed -i '1s/^/ids\tGCContent\ttotal_count\tG_count\tC_count\tA_count\tT_count\n/' \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_gc_content.tsv






for f in /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/*.fasta; do 
name="$(basename "$f")";
name="${name::(-6)}"; 
echo $name; 
perl ~/scripts/get_gc_content.pl $f /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp;
sed -i 's/>//g' /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp; 
tail -n +2 /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp > \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$name.txt;
done

cat /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/*.txt >> \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/ncbi_gc_content.tsv

sed -i '1s/^/ids\tGCContent\ttotal_count\tG_count\tC_count\tA_count\tT_count\n/' \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/ncbi_gc_content.tsv



/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/test

2012990006_1
3300000215_17

/scratch/users/pnovikova/archaea/data/gem/genomes/3300000215_17.fasta
/scratch/users/pnovikova/archaea/data/gem/genomes/2012990006_1.fasta

>3300000215.a:SI53jan11_120mDRAFT_c1004994  
>3300000215.a:SI53jan11_120mDRAFT_c1004942  
>3300000215.a:SI53jan11_120mDRAFT_c1003738 



/scratch/users/pnovikova/archaea/data/gut/genomes/GUT_GENOME120168.fasta


grep -o '>' GUT_GENOME286611.fasta | wc -l

sed '/^>/d' GUT_GENOME286611.fasta > new.txt

sed '/^>/d' GUT_GENOME286611.fasta | grep -o '>' | wc -l


for f in GUT*.fasta; do
sed '/^>/d' $f > full_$f
grep -o '>' full_$f | wc -l
sed -i "1s/^/>${f}\n/" full_$f
done


perl ~/scripts/get_gc_content.pl test1 test1_gc

perl ~/scripts/get_gc_content.pl GUT_GENOME286660.fasta GUT_GENOME286660_GC_assembly.txt




cp /scratch/users/pnovikova/archaea/data/gut/genomes/GUT_GENOME286575.fasta ./
cp /scratch/users/pnovikova/archaea/data/gut/genomes/GUT_GENOME286611.fasta ./                          
cp /scratch/users/pnovikova/archaea/data/gut/genomes/GUT_GENOME286660.fasta ./  

for f in GUT*.fasta; do
sed '/^>/d' $f > full_$f
sed -i "1s/^/>${f}\n/" full_$f
done


dataset=gut
cd /scratch/users/pnovikova/archaea/data/$dataset/genomes/
for f in *.fasta; do
sed '/^>/d' $f | sed "1s/^/>${f}\n/" > /scratch/users/pnovikova/archaea/data/$dataset/consistent_genomes/$f
done
ls /scratch/users/pnovikova/archaea/data/$dataset/genomes/ | wc -l
ls /scratch/users/pnovikova/archaea/data/$dataset/consistent_genomes/ | wc -l




perl ~/scripts/get_gc_content.pl /scratch/users/pnovikova/archaea/data/gem/genomes/3300029942_15.fasta sep.txt
perl ~/scripts/get_gc_content.pl /scratch/users/pnovikova/archaea/data/gem/consistent_genomes/3300029942_15.fasta consist.txt



dataset=gem

mkdir -p /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_new

for f in /scratch/users/pnovikova/archaea/data/$dataset/consistent_genomes/*.fasta; do 
name="$(basename "$f")";
name="${name::(-6)}"; 
echo $name; 
perl ~/scripts/get_gc_content.pl $f /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp;
sed -i 's/>//g' /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp; 
tail -n +2 /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp > \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_new/$name.txt;
done


ftp://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v1.0/\
all_genomes/MGYG-HGUT-000/MGYG-HGUT-00080/genomes1/GUT_GENOME000009.gff.gz


http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v1.0/\
uhgg_catalogue/MGYG-HGUT-000/MGYG-HGUT-00080/genome/MGYG-HGUT-00080.faa

ftp://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v1.0/\
all_genomes/MGYG-HGUT-021/MGYG-HGUT-02163/genomes1/GUT_GENOME001950.gff.gz

ftp://ftp.ebi.ac.uk//pub/databases/metagenomics/mgnify_genomes/human-gut/v1.0/\
uhgg_catalogue/

sed 's/all_genomes/uhgg_catalogue/g'

sed 's/genomes1/genome/g'

sed 's/\bGUT_[^ ]*/*.faa/'

sed 's/all_genomes/uhgg_catalogue/g' /scratch/users/pnovikova/archaea/data/raw/gut/test.links | sed 's/genomes1/genome/g' | sed 's/\bGUT_[^ ]*/*.faa/' | tail -n +2 > /scratch/users/pnovikova/archaea/data/raw/gut/gut_faa_links.txt

cat /scratch/users/pnovikova/archaea/data/raw/gut/gut_faa_links.txt | while read $f; do echo $f; wget $f; done
 wget $f; done



for f in `cat /scratch/users/pnovikova/archaea/data/raw/gut/gut_faa_links.txt`; do echo $f; wget ${f}; done


sed -n 's/>//p' proteins/GCF_000006175.1_ASM617v2_protein.faa | cut -d ' ' -f1 | head
sed -n 's/>//p' cds/GCF_000006175.1_ASM617v2_translated_cds.faa | cut -d ' ' -f1 | cut -d '_' -f4,5 | head

###### find differing entries in 2 lists:
sort <(sed -n 's/>//p' proteins/GCF_000006175.1_ASM617v2_protein.faa | cut -d ' ' -f1) \
<(sed -n 's/>//p' cds/GCF_000006175.1_ASM617v2_translated_cds.faa | cut -d ' ' -f1 | cut -d '_' -f4,5) \
| uniq -u



sed '1,9d' GCF_904067545.1_T_caminus_IRI35c_genomic.gff 

GCF_000007305.1_ASM730v1_assembly_report.txt

for f in *.txt; do 
  name="$(echo ${f} | cut -d '_' -f1,2)";





  awk -v n=2 '/>/{n--; if (!n) exit}; {print}' $f > /scratch/users/pnovikova/archaea/data/ncbi/genomes/$name.fna; 
  sed -i "s/>.*/>${name}/" /scratch/users/pnovikova/archaea/data/ncbi/genomes/$name.fna; 
done 

grep "RefSeq assembly and GenBank assemblies identical"
grep "Taxid" $f | cut -d ' ' -f12 

cd /scratch/users/pnovikova/archaea/data/ncbi/taxids/assembly_reports
for f in *.txt; do 
  name="$(echo ${f} | cut -d '_' -f1,2)";
  taxid="$(grep "Taxid" ${f} | cut -d ' ' -f12)";
  echo -e "$name\t$taxid" 
done >> /scratch/users/pnovikova/archaea/data/ncbi/taxids/genomes-taxid.csv



for f in *.txt; do 
  grep "Taxid" $f | cut -d ' ' -f12 
done

cd /scratch/users/pnovikova/archaea/data/ncbi/faa/
for f in *.faa; do
name="$(echo ${f} | cut -d '_' -f1,2)"; 
mv "$f" "${name}.faa" 
done




# Compile input files for MANTIS: 
# 1. protein paths:
	# NCBI: 

work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'

dataset=gut
cut -f1 $work/analysis/intermediate_results/nr-hq-all-genomes.csv | tail -n +2 | cat | sed 's/......$//' |\
while read f; do \
echo -e "$f\t"$(ls $scratch/data/${dataset}/faa/${f}.faa)""
done > $work/analysis/intermediate_results/tmp

grep "/scratch" $work/analysis/intermediate_results/tmp > \
$work/analysis/mantis/input_files/${dataset}.nr-hq.proteins.csv


# combine genome id, path to faa, taxid 
awk 'NR==FNR {h[$1] = $2; next} {print $1,$2,$3,h[$1]}' \
$scratch/data/ncbi/taxonomy/genomes-taxid.csv \
$work/analysis/mantis/input_files/ncbi.nr-hq.proteins.csv

	# GUT

GUT_GENOME286660

cd /scratch/users/pnovikova/archaea/data/gut/faa
for f in *.faa; do
	name="$(grep ">" $f | head -1 | cut -d '_' -f1,2| sed 's/>//')"
	mv $f $name.faa
done



# get taxids for GUT
tail genomes-all_metadata.tsv | cut -f1,19 | sed 's/;/\t/g' 

tail genomes-all_metadata.tsv | cut -f1,19 | sed 's/;/\t/g' | sed 's/.__//g'


-rw-r--r--. 1 pnovikova clusterusers 162K Jan 25 11:53 GUT_Archaea_taxonomy.tsv
-rw-r--r--. 1 pnovikova clusterusers  32M Jan 25 11:20 GUT_taxonomy.tsv
-rw-r--r--. 1 pnovikova clusterusers  12K Jan 25 11:59 to extract genomes-tax.ipynb

/scratch/users/pnovikova/archaea/data/gut/taxonomy/genomes-tax.csv


/work/projects/ecosystem_biology/archaea/analysis/prodigal/GUT*proteins


work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'
dataset=gut
cut -f1 $work/analysis/intermediate_results/nr-hq-all-genomes.csv | tail -n +2 | cat | sed 's/......$//' |\
while read f; do \
echo -e "$f\t"$(ls $work/analysis/prodigal/GUT*proteins)""
done > $work/analysis/intermediate_results/tmp

ls $work/analysis/prodigal/GUT*proteins | while read f; do
	echo basename "$f"

for f in $work/analysis/prodigal/GUT*proteins; do
	name="$(basename "$f" | cut -d. -f1)"
	echo -e "$name\t$f"
done > $work/analysis/intermediate_results/tmp




# GEM
work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'

dataset='gem'
cut -f1 $work/analysis/intermediate_results/nr-hq-all-genomes.csv | tail -n +2 | cat | sed 's/......$//' |\
while read f; do \
echo -e "$f\t"$(ls $scratch/data/${dataset}/faa/${f}.faa)""
done > $work/analysis/intermediate_results/tmp

grep "/scratch" $work/analysis/intermediate_results/tmp > \
$work/analysis/mantis/input_files/${dataset}.nr-hq.proteins.csv



cd /scratch/users/pnovikova/archaea/data/raw/gem
grep "d__Archaea" genome_metadata.tsv | cut -f1,15 | sed 's/;/\t/g' | sed 's/.__//g' | tail -n +2 > \
/scratch/users/pnovikova/archaea/data/gem/taxonomy/GEM_taxonomy.tsv




gem_path='/scratch/users/pnovikova/archaea/data/gem/taxonomy/GEM_taxonomy.tsv'
outfile='/work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/gem.taxids.tsv'
read_gtdb_files(gem_path,outfile)



/scratch/users/pnovikova/archaea/data/gem/taxonomy/gut_taxonomy.tsv 






sed '1i\\' unrecognized2ncbi.tsv | sed -e '$a\' | cat | while read f1 f2 ; do
	wrong=${f1}
	right=${f2}
	echo -e "$wrong\t$right"
	grep "$wrong" input_file.tsv | sed "s/$wrong/$right/g"
	sed -i "s/$wrong/$right/g" input_file.tsv
done 


sed -i 's|$work/analysis/mantis/output_tsv|/work/projects/archaeome/output|g' mantis2.sh

# perl -pe "s/,/\tconcoct./g;" 
sed -e '$aMethanosarcina\t224756' 



Could not find taxa ID for Desulfurococcaceae 114380
Could not find taxa ID for Nitrososphaeria 651137
Could not find taxa ID for Methanocullaceae 2194


Could not find taxa ID for Methanotrichaceae 143067
Could not find taxa ID for Methanomethylophilaceae 2194
Could not find taxa ID for Methanothrix 2222


sed -i -e '$aMethanosarcina\t224756' unrecognized2ncbi.tsv
sed -i -e '$aDesulfurococcaceae\t114380' unrecognized2ncbi.tsv
sed -i -e '$aNitrososphaeria\t651137' unrecognized2ncbi.tsv
sed -i -e '$aMethanocullaceae\t2194' unrecognized2ncbi.tsv
sed -i -e '$aMethanotrichaceae\t143067' unrecognized2ncbi.tsv
sed -i -e '$aMethanomethylophilaceae\t2194' unrecognized2ncbi.tsv
sed -i -e '$aMethanothrix\t2222' unrecognized2ncbi.tsv

cd /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files
sed '1i\\' unrecognized2ncbi.tsv | sed -e '$a\' | cat | while read f1 f2 ; do
	wrong=${f1}
	right=${f2}
	echo -e "$wrong\t$right"
	grep "$wrong" split_input/xaj.tsv | sed "s/$wrong/$right/g"
	sed -i "s/$wrong/$right/g" split_input/xaj.tsv
done 


/work/projects/ecosystem_biology/local_tools/mantis
Mantis_Assembler.py

sed -i -e '$aNatronomonas\t2235' unrecognized2ncbi.tsv
sed -i -e '$aThermoplasmataceae\t2301' unrecognized2ncbi.tsv



sed '1i\\' unrecognized2ncbi.tsv | sed -e '$a\' | cat | while read f1 f2 ; do
	wrong=${f1}
	right=${f2}
	echo -e "$wrong\t$right"
	grep "$wrong" split_input/xaf.tsv | sed "s/$wrong/$right/g"
	sed -i "s/$wrong/$right/g" split_input/xaf.tsv
done 


--begin=2021-02-05T15:00:00


sed -i 's|$/work/projects/archaeome/output|/work/projects/archaeome/outputs|g' mantis




sequence similarity clustering

gut_proteins=/work/projects/ecosystem_biology/archaea/analysis/prodigal/GUT*proteins
gem_proteins=/scratch/users/pnovikova/archaea/data/gem/faa/*faa
ncbi_proteins=/scratch/users/pnovikova/archaea/data/ncbi/faa/*faa

/work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/ncbi.nr-hq.proteins.csv





cat mantis/input_files/*nr-hq.proteins.csv | cut -f2 | while read f; do cp $f /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/$(basename "$f");  done 


mmseqs createdb /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/* hr-hq.DB


mmseqs cluster hr-hq.DB hr-hq.DB_clu /scratch/users/pnovikova/archaea/tmp


mmseqs createtsv hr-hq.DB hr-hq.DB hr-hq.DB_clu clustering.tsv



# process mantis output
grep "enzyme_ec:" integrated_annotation.tsv | head
grep "pfam:" integrated_annotation.tsv | head


/work/projects/archaeome/outputs/GCF_900095295.1/integrated_annotation.tsv


for f in /work/projects/archaeome/outputs/*; do
	cd $f;
	cat $f/integrated_annotation.tsv >> /work/projects/archaeome/all_integrated_annotation.tsv;
done


for f in *.faa; do 
name="$(echo $f | sed 's/....$//')";
# awk -v n=2 '/>/{n--; if (!n) exit}; {print}' $f > /scratch/users/pnovikova/archaea/data/ncbi/genomes/$name.fna;	
sed "s/>.*/>${name}/" /scratch/users/pnovikova/archaea/data/ncbi/faa/test/$f; 
done 




awk -v name="$name" 'NR == 1 {fn=FILENAME; sub(/\..*$/, "", fn)} $1 ~ /^>/{$1 = ">name" ++i} 1'  GCF_902813195.1.faa


awk 'BEGINFILE {fn=FILENAME; sub(/\..*$/, "", fn); i=0} $1 ~ /^>/{$1 = ">" fn "_" ++i} 1' $f > tmp && mv tmp $f

for f in *.faa; do 
	echo $f
	awk 'BEGINFILE {fn=FILENAME; sub(/\..*$/, "", fn); i=0} $1 ~ /^>/{$1 = ">" fn "_" ++i} 1' $f > tmp && mv tmp $f
done


awk '{print $0}' file > tmp && mv tmp file


Methanobrevibacter 2172
Nitrososphaerales 1033996
Thermofilum 2268
Micrarchaeota 1801631
Nanoarchaeota 192989
Methanoplasma 1607807
Methanolinea 499551
Methanoregula 395331
Methanospirillum 2202


sed -i -e '$aMethanobrevibacter\t2172' unrecognized2ncbi.tsv
sed -i -e '$aNitrososphaerales\t1033996' unrecognized2ncbi.tsv
sed -i -e '$aThermofilum\t2268' unrecognized2ncbi.tsv
sed -i -e '$aMicrarchaeota\t1801631' unrecognized2ncbi.tsv
sed -i -e '$aNanoarchaeota\t192989' unrecognized2ncbi.tsv
sed -i -e '$aMethanoplasma\t1607807' unrecognized2ncbi.tsv
sed -i -e '$aMethanolinea\t499551' unrecognized2ncbi.tsv
sed -i -e '$aMethanospirillum\t2202' unrecognized2ncbi.tsv



cd /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files
sed '1i\\' unrecognized2ncbi.tsv | sed -e '$a\' | cat | while read f1 f2 ; do
	wrong=${f1}
	right=${f2}
	echo -e "$wrong\t$right"
	grep "$wrong" input_file.tsv | sed "s/$wrong/$right/g"
	sed -i "s/$wrong/$right/g" input_file.tsv
done 
#### 4. split the input file into 10 part to get results quicker
split -n l/10 /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/input_file.tsv
for f in xa*; do mv "$f" "$f".tsv; done
mv xa*.tsv /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/split_input



# ectract pfam and ec values
grep "enzyme_ec:" all_integrated_annotation.tsv | cut -f1,14 | sed "s/enzyme_ec://" | sed '1 i\protein\tec' > functional_annotation_ec.csv


grep "pfam:" all_integrated_annotation.tsv | grep "description:"| grep -v "pfam:DUF" | grep -v "pfam:UPF" | grep -v "pfam:du" | cut -f1,12-20 | awk -F 'pfam:' '{print $2}' | cut -f1 > tmp_pfam
grep "pfam:" all_integrated_annotation.tsv | grep "description:"| grep -v "pfam:DUF" | grep -v "pfam:UPF" | grep -v "pfam:du" | cut -f1 > tmp_proteins

paste tmp_proteins tmp_pfam | sed '1 i\protein\tpfam' > functional_annotation_pfam.csv



sed '63650,63650,$ { $! s: >::g }' infile
sed '63650 s/ //'

sed -e '63650 {
  / //g
}' <


sed -e '63650 {
  / /s/ //g
}' < functional_annotation_ec.csv






/scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/

mmseqs easy-cluster /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/* /work/projects/ecosystem_biology/archaea/analysis/mmseqs_clustering/clustering /work/projects/ecosystem_biology/archaea/analysis/mmseqs_clustering/tmp \
--remove-tmp-files 1 --cluster-mode 1 -s 4 --similarity-type 2 --min-seq-id 0.95




#  - result_rep_seq.fasta: Representatives
#  - result_all_seq.fasta: FASTA-like per cluster
#  - result_cluster.tsv:   Adjacency list





srun -N 2 --ntasks-per-node 2 -c 2 -p gpu -t 06:00:00 --pty bash                      

conda activate jupyter

jupyter notebook --ip $(ip addr show em1 | grep 'inet ' | awk '{print $2}' | cut -d/ -f1) --no-browser


python3 -m notebook

mmseqs cluster --cov-mode 1 -c 0.9 --min-seq-id 0.9 



--remove-tmp-files 1 --cluster-mode 0



mmseqs createdb /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/* proteinsDB
mmseqs linclust proteinsDB clust_0.5DB tmp --cov-mode 1 -c 0.9 --min-seq-id 0.5 --remove-tmp-files 1
mmseqs linclust proteinsDB clust_0.7_0.75DB tmp --cov-mode 1 -c 0.7 --min-seq-id 0.75 --remove-tmp-files 1

mmseqs createtsv proteinsDB proteinsDB clust_0.7_0.75DB clust_0.7_0.75DB.tsv



mmseqs cluster linclust/proteinsDB clust_0.9seqid_0.9c tmp --cov-mode 0 --min-seq-id 0.9 -c 0.9 --sort-results 1 --remove-tmp-files 1
mmseqs createtsv linclust/proteinsDB linclust/proteinsDB clust_0.9seqid_0.9c clustering_0.9seqid_0.9c.tsv
mmseqs createseqfiledb linclust/proteinsDB clust_0.9seqid_0.9c clust_0.9seqid_0.9c_seq
mmseqs result2flat linclust/proteinsDB linclust/proteinsDB clust_0.9seqid_0.9c_seq clustering_0.9seqid_0.9c.fasta




awk '$2 ~ /'Pfam-A'/' all_integrated_annotation.tsv | grep "pfam:" | grep "description:" | grep -v "pfam:DUF" | grep -v "pfam:UPF" | grep -v "pfam:du" | cut -f1,12-20 | awk -F 'pfam:' '{print $2}' | cut -f1 > tmp_pfam

3300013103








# create instruction in python, as well as empty files (which are clusters)
# clean out the protein file to run the script
sed 's/\s.*$//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs_clustering/clustering_0.9seqid_0.9c.fasta | awk '!seen[$0]++' > /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/all_proteins.fasta

# run script ()
cd /work/projects/ecosystem_biology/archaea/analysis/mmseqs_clustering/protein_fastas
perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/clusters_above_threshold.csv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/all_proteins.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 


# calculate protein lengths
perl ~/scripts/calc.contig.lengths.pl /work/projects/ecosystem_biology/archaea/analysis/mmseqs/clustering_0.9seqid_0.9c.fasta > /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/protein_length.tsv
 
muscle -in /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/999966.fasta \
-out /work/projects/ecosystem_biology/archaea/analysis/muscle/999966 -clw -clwstrict -maxiters 1 -diags -sv





##### remove asterisks in protein sequences
sed -i 's/*//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*.fasta 
#### run muscle
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/afa
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/aln

conda activate muscle
for file in /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*.fasta; do 
	muscle -in $file -fastaout /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/afa/$(basename "${file}" .fasta).afa -clwout /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/$(basename "${file}" .fasta).aln -clwstrict
done

#### run clustalw
conda activate clustalw
for file in /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*.aln; do 
	clustalw2 -infile=$file -tree -outputtree=dist -clustering=NJ
done
mv /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*ph /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format
mv /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*dist /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format

mkdir 500_11
mv `ls *ph | head -500` 



# validation
path=/work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation

conda activate muscle
for file in $path/protein_fastas/3clusters.fasta; do 
	muscle -in $file -fastaout $path/alignment/$(basename "${file}" .fasta).afa -clwout $path/alignment/$(basename "${file}" .fasta).aln -clwstrict
done

conda activate clustalw
for file in $path/alignment/3clusters.aln; do 
	clustalw2 -infile=$file -tree -outputtree=dist -clustering=NJ
done





		  .a:gws2
2004178001.a:gws2_d1_0448_1		2004178001_1.faa
2009439000.a:bisonPool14jan08_C9621_1		2009439000_2.faa
2010170002.a:BISONQ_C3631_1		2010170002_1.faa 
2012990006.a:JC3AJCVIAssemblies_1106445189181_1		2012990006_1
2013515001.a:YNP8_C4015_1		2013515001_1.faa




head `ls *faa | head -5`


sed 's/.a.[^_]*[0-9]//g' *faa | grep ">"





head *faa | grep ">" | sed 's/.a.[^_]*[0-9]//g'













head *faa | grep ">" | sed 's/.a.[^_]*[0-9]//g'



head *faa | grep ">" | head *faa | grep ">" | sed 's/.a.[^_]*[a-zA-Z0-9]//g'

_SEDDRAFT
CR_10_Liq_3_inCRDRAFT
UnCtyDRAFT
_TP_S13solDRAFT




3300000526_2.faa
3300000563_111.faa



head 3300000558_46.faa | awk -v IGNORECASE="1" -F'[.]|DRAFT_' '{$2="_";sub(/ +_ +/,"_")} 1'
awk -F'[.]|DRAFT_' '{$2="_";sub(/ +_ +/,"_")} 1'
sed 's/.a.*[a-zA-Z0-9]DRAFT_.[^\_]*[a-zA-Z0-9]//g'


head 3300000526_2.faa | sed 's/.a.*[a-zA-Z0-9]DRAFT_.*[^_]_[a-zA-Z0-9]//g'

sed 's/.a.DRAFT[^_]*[0-9]//g'



3300000526.a:P_A23_Liq_2_FmtDRAFT_1000944_2
		  .a:P_A23_Liq_2_FmtDRAFT
3300000526_1000944_2






delete: 
.a:PaHGMunAill
Sequence
ConsensusfromContig
.a:rumenHiSeq_NODE
MB_Assembly_NODE


head *faa | grep ">" | awk 'BEGINFILE {fn=FILENAME; sub(/\..*$/, "", fn); i=0} $1 ~ /^>/{$1 = ">" fn "_" ++i} 1' 



head *faa | grep ">" | awk 'BEGINFILE {fn=FILENAME; sub(/\..*$/, "", fn); i=0} $1 ~ /^>/{$1 = ">" fn "_" ++i} 1' 



head *faa | grep ">" | sed "1s/^/>${f}\n/"
|  






head *faa | grep ">" | sed 's/.a.[^_]*[0-9]//g' | awk -F'[.]|[dD][rR][aA][fF][tT]_' '{$2="_";sub(/ +_ +/,"_")} 1'


head *faa | grep ">" | awk -v IGNORECASE="1" -F'[.]|DRAFT_' '{$2="_";sub(/ +_ +/,"_")} 1'

sed 's/.a.[^_]*[0-9]//g' | 






grep ">" *faa | awk -v IGNORECASE="1" -F'[.]|DRAFT_' '{$2="_";sub(/ +_ +/,"_")} 1' | sed 's/.a.[^_]*[0-9]//g' 

cp `ls *faa | head -5` test/ 

for f in 2*.faa; do
   awk 'BEGINFILE {fn=FILENAME; sub(/\..*$/, "", fn)} $1 ~ /^>/{$1 = ">" fn "_" ++i} 1' "$f" > _tmp && mv _tmp "$f"
done



archaea/data/gem/faa/
archaea/data/gem/faa/

split -n l/9 /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/input_file_gem.tsv
for f in xa*; do mv "$f" "$f".tsv; done
mv xa*.tsv /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/split_input_gem





mantis_9_gem.sh


#!/bin/bash -l                                                                                                 
#SBATCH -J mantis8                                       
#SBATCH --time=12:00:00                                   
#SBATCH --mem=100GB                                       
#SBATCH -p batch                                          
#SBATCH -o mantis_8_%j.log                                
                                                          
                                                          
conda activate mantis_env      X                           
                                                          
work='/work/projects/ecosystem_biology/archaea'                                                                
                       
date                                                                                                           
                                                                               
python /work/projects/ecosystem_biology/local_tools/mantis run_mantis \
-t $work/analysis/mantis/input_files/split_input_gem/xah.tsv \
-o /work/projects/archaeome/outputs_gem                                          
                                                                       
date      


#!/bin/bash -l                                                                                                 
#SBATCH -J mantis9                                        
#SBATCH --time=12:00:00                                   
#SBATCH --mem=100GB                                       
#SBATCH -p batch                                          
#SBATCH -o mantis_9_%j.log                                
                                                          
                                                          
conda activate mantis_env                                 
                                                          
work='/work/projects/ecosystem_biology/archaea'                                                                
                       
date                                                                                                           
                                                                               
python /work/projects/ecosystem_biology/local_tools/mantis run_mantis \
-t $work/analysis/mantis/input_files/split_input_gem/xai.tsv \
-o /mnt/lscratch/users/pnovikova/outputs_gem                                          
                                                                       
date      




/work/projects/archaeome  2  
/mnt/lscratch/users/pnovikova/  1


/mnt/lscratch/users/pnovikova/outputs_gem_

/work/projects/archaeome/outputs_gem_


conda activate jupyter

jupyter notebook --ip $(ip addr show em1 | grep 'inet ' | awk '{print $2}' | cut -d/ -f1) --no-browser


# this one is wrong
# sed 's/\s.*$//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/clustering_0.9seqid_0.9c.fasta | awk '!seen[$0]++'

sed 's/#.*//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/clustering_0.9seqid_0.9c.fasta
printf "%s\n" | sort -u


cat  /work/projects/ecosystem_biology/archaea/analysis/mmseqs/clustering_0.9seqid_0.9c.fasta | sort -u


sort -k1 | uniq -w 32 -d

sed 's/\s.*$//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/clustering_0.9seqid_0.9c.fasta | sed 's/#.*//' |awk '$0==b{next}{b=$0;print}' | head



sed 's/\s.*$//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/clustering_0.9seqid_0.9c.fasta | awk '$0==b{next}{b=$0;print}' | sed 's/*//g' | head


# run kalign
cat /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/protein_fastas/all_proteins_in_clusters.fasta  | kalign -f fasta > /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/alignment/all_proteins_in_clusters.afa


cp `ls *faa | head -5`


kalign `ls /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*fasta | head -100` -f fasta > combined100.afa


python ../multi_alignment.ploter.py plot --align genes.muscle.clwstrict.aln --formats aln --types nuc --prefix $pre
convert  $pre.svg $pre.png




python pymsaploter/multi_alignment.ploter.py plot --align ../alignment/aln/21887.aln --formats aln


# build phylogeny for the entire protein dataset
conda activate mafft
mafft --clustalout --thread -1 phylogeny/validation/protein_fastas/all _proteins_in_clusters.fasta > phylogeny/validation/alignment/all_proteins_in_clusters.aln  
conda activate clustalw
clustalw2 -infile=all_proteins_in_clusters.aln -tree -outputtree=dist -clustering=NJ                               



#!/bin/bash -l                                                                                                 
#SBATCH -J clustalw                                        
#SBATCH --time=24:00:00                                                                          
#SBATCH -p bigmem                                          
#SBATCH -o clustalw_%j.log   



/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/all_proteins.fasta






for file in /scratch/users/pnovikova/archaea/data/ncbi/faa/test/*faa; do
genome="$(basename "${file}" .faa)"
protein="$(grep ">" $file |sed 's/ .*//g'| sed 's/>//g')"
echo $protein
done | head






cd /scratch/users/pnovikova/archaea/data/ncbi/faa/
for file in *faa; do
awk -F'>' '
FNR==1{
  match(FILENAME,/.*\./)
  file=substr(FILENAME,RSTART,RLENGTH-1)
}
/^>/{
  printf ("%s\t%s\n", $2, file)
}
' $file
done >> /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/proteins_in_genomes.tsv


sed -i '1 i\protein\tgenome' /scratch/users/pnovikova/archaea/data/ncbi/faa/proteins_in_genomes.tsv




# make lists of protein in genomes 
NCBI - "/scratch/users/pnovikova/bacteria/data/ncbi/faa/*faa" 
GEM - "/scratch/users/pnovikova/bacteria/data/gem/proteins_gut/*faa"
GUT - "/work/projects/ecosystem_biology/archaea/analysis/prodigal/GUT*proteins"

declare paths=("/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/")

for path in ${paths[@]}; do
cd $path
for file in *; do
awk -F'>' '
FNR==1{
  match(FILENAME,/.*\./)
  file=substr(FILENAME,RSTART,RLENGTH-1)
}
/^>/{
  printf ("%s\t%s\n", $2, file)
}
' $file
done 
done >> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_bacteria.tsv

sed -i 's/.fasta//g' /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/proteins_in_genomes.tsv
sed -i '1 i\protein\tgenome' /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/proteins_in_genomes.tsv







for f in /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*fasta; do
	awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' $f | awk '/^>/ { getline seq } length(seq) >= 30 { print $0 "\n" seq }' > tmp && mv tmp $f
done

find -type f -empty -delete




clustalo -i my-in-seqs.fa -o my-out-seqs.fa -v --distmat-out=output_mat.txt






clustalo -i /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/999943.fasta -o 999943.fasta -v --distmat-out=output_mat.txt


conda activate clustalw
for file in /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*.fasta; do 
	clustalo -i $file --distmat-out /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat --full
	awk -v OFS="\t" '$1=$1' /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat | tail -n +2  > tmp && mv tmp /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat
done





# check how many proteins do not have Pfam annotation
grep "pfam:DUF" | grep -v "pfam:UPF" | grep -v "pfam:du" 




grep "pfam:" all_integrated_annotation.tsv | grep -e "pfam:DUF" -e "pfam:UPF" | uniq -u | wc -l



cat /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/protein_clusters_over0.2_dist.csv | cut -f1 | while read f; do 

while read f; do cp $f /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/$(basename "$f");  done 

/work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/

analysis/phylogeny/alignment/aln/999943.aln

hmmbuild hmms/999943.hmm phylogeny/alignment/aln/999943.aln

conda activate hmmer
cat /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/protein_clusters_over0.2_dist.csv | cut -f1 | tail -n +2 | while read f; do 
	hmmbuild /work/projects/ecosystem_biology/archaea/analysis/hmms/$f.hmm /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/$f.aln 
done

 sort | uniq -c | sort -nr



# ## Make phylogeny for same proteins but combined (to see their phylogenetic relations in one tree)
# ##### extract_selective_clusters.ipynb, instruction_all_proteins.csv is the output.

# cd /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas
# perl -we '
#     open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/instruction_all_proteins.csv" or die $!;
#     my %where = map split, <$instruction>;
#     open my $contents, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/all_proteins.fasta" or die $!;
#     my $out;
#     my $unknown = "file_unknown";
#     my %created;
#     while (<$contents>) {
#         open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
#              $where{$1} // $unknown if /^>(.*)/;
#         print {$out} $_;
#     }' 

# ##### give extension to all files
# mv all_proteins all_proteins_in_clusters.fasta
# mv all_proteins_in_clusters.fasta /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/protein_fastas/

# ### run MUSCLE

# ##### remove asterisks in protein sequences
# sed -i 's/*//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/all_proteins_in_clusters.fasta 

# # muscle cannot process many sequences, so we split into files of ~10k sequences
# split -n l/7 /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/protein_fastas/all_proteins_in_clusters.fasta 
# for f in xa*; do mv "$f" "$f".fasta; done

# #### run muscle
# conda activate muscle
# for file in /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/protein_fastas/xa*fasta; do 
# muscle -in $file -fastaout /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/alignment/$(basename "${file}" .fasta).afa -clwout /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/alignment/$(basename "${file}" .fasta).aln -clwstrict -maxiters 1 
# done


# #### run clustalw
# conda activate clustalw
# for file in /work/projects/ecosystem_biology/archaea/analysis/phylogeny/validation/alignment/xa*aln; do 
# clustalw2 -infile=$file -tree -outputtree=dist -clustering=NJ
# done


sample=1233_9
hmmfile=/work/projects/ecosystem_biology/archaea/analysis/hmms/2384126.hmm 

hmmsearch -o $sample.txt $hmmfile /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/annotation/prokka.faa



conda activate hmmer
# Hmd search
# condition=RBD
# condition=Ctrl
condition=PD
hmmfile=/work/projects/ecosystem_biology/archaea/analysis/hmms/2384126.hmm 
cat ${condition}_samples.tsv | while read sample; do 
	hmmsearch --tblout $condition/$sample.txt -E 0.05 $hmmfile /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/annotation/prokka.faa
done


conda activate hmmer
# Hmd search
condition=RBD
# condition=Ctrl
# condition=PD
hmmfile=/work/projects/ecosystem_biology/archaea/analysis/hmms/2384126.hmm 
cat ${condition}_samples.tsv | while read sample; do 
	hmmsearch -o $condition/${sample}_full.txt -E 0.05 $hmmfile /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/annotation/prokka.faa
done



 hmmsearch --tblout 1236_3_tblout.txt -E 0.05 $hmmfile /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1236_3/run1/Analysis/annotation/prokka.faa

H2-forming N5,N10-methylene-tetrahydromethanopterin dehydrogenase
PF03201


sed '3iACC   PF03201' /work/projects/ecosystem_biology/archaea/analysis/hmms/2384126.hmm | sed '4iDESC  H2-forming N5,N10-methylene-tetrahydromethanopterin dehydrogenase' > tmp && mv tmp/work/projects/ecosystem_biology/archaea/analysis/hmms/2384126.hmm




# fetch protein names from each prokka.file 
grep ">" /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1232_7/run1/Analysis/annotation/prokka.faa | sed 's/>//' | awk -F ' ' '{print $1}' > 1232_7




for f in 1232_7; do sed "s/$/\t$f/" $f; done | head

condition=RBD
# condition=Ctrl
# condition=PD
cat ${condition}_samples.tsv | while read sample; do 
	grep ">" /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/${sample}/run1/Analysis/annotation/prokka.faa | sed 's/>//' | awk -F ' ' '{print $1}' > proteins_in_prokka_files/$sample
	cd proteins_in_prokka_files
	for f in $sample; do sed -i "s/$/\t$f/" $f; done
	cd ..
done



sed 's/kegg_ko://g' seq_annotation.tsv | sed 's/description://g' > test.tsv



awk '/kegg_ko:/ && /description:/'



cat /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/clusters_filtered_annot_and_score.tsv | while read sample; do
	cp /work/projects/ecosystem_biology/archaea/analysis/hmms/${sample}.hmm /work/projects/ecosystem_biology/archaea/analysis/hmm_filtered/${sample}.hmm
done




/scratch/users/pnovikova/refinement_old/benchmarking/low/taxonomy/



/scratch/users/pnovikova/refinement_old/benchmarking/low/taxonomy/gsa_contig_taxonomy.txt





for bin in !(unbinned).fa; do
grep ">" $bin | wc -l
done


for bin in !(unbinned).fa; do grep ">" $bin | wc -l; done | awk '{s+=$1} END {print s}'  


awk '{s+=$1}END{print "ave:",s/NR}' RS=" " 

/scratch/users/pnovikova/binning_refinement/benchmarking/low


mv /scratch/users/pnovikova/binning_refinement/data/benchmarking/high/* ./  

conda activate prodigal
prodigal -i /scratch/users/pnovikova/binning_refinement/benchmarking/low/data/S_S001__genomes_30__insert_180_gsa_anonymous.fasta -f gff \
> /scratch/users/pnovikova/binning_refinement/benchmarking/low/analysis/gene_prediction/genes.gff

conda activate hmmer


/scratch/users/pnovikova/binning_refinement/benchmarking/low/analysis/taxonomy/

- taxonomy ok

/scratch/users/pnovikova/binning_refinement/benchmarking/low/analysis/features/
- features ok 



conda activate catbat


contigs_fasta=/scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_anonymous.fasta
database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/2020-03-04_CAT_database
taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/2020-03-04_taxonomy
bins_path=/scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins
tmp_dir=/work/projects/archaeome
output=/scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/taxonomy/

CAT bins -b $bins_path -d $database_folder -t $taxonomy_folder -s fa -o $output --tmpdir $tmp_dir 
CAT add_names -i $output/out.BAT.bin2classification.txt -o $output/bins_taxonomy.txt -t $taxonomy_folder --exclude_scores --only_official




unbinned_contigs=/scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins/unbinned.fa

awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' $unbinned_contigs | \
awk '/^>/ { getline seq } length(seq) >= 2500 { print $0 "\n" seq }' | awk -F\> 'NF>1{print $2}' | sed '1 i\contigID' > ../../analysis/contigs.unbinned.txt






awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' $unbinned_contigs | \
awk '/^>/ { getline seq } length(seq) >= 2500 { print $0 "\n" seq }' | awk -F\> 'NF>1{print $2}' | sed '1 i\contigID'





conda activate prodigal 

prodigal -f gff -a /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_prediction/prodigal.faa \
-d /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_prediction/prodigal.fna \
-p meta \
-o /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_prediction/prodigal.gff.tmp \
-i /scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_anonymous.fasta-q 2> /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_prediction/prodigal.err.log \
> /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_prediction/prodigal.out.log




conda activate hmmer 
db=/scratch/users/pnovikova/binning_refinement/essential.hmm
analysis=/scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/

hmmsearch --noali --notextw --tblout $analysis/essential_genes/prodigal.faa_essential.hmmscan $db prodigal.faa >/dev/null



/scratch/users/pnovikova/binning_refinement/hmmscan_addBest2gff.pl \
-h $analysis/essential_genes/prodigal.faa_essential.hmmscan \
-a $analysis/gene_prediction/prodigal.gff.tmp \
-n $db --cut_tc \
-o $analysis/essential_genes/essential_genes.gff \
-g $(cat $analysis/gene_prediction/prodigal.gff.tmp | wc -l)




#######DEFAULT PATHS#######
#You can add custom paths here, otherwise they will be set to default values
#uniprot_folder=    
#go_obo_folder=                                                                           
#ncbi_dmp_path_folder=                                                                         
#default_hmms_folder=                                                                    
#NOGT_hmm_folder=                                                                
#pfam_hmm_folder=                                                   
#kofam_hmm_folder=                                                                                                        
#dbcan_hmm_folder=                                                        
#tigrfam_hmm_folder=                                                
#uniprot_nlp_folder=/work/projects/ecosystem_biology/local_tools/mantis/Resources/Uniprot/
#go_obo_nlp_folder=/work/projects/ecosystem_biology/local_tools/mantis/Resources/Gene_Ontology/
#ncbi_dmp_path_folder=/work/projects/ecosystem_biology/local_tools/mantis/Resources/NCBI/
nog_hmm_folder=NA
pfam_hmm_folder=/mnt/isilon/projects/ecosystem_biology/data/hmm/pfam
kofam_hmm_folder=NA
tigrfam_hmm_folder=/mnt/isilon/projects/ecosystem_biology/data/hmm/tigrfam
ncbi_hmm_folder=NA




/scratch/users/pnovikova/binning_refinement/benchmarking/low/analysis/essential_genes/mantis



#!/bin/bash -l
#SBATCH -J bathigh
#SBATCH -o bathigh_%j.log
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --time=2-00:00:00
#SBATCH -p bigmem

#!/bin/bash -l
#SBATCH -J bat
#SBATCH -o bat_%j.log
#SBATCH --time=2-00:00:00
#SBATCH --mem=100GB
#SBATCH -p batch


conda activate catbat

contigs_fasta=/scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_anonymous.fasta
database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/2020-03-04_CAT_database
taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20200304/2020-03-04_taxonomy                               
bins_path=/scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins                                      
tmp_dir=/work/projects/archaeome                                                                               
output=/scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/taxonomy/
                                                                                       
CAT bins -b $bins_path -d $database_folder -t $taxonomy_folder -s fa -p $output/out.BAT.concatenated.predicted_proteins.faa --force --block_size 1.5
CAT add_names -i out.BAT.bin2classification.txt -o bins_taxonomy.txt -t $taxonomy_folder --exclude_scores --only_official
              




/scratch/users/pnovikova/binning_refinement/benchmarking/low/analysis/essential_genes/mantis


grep "is_essential_gene:True" /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/essential_genes/mantis/integrated_annotation.tsv | cut -f1,4  | tr '_' '\t' | cut -f1,3 | sed '1 i\contigID\tSCG' \
> /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/essential_genes/contigs2ess_genes.tsv


#!/bin/bash -l                                                                                                                        
#SBATCH -J cat                                                                                                           
#SBATCH -o cat_%j.log                                          
#SBATCH --time=05:00:00               
#SBATCH --mem=50GB                                                                                                                   
#SBATCH -p batch                                                                                                                      
                                                                                                                                      
                                                                                                                                      
conda activate catbat                                                                                                                 

dataset=high

contigs_fasta=/scratch/users/pnovikova/binning_refinement/benchmarking/$dataset/data/*_anonymous.fasta                         
database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20210107/*_CAT_database                                                
taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20210107/*_taxonomy                                                    
bins_path=/scratch/users/pnovikova/binning_refinement/benchmarking/$dataset/binning/bins                                                  
tmp_dir=/work/projects/archaeome                                                                                                      
output=/scratch/users/pnovikova/binning_refinement/benchmarking/$dataset/analysis/taxonomy

CAT bins -b $bins_path -d $database_folder -t $taxonomy_folder -s fa -p $output/out.BAT.concatenated.predicted_proteins.faa -a $output/out.BAT.concatenated.alignment.diamond --force --block_size 1.5
CAT add_names -i out.BAT.bin2classification.txt -o bins_taxonomy.tsv -t $taxonomy_folder --exclude_scores --only_official --force            
                                                                                                                                      
sed -i 's/# bin/bin/g' bins_taxonomy.tsv



CAT contigs -c $contigs_fasta -d $database_folder -t $taxonomy_folder --force --block_size 1.5
CAT add_names -i  out.CAT.contig2classification.txt -o gsa_contig_taxonomy.txt -t $taxonomy_folder --exclude_scores --only_official --force






dataset=high                                                            
                                                                        
contigs_fasta=/scratch/users/pnovikova/binning_refinement/benchmarking/$dataset/data/*_anonymous.fasta
database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20210107/*_CAT_database
taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20210107/*_taxonomy  
bins_path=/scratch/users/pnovikova/binning_refinement/benchmarking/$dataset/binning/bins
tmp_dir=/work/projects/archaeome                                                
output=/scratch/users/pnovikova/binning_refinement/benchmarking/$dataset/analysis/taxonomy


CAT contigs -c $contigs_fasta -d $database_folder -t $taxonomy_folder -p $output/out.CAT.predicted_proteins.faa -a $output/out.CAT.alignment.diamond --force --block_size 1.5



perl -we '
    open my $instruction, "<", "/home/users/pnovikova/reserach_project/benchmarking/low/binning/new_bins/new_bins_contig2bin.csv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/scratch/users/pnovikova/benchmarking_datasets/toy_low/S_S001__genomes_30__insert_180_gsa_anonymous.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }'
rm file_unknown




/scratch/users/pnovikova/binning_refinement/repo/jupyter\ notebooks




conda activate checkm
checkm lineage_wf -t 64 -r -x fa /scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins/ /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/checkm/dastool/ --individual_markers --tab_table --tmpdir /work/projects/archaeome/tmp/





for file in `ls dastool/`; do
  paste dastool/${file} refined/*${file}* > ${file}_refined
done






for file in `ls refined/`; do
  paste refined/${file} dastool/*${file}* > ref_${file}
done




rename "s/.*_//"  *


rename "s/.*_//" ref_metabat.193.fa

awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' metabat.193.fa > tmp && mv tmp metabat.193.fa


for file in `ls refined_bins/`; do
  cat refined_bins/${file} dastool/*${file}* > ref_${file}
done


# create new refined bins

rename "s/.*_//" ref_metabat.193.fa

# extend lines in bin fastas
for file in /scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins/*; do
	echo $file
	awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' $file > tmp && mv tmp $file
done


cd /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/contigs2add/fastas/

for file in `ls /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/contigs2add/fastas`; do
  cat /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/contigs2add/fastas/${file} /scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins/*${file}* > /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/refined_bins/ref_${file}
done


# checkM on new bins

sed '1,/Test XXX/d' file*

sed -n -E -e '/Strain heterogeneity/,$ p' 



for file in `ls /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/checkm/CheckM_*`; do
echo $file
sed -n -E -e '/Strain heterogeneity/,$ p' $file > /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/checkm/tmp && mv tmp $file
done


for file in `ls /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/checkm/CheckM_*`; do
echo $file
sed -n -E -e '/Strain heterogeneity/,$ p' $file > /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/checkm/tidy_$(basename "${file}" .log).txt
done



gsa file - /scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_mapping.tsv


@Version:0.9.1
@SampleID:bench_low

sed 's/#anonymous_gsa_id/@@SEQUENCEID/g'
sed 's/genome_id/BINID/g' 
sed 's/tax_id/TAXID/g' 


sed '1 i\@Version:0.9.1\n@SampleID:bench_low\n' 


cut -f1-3 /scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_mapping.tsv | sed 's/#anonymous_gsa_id/@@SEQUENCEID/g' | sed 's/genome_id/BINID/g' | sed 's/tax_id/TAXID/g' | sed '1 i\@Version:0.9.1\n@SampleID:bench_high\n'



# before running amber
# GSA
cd /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/amber
cut -f1-3 /scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_mappin
g.tsv | sed 's/#anonymous_gsa_id/@@SEQUENCEID/g' | sed 's/genome_id/BINID/g' | sed 's/tax_id/TAXID/g' | sed '1 i\@Version:0.9.1\n@SampleID:bench_high\n' > gold_standard_prev.tsv 


conda activate jupyter
fasta=/scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_anonymous.fasta
gsa=/scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/amber/gold_standard_prev.tsv 
python ~/tools/AMBER/src/utils/add_length_column.py -g ${gsa} -f ${fasta} > gold_standard_bioboxes.tsv


# actual binning
paths=/scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins/*
python3 ~/tools/AMBER/src/utils/convert_fasta_bins_to_biobox_format.py ${paths} -o dastool_binning_bioboxes.tsv

sed -i 's/_SAMPLEID_/bench_high\n/g' dastool_binning_bioboxes.tsv


# run amber

amber.py -g gold_standard_bioboxes.tsv \
-l "dastool" \
-p 1 \
--ncbi_nodes_file ~/tools/AMBER/nodes.dmp \
-o /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/amber \
dastool_binning_bioboxes.tsv

# create folder with dastool bins + refined 
mkdir all_bins_w_refined
cp /scratch/users/pnovikova/binning_refinement/benchmarking/high/binning/bins/* all_bins_w_refined/
cd refined_bins/

# remove ref_ from each filename
for fn in *.fa; do newfn=$(echo $fn | cut -c 5-); mv $fn $newfn; done

# update all_bins_w_refined/
cp refined_bins/* all_bins_w_refined/ 

# put ref_ to each 
for f in *; do mv "$f" ref_"$f"; done

# run amber again
cd /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/amber

paths=/scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/all_bins_w_refined/*
python3 ~/tools/AMBER/src/utils/convert_fasta_bins_to_biobox_format.py ${paths} -o refined_binning_bioboxes.tsv

sed -i 's/_SAMPLEID_/bench_high\n/g' refined_binning_bioboxes.tsv

# run amber again
amber.py -g gold_standard_bioboxes.tsv \
-l "dastool, refined" \
-p 1 \
--ncbi_nodes_file ~/tools/AMBER/nodes.dmp \
-o /scratch/users/pnovikova/binning_refinement/benchmarking/high/refinement/amber \
dastool_binning_bioboxes.tsv refined_binning_bioboxes.tsv





grep "Archaea" archaea/data/raw/gem/genome_metadata.tsv | awk '{print $16}' | uniq | sort | uniq

dataset=gem
cd /scratch/users/pnovikova/archaea/data/$dataset/genomes/
for f in *.fasta; do
sed '/^>/d' $f | sed "1s/^/>${f}\n/" > /scratch/users/pnovikova/archaea/data/$dataset/consistent_gut_bacteria_genomes/$f
done


new list 
ls /scratch/users/pnovikova/archaea/data/gut/genomes_bacteria/ | cut -d. -f1 > have.txt

old list 
grep "d__Bacteria" /scratch/users/pnovikova/archaea/data/raw/gut/genomes-all_metadata.tsv | cut -f1,24 > all.txt


comm -23 f1.txt f2.txt

comm -23 all.txt have.txt




BAXCTERIA 

gem - 15841
/scratch/users/pnovikova/archaea/data/gem/consistent_genomes_bacteria_gut

gut - not there yet
/scratch/users/pnovikova/archaea/data/gut/genomes_bacteria

TO /work/projects/archaeome/coevolution/data/bacteria/


ARCHAEA 
gem
/work/projects/archaeome/coevolution/data/archaea/

gut 
/scratch/users/pnovikova/archaea/data/gut/consistent_genomes/ 


TO /work/projects/archaeome/coevolution/data/archaea/

tar -czvf 


tar -czvf /scratch/users/pnovikova/data/GEM_collection.tar.gz /scratch/users/pnovikova/archaea/data/gem/consistent_genomes_bacteria/*fasta


/scratch/users/pnovikova/data/GEM/faa/





find "$(pwd)" 

find "$(archaea/data/gut/genomes_bacteria/)"

sed -n '/##FASTA/,$p' GUT_GENOME000001





conda activate mmseqs2
mmseqs createdb /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/* proteinsDB
mmseqs linclust proteinsDB clust_0.9seqid_0.9c tmp --cov-mode 0 --min-seq-id 0.9 -c 0.9 --sort-results 1 --remove-tmp-files 1
mmseqs createtsv proteinsDB proteinsDB clust_0.9seqid_0.9c clustering_0.9seqid_0.9c.tsv
mmseqs createseqfiledb proteinsDB clust_0.9seqid_0.9c clust_0.9seqid_0.9c_seq
mmseqs result2flat proteinsDB proteinsDB clust_0.9seqid_0.9c_seq clustering_0.9seqid_0.9c.fasta


                                           
conda activate mmseqs2

cd /work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2

mmseqs createdb /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/* proteinsDB
mmseqs linclust proteinsDB clust_0.9seqid_0.9c tmp --cov-mode 0 --min-seq-id 0.9 -c 0.9 --sort-results 1 --remove-tmp-files 1
mmseqs createtsv proteinsDB proteinsDB clust_0.9seqid_0.9c clustering_0.9seqid_0.9c.tsv
mmseqs createseqfiledb proteinsDB clust_0.9seqid_0.9c clust_0.9seqid_0.9c_seq
mmseqs result2flat proteinsDB proteinsDB clust_0.9seqid_0.9c_seq clustering_0.9seqid_0.9c.fasta

/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/
find -type f -name '*'  | wc -l


cd /scratch/users/pnovikova/archaea/data/gut/genomes_bacteria
for f in *gff; do
	sed -n '/##FASTA/,$p' $f | tail -n +2 > /scratch/users/pnovikova/bacteria/data/genomes_gut/$(basename "${f}" .gff).fasta
done
cd /scratch/users/pnovikova/bacteria/data/genomes_gut/
ls GUT* | wc -l


# TO RUN MANTIS: 
1. compose taxonomy file for: 
	- archaea 
	cd /work/projects/ecosystem_biology/archaea/coevolution/data

	ls /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/ > list_genomes_archaea_gut.txt
	cut -d. -f1 list_genomes_archaea_gut.txt > tmp && mv tmp list_genomes_archaea_gut.txt

/scratch/users/pnovikova/archaea/data/gut/taxonomy/gut_taxonomy.tsv
/scratch/users/pnovikova/archaea/data/gem/taxonomy/gem_taxonomy.tsv

2. compose genome to protein path: 
	- archaea 
for f in /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/*; do
	name="$(basename "$f" | cut -d. -f1)"
	echo -e "$name\t$f"
done > /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/input/tmp_id2faa.tsv


for f in /scratch/users/pnovikova/bacteria/data/genomes_gut/GUT_GENOME{015000..286997}.fasta; do
	echo $f
	prodigal -i $f -a /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/$(basename "$f").proteins -f gff; done

!(unbinned).fa

cd /work/projects/ecosystem_biology/archaea/coevolution/

cut -f1 analysis/intermediate_results/reduced_GUT_catalogue.csv | cat | while read genome; do
	prodigal -i /scratch/users/pnovikova/bacteria/data/genomes_gut/${genome}.fasta -a /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/${genome}.proteins -f gff;
done


GUT - `/scratch/users/pnovikova/archaea/data/gut/proteins/*.proteins` - 1162 - by me
GEM - `/scratch/users/pnovikova/archaea/data/gem/proteins/*.faa` - 3038 - official
NCBI - `/scratch/users/pnovikova/archaea/data/ncbi/proteins/*.faa` - 372 - official



/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/archaea_all_known_proteins.tsv



first copy all archaeal proteins (GEM, GUT, NCBI) to working dir: 
```
cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/archaea_all_known_proteins.tsv | while read f; do
	cp $f /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/all_known/
done
```

Run mmseqs2: 

```
conda activate mmseqs2

cd /work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/archaea_all_known

mmseqs createdb /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/all_known/* proteinsDB
mmseqs linclust proteinsDB clust_0.9seqid_0.9c tmp --cov-mode 0 --min-seq-id 0.9 -c 0.9 --sort-results 1 --remove-tmp-files 1
mmseqs createtsv proteinsDB proteinsDB clust_0.9seqid_0.9c clustering_0.9seqid_0.9c.tsv
mmseqs createseqfiledb proteinsDB clust_0.9seqid_0.9c clust_0.9seqid_0.9c_seq
mmseqs result2flat proteinsDB proteinsDB clust_0.9seqid_0.9c_seq clustering_0.9seqid_0.9c.fasta
```



cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/
find -type f -name '*'  | wc -l


/scratch/users/pnovikova/archaea/data/ncbi/gffs/GCF_902387285.1_UHGG_MGYG-HGUT-02456_genomic.gff - NZ_LR699000 - GCF_902387285.1
/scratch/users/pnovikova/archaea/data/ncbi/gffs/GCF_902384015.1_UHGG_MGYG-HGUT-02164_genomic.gff - NZ_LR698975 - GCF_902384015.1
/scratch/users/pnovikova/archaea/data/ncbi/gffs/GCF_902383905.1_UHGG_MGYG-HGUT-02160_genomic.gff - NZ_LR698974 - GCF_902383905.1



Wed Jun  2 21:30:50 CEST 2021
Thu Jun  3 03:37:20 CEST 2021

sed -i 's/d__//g' tmp_tax_gut
sed -i 's/p__//g' tmp_tax_gut
sed -i 's/c__//g' tmp_tax_gut
sed -i 's/o__//g' tmp_tax_gut
sed -i 's/f__//g' tmp_tax_gut
sed -i 's/g__//g' tmp_tax_gut
sed -i 's/s__//g' tmp_tax_gut
sed -i 's/;/\t/g' tmp_tax_gut
                                                                                                                  


/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/tmp_tax_gut


split -n l/4 xaj.tsv 
mv xaa xaj1.tsv
mv xab xaj2.tsv
mv xac xaj3.tsv
mv xad xaj4.tsv



conda activate prokka
prokka \
--outdir /work/projects/ecosystem_biology/archaea/coevolution/analysis/prokka \
--kingdom 'Archaea' \
--cpus 0 \
--proteins /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/all_known/GCF* \
/scratch/users/pnovikova/archaea/data/ncbi/genomes_gut/GCF* \
--force




prokka \
--outdir /work/projects/ecosystem_biology/archaea/coevolution/analysis/prokka/archaea_all/gut/ \
--kingdom 'Archaea' \
--cpus 0 \
/scratch/users/pnovikova/archaea/data/gut/consistent_genomes/* \
--force

#!/bin/bash -l
#SBATCH -J prok_gem
#SBATCH --time=24:00:00
#SBATCH --mem=100GB
#SBATCH -p batch
#SBATCH -N 1
#SBATCH -n 16
#SBATCH --begin=now
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate prokka 

prokka \
--outdir /work/projects/ecosystem_biology/archaea/coevolution/analysis/prokka/archaea_not_gut/ \
--kingdom 'Archaea' \
--cpus 0 \
/work/projects/ecosystem_biology/archaea/coevolution/data/archaea_not_gut_genomes/* \
--force






cut -f1,4-6 proteins_archaea_gut.tsv > tmp_proteins_archaea_gut.tsv

cut -f1,7 proteins_archaea_gut.tsv > tmp_product_proteins_archaea_gut.tsv                                                

/work/projects/ecosystem_biology/archaea/coevolution/analysis



wed 18

/scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_S001__insert_180_gsa_anonymous.fasta.gz



/scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_prediction/prodigal.faa




conda activate prodigal 

assembly=/scratch/users/pnovikova/binning_refinement/benchmarking/high/data/H_pooled_gsa_anonymous.fasta

prodigal -f gff -p meta -o prodigal.gff -i $assembly -q 2> prodigal.err.log > prodigal.out.log

cat prodigal.gff | sed '/# Sequence Data:/d' | sed '/# Model Data:/d' | grep "CDS" > 
 

perl ~/scripts/calc.contig.lengths.pl $assembly > /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/features/length.txt 


conda activate prokka 

cd /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_annotation
prokka \
--outdir /scratch/users/pnovikova/binning_refinement/benchmarking/high/analysis/gene_annotation \
--kingdom 'Bacteria' \
--cpus 0 \
assembly.fasta \
--force



/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/reduced_GUT_catalogue.csv



cd /work/projects/archaeome/coevolution/mantis_final
in mantis_final - /work/projects/ecosystem_biology/archaea/done.tsv

cd /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/input/split_input
in split - /work/projects/ecosystem_biology/archaea/in_split.tsv


awk '{$1 = $1 ".1"} 1' {O,}FS=,

GCF_900188065.1


for f in /work/projects/archaeome/coevolution/mantis_bacteria/*; do
cd $f;
cat $f/integrated_annotation.tsv >> /work/projects/archaeome/coevolution/all_integrated_annotation_bacteria.tsv;
done
cd ../../



for f in /work/projects/archaeome/coevolution/mantis_archaea/*; do
cd $f;
cat $f/consensus_annotation.tsv >> /work/projects/archaeome/coevolution/all_consensus_annotation_archaea.tsv;
done
cd ../../

for f in /work/projects/archaeome/coevolution/mantis_archaea/*; do
cd $f;
cat $f/output_annotation.tsv >> /work/projects/archaeome/coevolution/all_output_annotation_archaea.tsv;
done
cd ../../



grep -v "##" PROKKA_06112021.gff | cut -f1,3,9 | sed -n -e 's/^.*product=//p' > intermediate/products.tsv

grep -v "##" PROKKA_06112021.gff | cut -f1,3,9 | grep "product=" | cut -f1,2 > intermediate/products_ids.tsv

paste intermediate/products_ids.tsv intermediate/products.tsv > tmp && mv tmp intermediate/products.tsv
rm intermediate/products_ids.tsv




grep -v "##" PROKKA_06112021.gff | cut -f1,3,9 | sed -n -e 's/^.*db_xref=//p' | tr ';' ',' | cut -f1 -d,  > intermediate/cog.tsv

grep -v "##" PROKKA_06112021.gff | cut -f1,3,9 | grep "db_xref=" | cut -f1,2 > intermediate/cog_ids.tsv

 paste intermediate/cog_ids.tsv intermediate/cog.tsv > tmp && mv tmp intermediate/cog.tsv



grep -v "##" PROKKA_06112021.gff | cut -f1,3,9 | sed -n -e 's/^.*gene=//p' | tr ';' ',' | cut -f1 -d,  > intermediate/gene.tsv

grep -v "##" PROKKA_06112021.gff | cut -f1,3,9 | grep "gene=" | cut -f1,2 > intermediate/gene_ids.tsv

 paste intermediate/gene_ids.tsv intermediate/gene.tsv > tmp && mv tmp intermediate/gene.tsv








sed -n -e 's/^.*pfam://p'

cd /work/projects/archaeome/coevolution


PFAM 

cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "pfam:" | cut -f1 > intermediate/pfam_ids.tsv

cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "pfam:" | sed -n -e 's/^.*pfam://p' | cut -f1 > intermediate/pfams.tsv

cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "pfam:" | sed -n -e 's/^.*pfam://p' | cut -f2 | sed 's/description://g' > intermediate/pfam_descriptions.tsv



# sort all_integrated_annotation_archaea.tsv | join -o1.2 - file2 | uniq

# grep -F -f all_integrated_annotation_archaea.tsv tmp


# cat tmp | while read line; do
#     grep $line all_integrated_annotation_archaea.tsv
# done


KEGG 

cat all_integrated_annotation_archaea.tsv | grep "kegg_ko" | grep "description:" | cut -f1,3 > intermediate/kegg_proteins2ids.tsv

cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "kegg_ko:" |  grep "description:" | sed -n -e 's/^.*kegg_ko://p' | cut -f1 > intermediate/kegg_ids.tsv

cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "kegg_ko:" |  grep "description:" | sed -n -e 's/^.*description://p' > intermediate/kegg_drscriptions.tsv



EC 
cd /work/projects/archaeome/coevolution

cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "enzyme_ec:"  | grep "description:" | cut -f1 > intermediate/ec_ids.tsv

cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "enzyme_ec:"  | grep "description:" | sed -n -e 's/^.*enzyme_ec://p' | cut -f1 > intermediate/ecs.tsv




cat all_integrated_annotation_archaea.tsv | grep -v "DUF" | grep "enzyme_ec:" |  grep "description:" | sed -n -e 's/^.*description://p' > intermediate/ec_descriptions.tsv








bacteria 
proteins


for f in /work/projects/archaeome/coevolution/mantis_bacteria/*; do
cd $f;
cat $f/integrated_annotation.tsv >> /work/projects/archaeome/coevolution/all_integrated_annotation_bacteria.tsv;
done
cd ../../




if [[ "$file" =~ ^>.* ]]; then
    echo "yes"
fi


cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/split_input/xak.tsv | cut -f2 | while read line; do if [[ $line = \>* ]] ; then echo "yes"; fi; done
name="${name::(-6)}"

cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/split_input/xan.tsv | cut -f2 | while read line; do
top="$(head -n1 $line)"; 
if [[ $top = \>* ]]; then
echo "yes";
else
echo "no";
echo $line;
fi
done | sort | head -10

GUT_GENOME203873.proteins



/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/GUT_GENOME120914.proteins




clustering_file = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/bacteria_gut/clustering_0.9seqid_0.9c.tsv'
save_result2 = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv'



##### clusters_above_threshold.csv - which cluster each protein belongs to; all_proteins.fasta - fasta file formatted to run the perl script
cd /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas
perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/instruction.csv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/all_proteins.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins

##### give extension to all files
for f in *; do mv "$f" "$f".fasta; done

mkdir unclustered
mv unclustered_proteins.fasta unclustered




/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv



# TEMPLATE 
main_dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/
clustering_fasta=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/bacteria_gut/clustering_0.9seqid_0.9c.fasta
instruction=/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv

# PSEUDO CODE 

sed 's/\s.*$//' clustering_fasta | awk '$0==b{next}{b=$0;print}' | sed 's/*//g' > all_proteins.fasta

mkdir dir_protein_families_fastas && cd "$_"

perl -we '
    open my $instruction, "<", "instructions" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "all_proteins.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
for f in *; do mv "$f" "$f".fasta; done

mkdir unclustered
mv unclustered_proteins.fasta unclustered


# for archaea 👆 replace those to:  
clustering_fasta -> /work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/archaea_gut/clustering_0.9seqid_0.9c.fasta
all_proteins.fasta -> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_unique_gut_archaea.fasta
dir_protein_families_fastas -> /scratch/users/pnovikova/coevolution/analysis/phylogenetic_analysis/archaea/PFs_fasta
instructions -> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_baceria_func_relat_clusters.csv


mkdir -p /scratch/users/pnovikova/coevolution/analysis/phylogenetic_analysis/archaea/PFs_fasta
cd /scratch/users/pnovikova/coevolution/analysis/phylogenetic_analysis/archaea/PFs_fasta

perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_arch_func_relat_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_unique_gut_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
for f in *; do mv "$f" "$f".fasta; done

mkdir unclustered
mv unclustered_proteins.fasta unclustered


gawk 'BEGIN{FS="\t"; OFS="\t"} {$2="b_"$2; print}' /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv > /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv





# for bacteria replace to: 
clustering_fasta -> /work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/bacteria_gut/clustering_0.9seqid_0.9c.fasta
all_proteins.fasta -> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta
dir_protein_families_fastas -> /work/projects/archaeome/coevolution/PFs_all_bac+GU_arch
instructions -> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv


sed 's/\s.*$//' /work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/bacteria_gut/clustering_0.9seqid_0.9c.fasta | awk '$0==b{next}{b=$0;print}' | sed 's/*//g' > /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta

cd /work/projects/archaeome/coevolution/PFs_all_bac+GU_arch

perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins

rm unclustered_proteins



perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_arch_GU_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_unique_gut_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
rm unclustered_proteins


for f in *; do mv "$f" "$f".fasta; done








tar cvjf dir.tbz dir --remove-files



###  make lists of protein in genomes 
GUT - `/scratch/users/pnovikova/archaea/data/gut/proteins/*.proteins` - 1162 - by me
GEM - `/scratch/users/pnovikova/archaea/data/gem/proteins_gut/*.faa` - 29 - official
NCBI - `/scratch/users/pnovikova/archaea/data/ncbi/proteins_gut/*.faa` - 3 - official

declare paths=("/scratch/users/pnovikova/archaea/data/gem/proteins_gut/" "/scratch/users/pnovikova/archaea/data/ncbi/proteins_gut/")
# declare paths=(/scratch/users/pnovikova/archaea/data/gut/proteins/test/)

for path in ${paths[@]}; do
cd $path
# for file in *proteins; do
for file in *faa; do
awk -F'>' '
FNR==1{
  match(FILENAME,/.*\./)
  file=substr(FILENAME,RSTART,RLENGTH-1)
}
/^>/{
  printf ("%s\t%s\n", $2, file)
}
' $file
done 
done >> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes.tsv

sed -i '1 i\protein\tgenome' /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes.tsv


# taxonomy 
GEM - `/scratch/users/pnovikova/archaea/data/gem/taxonomy/gem_taxonomy.tsv`
GUT - `/scratch/users/pnovikova/archaea/data/gut/taxonomy/gut_taxonomy.tsv`
NCBI - `/scratch/users/pnovikova/archaea/data/ncbi/taxonomy/ncbi_taxonomy_lineage.tsv`

















cd /work/projects/archaeome/coevolution/PFs_all_bac+GU_arch

perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins

rm unclustered_proteins



perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_common_kegg_ids__kegg_groups_matrix_Gla.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_bacteria_plus_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
rm unclustered_proteins


for f in *; do mv "$f" "$f".fasta; done



conda activate smash 

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch

mkdir -p $main_dir/sig
mkdir -p $main_dir/out

# create signatures
cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch
for f in K02369.fasta; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $main_dir/sig/$f.sig; done

# index 
sourmash index -k 31 $main_dir/sbt_08 $main_dir/sig/*.sig

# compare
sourmash compare -p 16 $main_dir/sig/*.sig -o $main_dir/out/08_cmp --csv $main_dir/out.csv


cd $main_dir

rm -r sig/
rm -r out/


# Run MUSCLE

main_dir=/work/projects/archaeome/coevolution/alignment/FR_bac_VS_FR_arch
pfs=/work/projects/coevolution/phylogeny/smash/FR_bac_VS_FR_arch/PFs_2plus_proteins/

mkdir -p $main_dir/aln
mkdir -p $main_dir/afa

conda activate muscle
for file in $pfs/*.fasta; do
muscle -in $file -fastaout $main_dir/afa/$(basename "${file}" .fasta).afa -clwout $main_dir/aln/$(basename "${file}" .fasta).aln 
rm $main_dir/afa/$(basename "${file}" .fasta).afa
done




muscle -in /work/projects/coevolution/phylogeny/smash/FR_bac_VS_FR_arch/PFs/a_100070.fasta \
-fastaout afa.afa \
-clwout aln.aln -clwstrict


t_coffee


/work/projects/coevolution/phylogeny/smash/FR_bac_VS_FR_arch/PFs/


/work/projects/coevolution/phylogeny/smash/FR_bac_VS_FR_arch/PFs_2plus_proteins/


/work/projects/coevolution/phylogeny/smash/FR_bac_VS_FR_arch/PFs_2plus_proteins/a_100893.fasta



main_dir=/work/projects/coevolution/phylogeny/tcoffee/FR_bac_VS_FR_arch
pfs=/work/projects/coevolution/phylogeny/smash/FR_bac_VS_FR_arch/PFs_2plus_proteins

conda activate tcoffee
cd $main_dir

for file in $pfs/*.fasta; do
	t_coffee $file
	rm $(basename "${file}" .fasta).html
done



sed -n '/^>GUT_GENOME103991.fasta_782/ s/.// ; //,/>/ { />/! p }' input_file






cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_selective_genomes.tsv | while read f; do
	awk -F'>' '
	FNR==1{
	  match(FILENAME,/.*\./)
	  file=substr(FILENAME,RSTART,RLENGTH-1)
	}
	/^>/{
	  print $2,file
	}
	' $f* >> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_protein2genome.tsv
done


sed 's/#.*$//'




clustalW output - /work/projects/coevolution/distance_matrices/common_kegg_ids__kegg_groups
clustalW input - /work/projects/coevolution/phylogeny/smash/bac_VS_arch_common_kegg_ids/kegg_groups/*.fasta




conda activate clustalw
for file in /work/projects/coevolution/phylogeny/smash/bac_VS_arch_common_kegg_ids/kegg_groups/*.fasta; do 
	clustalo -i $file --distmat-out /work/projects/coevolution/distance_matrices/common_kegg_ids__kegg_groups/$(basename "${file}" .fasta).mat --full
	awk -v OFS="\t" '$1=$1' /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat | tail -n +2  > tmp && mv tmp /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat
done



conda activate clustalw
for file in /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*.aln; do 
clustalw2 -infile=$file -tree -outputtree=dist -clustering=NJ
done
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format/
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format/dst
mv /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*ph /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format
mv /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*dst /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format/dst





conda activate tcoffee
cd $main_dir

for file in /work/projects/coevolution/phylogeny/smash/bac_VS_arch_common_kegg_ids/kegg_groups/*.fasta; do
	t_coffee $file (-output newick)
	rm $(basename "${file}" .fasta).html
done




K14495.dnd
K16195.dnd
K19543.dnd
K23498.dnd




sed -i "s/$var1/ZZ/g" "$file"


should be 2 columns (protein, genome)
sed -n "/^>$f/ s/.// ; //,/>/ { />/! p }" $input_file > $f.faa




sed -n '/^>GUT_GENOME103880_1_2225/ s/.// ; //,/>/ { />/! p }' /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/GUT_GENOME103880.*


sed -n '/^>GUT_GENOME103880_1_2225/ s/.// ; //,/>/ { />/! p }' /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/GUT_GENOME103880.* | sed 's/GUT_GENOME103880_1_2225/>GUT_GENOME103880_1_2225/g' > GUT_GENOME103880_1_2225.faa


cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_selected_genome2protein_matrix_Gla.tsv | while read f1 f2 ; do
	sed -n "/^>$f1 / s/.// ; //,/>/ { />/! p }" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/$f2* >> /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__matrix_Gla/$f1.faa
	sed -i "s/$f1/>$f1/g" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__matrix_Gla/$f1.faa
done



for f in /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__matrix_Gla/*; do
	name="$(basename "$f" | cut -d. -f1)"
	echo -e "$name\t$f"
done > /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/tmp_id2faa_selective_matrix_Gla.tsv



grep "GUT_GENOME286792" /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_selected_genome2protein.tsv | while read f1 f2 ; do
	awk "BEGIN{RS=">";ORS=""} /${f1}/" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/$f2* >> /work/projects/ecosystem_biology/archaea/$f1.faa
done

GUT_GENOME286792_22_39


grep "GUT_GENOME286792" /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_selected_genome2protein.tsv | while read f1 f2 ; do
	sed -n "/^>$f1 / s/.// ; //,/>/ { />/! p }" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/$f2* >> /work/projects/ecosystem_biology/archaea/$f1.faa
done

grep ">" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__kegg_groups_bacteria/*.faa | wc -l
ls /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__kegg_groups_bacteria/*faa | wc -l




cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_selected_genome2protein_whatsleft.tsv | while read f1 f2 ; do
sed -n "/^>$f1 / s/.// ; //,/>/ { />/! p }" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/$f2* >> /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__kegg_groups_bacteria/$f1.faa
sed -i "s/$f1/>$f1/g" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__kegg_groups_bacteria/$f1.faa
done




find . -name '*.faa' -size 0

sed -n '/^>3300007796_31_171/ s/.// ; //,/>/ { />/! p }' /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/3300007796_31.* | sed 's/3300007796_31_171/>3300007796_31_171/g' > 3300007796_31_171.faa




cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_selected_genome2protein_whatsleft.tsv | while read f1 f2 ; do
sed -n "/^>${f1}/ s/.// ; //,/>/ { />/! p }" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/$f2* >> /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__kegg_groups_bacteria/$f1.faa
sed -i "s/${f1}/>${f1}/g" /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_common_kegg_ids__kegg_groups_bacteria/$f1.faa
done


conda activate mantis_env
work='/work/projects/ecosystem_biology/archaea/coevolution'

python /work/projects/ecosystem_biology/local_tools/mantis run_mantis \
-t /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/id2faa2ncbi_id_selective_matrix_Gla.tsv \
-o /work/projects/coevolution/tmp_matrix_Gla \
-mc $work/analysis/mantis/config_eggnog.config \
-da heuristic \
-cs 10000     


/work/projects/archaeome/coevolution/all_integrated_annotation_bacteria_same_kegg_proteins_matrix_Gla.tsv

/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/bacteria_annot_eggnog_matrix_Gla.tsv

/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria - move annotations here 

#!/bin/bash -l
#SBATCH -J mantis_select
#SBATCH --time=48:00:00
#SBATCH -p batch
#SBATCH -o mantis_select_%j.log
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -N 1
#SBATCH -n 64
#SBATCH --mem=200GB


conda activate mantis_env
work='/work/projects/ecosystem_biology/archaea/coevolution'

date

python /work/projects/ecosystem_biology/local_tools/mantis run_mantis -t /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/id2faa2ncbi_id_selective.tsv -o /work/projects/coevolution/tmp -mc $work/analysis/mantis/config_eggnog.config -da heuristic -cs 10000

date

python /work/projects/ecosystem_biology/local_tools/mantis run_mantis \
-t /work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/input/id2faa2ncbi_id_selective_matrix_Gla.tsv \
-o /work/projects/coevolution/tmp_matrix_Gla \
-mc $work/analysis/mantis/config_eggnog.config \
-da heuristic \
-cs 1000  


for f in /work/projects/coevolution/tmp_select_same_kegg/*; do
cd $f;
cat $f/integrated_annotation.tsv >> /work/projects/archaeome/coevolution/all_integrated_annotation_bacteria_same_kegg_proteins.tsv;
done
cd ../../




MTKFFLLDLILFLIIGGVAIILQYHFGNRLSKKVEIVYGCISAGIIAVTSTVFAIKMNPIITPSLHADYLKLNEEIKYESIELCENYRDCTLKIIANGFRGYQTYYSGDANVEIYNSKNERLNYYYKAKVYYNIDVNSVIVLKDANDAKVYIFNFDTNQQTQDFYDLLCKKVDKVGVSHTESHSVHH*



find . | head -1063079 | tar -cvf ../PF_rprs_1st_1063079.tar.gz



tar -cvf files.tar --remove-files my_directory



tar -cvf ../PF_rprs_1st_1063079.tar.gz -T /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PF_rprs_1st_1063079_list.txt



MTKFFLLDLILFLIFGGVAIILQYHFGNRLSKKVEIVYGCISAGIIAVTSIVFAIKMNPIITPSLHADYLKLNEEIKYTSIELCENYRDCTLKIVANGFRWGYQTYYSGDANVEIYNSKNERLNYYYKAKVYYNIDVNSVIVLKDANDAKVYIFNFDTNQQTQDFYDLLCKKVDKVGVSHTESHSVHH




tar czf /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PF_rprs_1st_1063079.tar.gz -T /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PF_rprs_1st_1063079_list.txt


cat /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PF_rprs_1st_1063079_list.txt | while read f; do rm /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs/${f}; done    





date
find -type f -name '*' | wc -l



/work/projects/archaeome/coevolution/annotation_archaea/all_integrated_annotation_archaea_eggnog.tsv

/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_common_kegg_ids__kegg_groups_matrix_Gla.tsv


head /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_common_kegg_ids__kegg_groups_matrix_Gla.tsv | cut -f1 | while read f; do
grep $f all_integrated_annotation_bacteria_same_kegg_proteins_matrix_Gla.tsv
done
	


cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs
gunzip /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PF_rprs_1st_1063079.tar.gz



cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective
perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_PFs_oxidase_cluster_4_PLUS_archaea_nog_annotated.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_bacteria_plus_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
rm unclustered_proteins


for f in *; do mv "$f" "$f".fasta; done



/work/projects/archaeome/binning_refinement/data/assembly.fa





comm -23 f1 f2

comm -23 <(sort all.rprs.txt) <(sort whats.done.txt)





conda activate smash

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch

# create signatures
cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs

cat /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/whats.left.txt | while read f; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $main_dir/sig/$f.sig; done

find -type f -name '*.fasta' -exec mv {} ../ \;


cd PFs_rprs/
find -type f -name '*.fasta' >> ../all.txt
sed -i 's/.\///g' all.txt 


cd sig/
find -type f -name '*.fasta*' >> ../whats.done.txt

sed -i 's/.\///g' whats.done.txt 



cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs

cat /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/whats.left.txt | while read f; do ls /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs/$f; done | head 




conda activate mmseqs2

cd /work/projects/archaeome/binning_refinement/analysis/taxonomy/H_S005
mmseqs createdb /work/projects/archaeome/binning_refinement/data/H_S005_assembly.fa contig
mmseqs databases UniProtKB/Swiss-Prot swissprot tmp
mmseqs taxonomy contig swissprot assignments tmpFolder --tax-lineage 2 --majority 0.5 --vote-mode 1 --lca-mode 3 --orf-filter 1 --lca-ranks superkingdom,phylum,class,order,family,genus,species
mmseqs createtsv contig assignments assignRes.tsv 
cat assignRes.tsv | cut -f1,9 | sed 's/;/\t/g'  | sed '1 i\contig\tsuperkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies' >> taxonomy_lineage.tsv


# make a snakemake rule 
sample = "H_S005"

rule all:
  input: 
    f"data/{sample}_assembly.fa"

rule mmseqs2:
  input: "data/{sample}_assembly.fa"
  output:
    assignRes="analysis/taxonomy/{sample}/assignRes.tsv",
    lineage="analysis/taxonomy/{sample}/taxonomy_lineage.tsv"
  conda: "envs/mmseqs2.yaml"
  shell:
    """
    cd analysis/taxonomy/{sample}/

    mmseqs createdb {input} contig
    mmseqs databases UniProtKB/Swiss-Prot swissprot tmp
    mmseqs taxonomy contig swissprot assignments tmpFolder --tax-lineage 2 --majority 0.5 --vote-mode 1 --lca-mode 3 --orf-filter 1 --lca-ranks superkingdom,phylum,class,order,family,genus,species
    mmseqs createtsv contig assignments {output.assignRes}

    cat {output.assignRes}| cut -f1,9 | sed 's/;/\t/g'  | sed '1 i\contig\tsuperkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies' >> {output.lineage}
    """



sample = "H_S005"
outdir = "analysis/taxonomy/{sample}"

rule all:
  input: 
    f"data/{sample}_assembly.fa"

rule mmseqs2:
  input: "data/{sample}_assembly.fa"
  params:
    tmp=outdir+"/tmp",
    tmpFolder=outdir+"/tmpFolder"
  output:
    contig=outdir+"/contig",
    swissprot=outdir+"/swissprot",
    assignments=outdir+"/assignments",
    assignRes=outdir+"/assignRes.tsv",
    lineage=outdir+"/taxonomy_lineage.tsv"
  conda: 
     "envs/mmseqs2.yaml" 
  shell:
    """
    mmseqs databases UniProtKB/Swiss-Prot {output.swissprot} {params.tmp}
    mmseqs createdb {input} {output.contig}
    mmseqs taxonomy {output.contig} {output.swissprot} {output.assignments} {params.tmpFolder} --tax-lineage 2 --majority 0.5 --vote-mode 1 --lca-mode 3 --orf-filter 1 --lca-ranks superkingdom,phylum,class,order,family,genus,species

    mmseqs createtsv {output.contig} {output.assignments} {output.assignRes}

    cat {output.assignRes}| cut -f1,9 | sed 's/;/\t/g'  | sed "1 i\contig\tsuperkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies" >> {output.lineage}
    """





rm Snakefile
vim Snakefile



outdir = "analysis/taxonomy/H_S005"

rule mmseqs2:
  input: "data/H_S005_assembly.fa"
  params:
    tmp=outdir+"/tmp",
    tmpFolder=outdir+"/tmpFolder"
  output:
    lineage=outdir+"/taxonomy_lineage.tsv"
  conda: 
     "envs/mmseqs2.yaml" 
  shell:
    """
    mmseqs databases UniProtKB/Swiss-Prot outdir+"/swissprot" {params.tmp}
    mmseqs createdb {input} outdir+"/contig"
    mmseqs taxonomy outdir+"/contig" outdir+"/swissprot" outdir+"/assignments" {params.tmpFolder} --tax-lineage 2 --majority 0.5 --vote-mode 1 --lca-mode 3 --orf-filter 1 --lca-ranks superkingdom,phylum,class,order,family,genus,species

    mmseqs createtsv outdir+"/contig" outdir+"/assignments" {output.assignRes}

    cat {output.assignRes}| cut -f1,9 | sed 's/;/\t/g'  | sed '1 i\contig\tsuperkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies' >> {output.lineage}
    """




cd /work/projects/coevolution/phylogenetic_trees/cyctochrome_oxidase_family

conda activate tcoffee

t_coffee /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/cyctochrome_oxidase_family.fasta







cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/homogenuous_clusters/bacteria
perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_homogenuous_clusters_bacteria_smash.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
rm unclustered_proteins


for f in *; do mv "$f" "$f".fasta; done








grep -A 15 "GUT_GENOME023259_22_15" /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta   

K19481_matrixGla_4.fasta



#!/bin/bash -l
#SBATCH -J mantis_l
#SBATCH --time=14-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=long
#SBATCH -o mantis_xaa_long_%j.log
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -N 1
#SBATCH -n 88
#SBATCH --mem=1TB


conda activate smash

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch


mkdir -p $main_dir/out

# index                  
sourmash index -k 51 -f $main_dir/sbt_08 --protein --from-file $main_dir/list_of_sigs.txt


# compare                                                
sourmash compare -p 16 --from-file $main_dir/list_of_sigs.txt -o $main_dir/out/08_cmp --csv $main_dir/out.csv

cd $main_dir/sig
sourmash index -k 51 -f $main_dir/sbt_08 --from-file ../list_of_sigs_1.txt                                                            
                                                            
b_4143468.fasta.sig



mkdir /work/projects/archaeome/binning_refinement/analysis/taxonomy/2017.12.04_18.45.54_sample_18
cd /work/projects/archaeome/binning_refinement/analysis/taxonomy/2017.12.04_18.45.54_sample_18

mmseqs createdb /work/projects/archaeome/binning_refinement/data/2017.12.04_18.45.54_sample_18_assembly.fa contig
mmseqs databases UniProtKB/Swiss-Prot swissprot tmp
mmseqs taxonomy contig swissprot assignments tmpFolder --tax-lineage 2 --majority 0.5 --vote-mode 1 --lca-mode 3 --orf-filter 1 --lca-ranks superkingdom,phylum,class,order,family,genus,species
mmseqs createtsv contig assignments assignRes.tsv 
cat assignRes.tsv | cut -f1,9 | sed 's/;/\t/g'  | sed '1 i\contig\tsuperkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies' >> taxonomy_lineage.tsv



conda activate hhsuite

cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/homogenuous_clusters/archaea/

for f in *fasta; do
hhblits \
-i $f \
-oa3m /work/projects/ecosystem_biology/archaea/coevolution/analysis/hhblits/archaea/$f.a3m \
-cpu 16 \
-d dbs/uniprot20
done 	

hhblits \
-i /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/homogenuous_clusters/archaea/*fasta \
-oa3m /work/projects/ecosystem_biology/archaea/coevolution/analysis/hhblits/archaea/*a3m \
-cpu 16 \
-d dbs/uniprot20




cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/homogenuous_clusters/archaea/

for f in *fasta; do
	clustalo -i $f -o /work/projects/ecosystem_biology/archaea/coevolution/analysis/alignment/clustalO/homogenuous_clusters/archaea/$(basename "${f}" .fasta).clustal --outfmt=clu
done



/work/projects/ecosystem_biology/archaea/hmms/analysis/hmms
/work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds


cd /work/projects/ecosystem_biology/archaea/hmms/analysis/hmms/
rm *.h3i
rm *.h3m
rm *.h3f
rm *.h3p
for f in *.hmm; do
	hmmpress $f
	hmmscan --noali -o /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds/hmmscan_output_HmdC/$f.output $f /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds/HmdC.faa 
done

cd /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds/hmmscan_output_HmdB/
grep -L "No targets" *output 





hmmscan -o 1000004.output --noali /work/projects/ecosystem_biology/archaea/hmms/analysis/hmms/1000004.hmm /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds/HmdB.faa 




HmdC:
more 2400376.hmm.output - 0

HmdB:
more 1819374.hmm.output -2.5e-09                                                         
more 2183986.hmm.output -4.5e-05                                                                          
more 2391866.hmm.output -1.6e-236                                                                                             
more 2401948.hmm.output -2.6e-06                                                                               
more 2402490.hmm.output -2.2e-10                                                   
more 2418528.hmm.output -3.2e-08


/work/projects/ecosystem_biology/archaea/hmms/analysis/intermediate_results/annotated_protein_clusters.csv


cd /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds

conda activate diamond 

diamond makedb --in 2hp_proteins_prokka.faa -d 2hp_proteins_prokka 

diamond blastp -d 2hp_proteins_prokka -q HmdB.faa -o HmdB_matches.tsv --evalue 1e-6 --mid-sensitive
diamond blastp -d 2hp_proteins_prokka -q HmdC.faa -o HmdC_matches.tsv --evalue 1e-6 --mid-sensitive




cd /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds

cat HmdB_matches_VS.tsv | cut -f2 | while read f; do
 sed -n "/^>$f/ s/.// ; //,/>/ { />/! p }" 2hp_proteins_prokka.faa | sed "s/$f/>$f/g"
done >> HmdB_matches_VS.faa

sed -n '/^>NOEFAICJ_07554/ s/.// ; //,/>/ { />/! p }' 2hp_proteins_prokka.faa | sed 's/NOEFAICJ_07554/>NOEFAICJ_07554/g'



*.prokka.faa

cd /mnt/isilon/projects/ecosystem_biology/MiBiPa/users/patrick/proteins/sequences


cd /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds/samples
for file in Man14122018_2*faa; do
awk -F'>' '
FNR==1{
  match(FILENAME,/.*\./)
  file=substr(FILENAME,RSTART,RLENGTH-1)
}
/^>/{
  printf ("%s\t%s\n", $2, file)
}
' $file > /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds/protein-sample_lists/$(basename "${file}" .prokka.faa).tsv
done 




cd /work/projects/ecosystem_biology/archaea/hmms/analysis/mibipa/other_hmds



#!/bin/bash -l
#SBATCH -J mantis
#SBATCH --time=48:00:00
#SBATCH -p bigmem
#SBATCH -o mantis_%j.log
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -N 1
#SBATCH -n 64
#SBATCH --mem=1TB


conda activate mantis_env

date

python /work/projects/ecosystem_biology/local_tools/mantis run_mantis \
-t /scratch/users/smartinezarbas/LAO/IMP_results/Assemblies/sample_to_faa.tsv \
-o /work/projects/archaeome/mantis/susana \
-mc /work/projects/ecosystem_biology/local_tools/mantis_configs/default.config \
-da heuristic \
-cs 7000



	

/scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME139173.fasta



conda activate prodigal 

cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/test/

prodigal -i GUT_GENOME139173.fasta -o GUT_GENOME139173.fasta.gff -a GUT_GENOME139173.fasta.proteins -f gff






prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME139173.fasta -o /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/test/GUT_GENOME139173.fasta_A.gff -a /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/test/GUT_GENOME139173.fasta.proteins -f gff


# enumerate 
awk 'BEGINFILE {fn=FILENAME; sub(/\..*$/, "", fn); i=0} $1 ~ /^GUT/{$1 = "" fn ".fasta_" ++i} 1' GUT_GENOME139173.fasta.gff






cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/test/

genome=GUT_GENOME111483.fasta

cp /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/$genome ./

prodigal -i $genome -o $genome.gff -a $genome.proteins -f gff






perl -lpi -e '$_ .= "\tNone"' file




split -n l/5 ../xaa.tsv
for f in xa*; do mv "$f" "$f".tsv; done




/scratch/users/pnovikova/tools/mantis/configs/only_kegg.config



#!/bin/bash -l
#SBATCH -J mantis_l
#SBATCH --time=7-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=long
#SBATCH -o mantis_xaa_long_%j.log
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -N 1
#SBATCH -n 88
#SBATCH --mem=1TB

conda activate mantis_env

date

python /work/projects/ecosystem_biology/local_tools/mantis run_mantis \
-t /work/projects/ecosystem_biology/archaea/tmp/split_xaa/xaa.tsv \
-o /work/projects/archaeome/mantis/susana \
-mc /scratch/users/pnovikova/tools/mantis/configs/only_kegg.config \
-da heuristic \
-cs 7000


cat /scratch/users/pnovikova/archaea/data/gem/taxonomy/gem_taxonomy.tsv /scratch/users/pnovikova/archaea/data/gut/taxonomy/gut_taxonomy.tsv > 



/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tax_all_archaea.tsv

/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/homogenuous_clusters_all.tsv


join -1 3 -2 1 <(sort -k 3 homogenuous_clusters_all.tsv) <(sort -k 1 tax_all_archaea.tsv) -t '\t'



join -t "`echo '\t'`" -1 3 -2 1 <(sort -k 3 homogenuous_clusters_all.tsv) <(sort -k 1 tax_all_archaea.tsv)



join join -t "$tab" -1 3 -2 1 homogenuous_clusters_all.tsv tax_all_archaea.tsv | head



join -t "$tab" homogenuous_clusters_all.tsv tax_all_archaea.tsv | head




join -a1 -t',' <(sort homogenuous_clusters_all.tsv) <(sort tax_all_archaea.tsv) 


sed -i 's/,/\t/g' tax_all_archaea.tsv 
sed -i 's/,/\t/g' homogenuous_clusters_all.tsv

-1 1 -2 1 



join homogenuous_clusters_all.tsv tax_all_archaea.tsv 




declare paths=("/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/")

or path in ${paths[@]}; do
cd $path
for file in *; do
awk -F'>' '
FNR==1{
  match(FILENAME,/.*\./)
  file=substr(FILENAME,RSTART,RLENGTH-1)
}
/^>/{
 printf ("%s\t%s\n", $2, file)
}
' $file
done 
done >> /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_bacteria.tsv






cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/homogenuous_clusters_vs_bacteria_w_same_keggs
perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_for_phyl_trees_homogenuous_clusters_VS_bacterial_clusters_w_same_keggs.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_bacteria_w_b_suffix_plus_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
rm unclustered_proteins


for f in *; do mv "$f" "$f".fasta; done




cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_and_bacteria_w_same_kegg__homogenuous_groups



scp -P 8022 pnovikova@access-iris.uni.lu:/work/projects/archaeome/coevolution/annotation_archaea/all_integrated_annotation_archaea_kegg.tsv ../intermediate_results/




# extract coordinates from mantis files

## archaea

cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_and_bacteria_w_same_kegg__homogenuous_groups/

for file in c*.fasta; do
grep ">" $file | sed 's/>//g' | while read f; do
grep -w "$f" /work/projects/archaeome/coevolution/annotation_archaea/all_integrated_annotation_archaea_kegg.tsv
done > /work/projects/ecosystem_biology/archaea/coevolution/analysis/annotations_coordinates/arch_and_bacteria_w_same_kegg__homogenuous_groups/archaea/$(basename "${file}" .fasta).tsv 

sed -i '1 i\Query\tRef_file\tRef_hit\tRef_hit_accession\tevalue\tbitscore\tDirection\tQuery_length\tQuery_hit_start\tQuery_hit_end\tRef_hit_start\tRef_hit_end\tRef_length\t|\tLinks' /work/projects/ecosystem_biology/archaea/coevolution/analysis/annotations_coordinates/arch_and_bacteria_w_same_kegg__homogenuous_groups/archaea/$(basename "${file}" .fasta).tsv

done

## bacteria


cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_and_bacteria_w_same_kegg__homogenuous_groups/

for file in c*.fasta; do
grep ">b_" $file | sed 's/>//g' | while read f; do
grep -w "$f" /work/projects/archaeome/coevolution/annotation_bacteria/all_integrated_annotation_bacteria_no_title.tsv 
done > /work/projects/ecosystem_biology/archaea/coevolution/analysis/annotations_coordinates/arch_and_bacteria_w_same_kegg__homogenuous_groups/bacteria/$(basename "${file}" .fasta).tsv 

sed -i '1 i\Query\tRef_file\tRef_hit\tRef_hit_accession\tevalue\tbitscore\tDirection\tQuery_length\tQuery_hit_start\tQuery_hit_end\tRef_hit_start\tRef_hit_end\tRef_length\t|\tLinks' /work/projects/ecosystem_biology/archaea/coevolution/analysis/annotations_coordinates/arch_and_bacteria_w_same_kegg__homogenuous_groups/bacteria/$(basename "${file}" .fasta).tsv
done



scp -P 8022 pnovikova@access-iris.uni.lu:/work/projects/ecosystem_biology/archaea/coevolution/analysis/annotations_coordinates/arch_and_bacteria_w_same_kegg__homogenuous_groups/archaea/ ../intermediate_results/coordinates_archaea


/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/all_known


str="MKSVDFPETVSAKVTSRCKKLLEKHNISVRSAVEVGLNTLLSSKGRLEFEIMELDKEIREVKLDLIALEMERDQLLGKLEGLHSSEEPVEIHMCKQVYKCKQM"
echo "${str:3:96}"










/work/projects/coevolution/phylogeny/smash/arch_uncommon_keg_ids





/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_arch_clusters_uncommon_functions.csv


scp -P 8022 pnovikova@access-iris.uni.lu:/work/projects/coevolution/phylogeny/smash/arch_uncommon_kegg_ids/smash_clusters/*.fasta /Users/polina.novikova/Documents/phd/host_association/phylogeny__uncommon_kegg_archaea/smash_clusters





/work/projects/coevolution/phylogeny/smash/arch_uncommon_kegg_ids/smash_clusters




dir=/work/projects/archaea_neurodeg/data/MCI/HZ

cat ctrl.txt | while read f; do
	echo $f
	mv $dir/all/$f* $dir/ctrl
done



dir=/work/projects/archaea_neurodeg/data/
output=/work/projects/archaea_neurodeg/analysis/fastqc
dataset=AD/WI/disease
name=AD_WI_disease

cd $dir/$dataset
gunzip *

conda activate multiqc

mkdir -p $output/$dataset

fastqc -t 100 -o $output/$dataset $dir/$dataset/*
cd $output/$dataset

multiqc . -o $output -n multiqc_report_$name.html
cd $output





source=/work/projects/archaea_neurodeg/data/AD/WI/ctrl
target=/Users/polina.novikova/Documents/phd/archaea_in_ND/datasets/AD/WI/ctrl

scp -P 8022 pnovikova@access-iris.uni.lu:$source/* $target/



bash setup_and_test/full_database_download.bash ---- done






conda activate diamond 

diamond makedb --in 2hp_proteins_prokka.faa -d 2hp_proteins_prokka 

diamond blastp -d 2hp_proteins_prokka -q HmdB.faa -o HmdB_matches.tsv --evalue 1e-6 --mid-sensitive
diamond blastp -d 2hp_proteins_prokka -q HmdC.faa -o HmdC_matches.tsv --evalue 1e-6 --mid-sensitive




/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1232_7/run1/Assembly/mt.megahit_preprocessed.1/final.contigs.fa - the MT assembly (not use)

/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1232_7/run1/Analysis/mt.gene_depth.avg - table with gene abundance (use later)

/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1232_7/run1/Analysis/annotation/prokka.faa - called genes



/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis - directory


# dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis

# $dir/mapping 

# conda activate diamond

# # diamond makedb --in $dir/proteins_homo.faa -d proteins_homo

# diamond makedb --in /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1232_7/run1/Analysis/annotation/prokka.faa -d 1232_7_proteins


# diamond blastp -d 1232_7_proteins -q $dir/proteins_homo.faa -o $dir/mapping/1232_7_matches.tsv --evalue 1e-6 --mid-sensitive


# RUN BLASTP

conda activate diamond

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis

cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/all.ids | while read id; do
	echo ${id}

	diamond makedb --in /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/${id}/run1/Analysis/annotation/prokka.faa -d ${id}_proteins

	diamond blastp -d ${id}_proteins -q $dir/proteins_homo.faa -o $dir/mapping/${id}_matches.tsv --evalue 1e-6 --id 50 --sensitive --header 

	echo "done ${id}"
done

# PROCESS OUTPUT

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/all.ids | while read id; do
	# remove headers
	sed -e '1,3d' $dir/mapping/${id}_matches.tsv > $dir/mapping_processed/${id}_matches.tsv

	# keep only 3 first and 12th columns
	cut -f1-3,12 $dir/mapping_processed/${id}_matches.tsv > tmp && mv tmp $dir/mapping_processed/${id}_matches.tsv
done



$dir/mapping_processed/Man14122018_2_matches.tsv


/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/Man14122018_2/run1/Analysis/mt.gene_depth.avg


# ################
# MAY 24TH 2022
# ################

# join -j 2 -o 1.1,1.2,1.3,2.3 <(sort -k2 file1) <(sort -k2 file2)

# file1=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping_processed/Man14122018_2_matches.tsv
# file2=/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/Man14122018_2/run1/Analysis/mt.gene_depth.avg

# join -1 2 -2 1 <(sort -k 2 $file1) <(sort -k 1 $file2) - did that in python search_of_MG_genes_in_MT_data.ipynb


# sort to folders according to the condition

cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mibipa_rbd.tsv | cut -f1 | while read f; do
	mkdir -p rbd/
	mv ${f}.csv rbd/
done


# calculate how many times an MG gene occurs in samples of a certain condition
cd $dir/mg_mt_matches/tables_per_sample_reduced/rbd

cat *.csv | cut -f1 | uniq | sed 's/gene_mg//g' | sort | uniq -c | tail -n +2 > $dir/counts_in_rbd.tsv 

sort -rn -k1 $dir/counts_in_ctrl.tsv > tmp && mv tmp $dir/counts_in_ctrl.tsv

# check
head -10 counts_in_*

# ################
# MAY 25TH 2022
# ################

# RUN BLASTP

conda activate diamond

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis

cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/all.ids | while read id; do
	echo ${id}

	diamond makedb --in /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/${id}/run1/Analysis/annotation/prokka.faa -d ${id}_proteins

	diamond blastp -d ${id}_proteins -q $dir/proteins_homo.faa -o $dir/mapping/uniq/${id}_matches.tsv --evalue 1e-6 --id 50 --sensitive --header 

	echo "done ${id}"
done

# PROCESS OUTPUT

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/all.ids | while read id; do
	# remove headers
	sed -e '1,3d' $dir/mapping/uniq/${id}_matches.tsv > $dir/mapping_processed/uniq/${id}_matches.tsv

	# keep only 3 first and 12th columns
	cut -f1-3,12 $dir/mapping_processed/uniq/${id}_matches.tsv > tmp && mv tmp $dir/mapping_processed/uniq/${id}_matches.tsv
done


# ################
# MAY 27TH 2022
# ################


# run search_of_MG_genes_in_MT_data.ipynb for merging diamond hits with MT abundancies

cd $dir/mg_mt_matches/tables_per_sample_reduced/uniq

# sort to folders according to the condition

cat /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mibipa_rbd.tsv | cut -f1 | while read f; do
	mkdir -p rbd/
	mv ${f}.csv rbd/
done


# calculate how many times an MG gene occurs in samples of a certain condition
cd $dir/mg_mt_matches/tables_per_sample_reduced/uniq/ctrl

cat *.csv | cut -f1 | uniq | sed 's/gene_mg//g' | sort | uniq -c | tail -n +2 | sort -rn -k1 > $dir/counts_in_ctrl_uniq.tsv 

# check
cd $dir
head -10 counts_in_*_uniq.tsv

# put the result manually to a .tsv file and upload it to intermediate_result folder




/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/Man14122018_2/run1/Preprocessing

# ################
# MAY 31st 2022
# ################

conda activate prodigal

cat /work/projects/archaea_neurodeg/host_association/prodigal_genes/genomes_uniq.tsv | while read genome; do 
	for f in /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}; do
	echo $f
	prodigal -i $f -d /work/projects/archaea_neurodeg/host_association/prodigal_genes/$(basename "$f").genes \
	-a /work/projects/archaea_neurodeg/host_association/prodigal_genes/$(basename "$f").proteins -f gff;
	done
done



for f in /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME152649.fasta; do
	echo $f
	prodigal -i $f -d /work/projects/archaea_neurodeg/host_association/prodigal_genes/$(basename "$f").genes \
	-a /work/projects/archaea_neurodeg/host_association/prodigal_genes/$(basename "$f").proteins -f gff;
done




grep -A 30 "GUT_GENOME234397.fasta_879" GUT_GENOME234397.fasta.genes


# ################
# JUNE 2nd 2022
# ################

$dir/proteins_uniq_n.fa - nucleotide seqeunces of unique proteins 

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

conda activate mapping

reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/must_m_01/M1.1-V1/Reads/MT.R1.fq
reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/must_m_01/M1.1-V1/Reads/MT.R2.fq


# indexing
bwa index $dir/proteins_uniq_n.fa

# paired-end mapping, general command structure, adjust to your case
bwa mem $dir/proteins_uniq_n.fa $reads1 $reads2 > $dir/uniq_aln_pe.sam



# ################
# JUNE 3rd 2022
# ################

conda activate mapping

# fix mates and compress
samtools sort -n -O sam $dir/uniq_aln_pe.sam | samtools fixmate -m -O bam - $dir/uniq.fixmate.bam

# convert to bam file and sort
samtools sort -O bam -o $dir/uniq.sorted.bam $dir/uniq.fixmate.bam

# Once it successfully finished, delete the fixmate file and the sam file to save space
rm $dir/uniq.fixmate.bam
rm $dir/uniq_aln_pe.sam

# Lets get an mapping overview:
samtools flagstat $dir/uniq.sorted.bam

# For the sorted bam-file we can get read depth for at all positions of the reference genome, e.g. how many reads are overlapping the genomic position.
samtools depth $dir/uniq.sorted.bam > $dir/uniq.depth.txt

# --------------
# TOTAL COVERAGE

# The total size of the genome can be calculated like this:
samtools view -H $dir/uniq.sorted.bam | grep -P '^@SQ' | cut -f 3 -d ':' | awk '{sum+=$1} END {print sum}'
# NR = 16683

# and now calculate the average coverage (for covered bases):
samtools depth $dir/uniq.sorted.bam |  awk '{sum+=$3} END { print "Average = ",sum/16683}'
# --------------

# get mean (median) read coverage for each gene:
samtools index $dir/uniq.sorted.bam
cd $dir
mosdepth prefix $dir/uniq.sorted.bam --use-median -t 4 



# ################
# JUNE 7th 2022
# ################

# create meta files with sample names to iterate over them: 
ls -d /mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/must_m_* | awk -F "/" '{print $NF}' > must_family_ids.tsv 




reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/must_m_01/M1.1-V1/Reads/MT.R1.fq
reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/must_m_01/M1.1-V1/Reads/MT.R2.fq

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

cat must_family_ids.tsv | while read f; do
	touch $dir/$f.tsv
	echo $f
	ls -d /mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/$f/M* | awk -F "/" '{print $NF}' >> $dir/$f.tsv
done




conda activate kallisto 

kallisto index -i $dir/proteins_uniq_n.idx $dir/proteins_uniq_n.fa

cat $dir/must_family_ids.tsv | while read family; do
	echo $family
	mkdir -p $dir/kallisto/${family}
	cat $dir/${family}.tsv | while read sample; do
		echo $sample
		mkdir -p $dir/kallisto/${family}/${sample}

		reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
		reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

		echo $reads1
		echo $reads2

		kallisto quant -i $dir/proteins_uniq_n.idx -o $dir/kallisto/${family}/${sample}/ --plaintext -t 128 $reads1 $reads2
	done
done


# ################
# JUNE 8th 2022
# ################



cat $dir/must_family_ids.tsv | while read family; do
echo $family
cat $dir/${family}.tsv | while read sample; do
echo $sample
# mv $dir/kallisto/${family}/${sample}/abundance.tsv $dir/kallisto/all/${sample}_abundance.tsv
cp $dir/kallisto/all/${sample}_abundance.tsv $dir/kallisto/${family}/${sample}/abundance.tsv
done
done



# script to run normal mapping too 

#!/bin/bash -l
#SBATCH -J mapping
#SBATCH -o mapping_%j.log
#SBATCH -N 2
#SBATCH -c 16
#SBATCH --time=2-00:00:00
#SBATCH --partition=bigmem
#SBACTH --qos=qos-bigmem
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

# index the reference
bwa index $dir/proteins_uniq_n.fa

cat $dir/must_family_ids.tsv | while read family; do
	echo $family
	cat $dir/${family}.tsv | while read sample; do
		echo $sample

		reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
		reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

		# paired-end mapping, general command structure, adjust to your case
		bwa mem $dir/proteins_uniq_n.fa $reads1 $reads2 > $dir/mapping/uniq/${sample}_uniq.aln_pe.sam

		# fix mates and compress
		samtools sort -n -O sam $dir/mapping/uniq/${sample}_uniq.aln_pe.sam | samtools fixmate -m -O bam - $dir/mapping/uniq/${sample}_uniq.fixmate.bam

		# convert to bam file and sort
		samtools sort -O bam -o $dir/mapping/uniq/${sample}_uniq.sorted.bam $dir/mapping/uniq/${sample}_uniq.fixmate.bam

		# Once it successfully finished, delete the fixmate file and the sam file to save space
		rm $dir/mapping/uniq/${sample}_uniq.fixmate.bam
		rm $dir/mapping/uniq/${sample}_uniq.aln_pe.sam

		# get median read coverage for each gene:
		samtools index $dir/mapping/uniq/${sample}_uniq.sorted.bam
		cd $dir/mapping/uniq/
		mosdepth ${sample} $dir/mapping/uniq/${sample}_uniq.sorted.bam --use-median -t 128
	done
done



# grep non-zero entries from _abundance.tsv tables:
grep -vw "0\|tpm" * | cut -f1,4-5 |  sed 's/_abundance.tsv//g' | tr ":" "\t" | sed 's/ /\t/g'
# copy to Numbers


# ################
# JUNE 10th 2022
# ################


conda activate prodigal

cat /work/projects/archaea_neurodeg/coevolution/prodigal_genes/genomes_homo.tsv | while read genome; do 
	for f in /scratch/users/pnovikova/archaea/data/gut/coevolution/${genome}; do
	echo $f
	prodigal -i $f -d /work/projects/archaea_neurodeg/coevolution/prodigal_genes/uniq/$(basename "$f").genes \
	-a /work/projects/archaea_neurodeg/coevolution/prodigal_genes/uniq/$(basename "$f").proteins -f gff;
	done
done


# fetch nucleotide sequences by protein name
cat /work/projects/archaea_neurodeg/coevolution/prodigal_genes/proteins_homo.tsv | while read protein; do
	genome=$(echo $protein | cut -d '.' -f1)
	grep -w -A 30 $protein ${genome}.fasta.genes
done

# for some do manually
protein=GUT_GENOME265207.fasta_606
genome=$(echo $protein | cut -d '.' -f1)
grep -w -A 200 $protein ${genome}.fasta.genes

# turn to full line 
awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' /work/projects/archaea_neurodeg/host_association/prodigal_genes/proteins_homo_n.tsv > tmp && mv tmp /work/projects/archaea_neurodeg/host_association/prodigal_genes/proteins_homo_n.tsv
# copy to local just in case



# and now run kallisto

#!/bin/bash -l
#SBATCH -J kallisto
#SBATCH -o kallisto_%j.log
#SBATCH -N 2
#SBATCH -c 16
#SBATCH --time=8:00:00
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -p batch

conda activate kallisto 

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

kallisto index -i $dir/proteins_homo_n.idx $dir/proteins_homo_n.fa

cat $dir/must_family_ids.tsv | while read family; do
	echo $family
	mkdir -p $dir/kallisto/${family}
	cat $dir/${family}.tsv | while read sample; do
		echo $sample
		mkdir -p $dir/kallisto/homo/${family}/${sample}

		reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
		reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

		echo $reads1
		echo $reads2

		kallisto quant -i $dir/proteins_homo_n.idx -o $dir/kallisto/homo/${family}/${sample}/ --plaintext -t 128 $reads1 $reads2
	done
done




cat $dir/must_family_ids.tsv | while read family; do
	echo $family
	cat $dir/${family}.tsv | while read sample; do
		echo $sample
		cp $dir/kallisto/homo/${family}/${sample}/abundance.tsv $dir/kallisto/homo/all/${sample}_abundance.tsv
	done
done


# grep non-zero entries from _abundance.tsv tables:
cd $dir/kallisto/homo/all/
grep -vw "0\|tpm" * | cut -f1,4-5 |  sed 's/_abundance.tsv//g' | tr ":" "\t" | sed 's/ /\t/g'
# copy to Numbers


# ################
# JUNE 21st 2022
# ################

# ---------------------
# setup mapping for unique genes
# ---------------------

#!/bin/bash -l
#SBATCH -J mapping
#SBATCH -o mapping_%j.log
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --time=4-00:00:00
#SBATCH --partition=batch
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

# index the reference
bwa index $dir/proteins_uniq_n.fa

cat $dir/must_family_ids_v2.tsv | while read family; do
	echo $family
	cat $dir/${family}.tsv | while read sample; do
		echo $sample

		reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
		reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

		# paired-end mapping, general command structure, adjust to your case
		bwa mem $dir/proteins_uniq_n.fa $reads1 $reads2 > $dir/mapping/uniq/${sample}_uniq.aln_pe.sam

		# fix mates and compress
		samtools sort -n -O sam $dir/mapping/uniq/${sample}_uniq.aln_pe.sam | samtools fixmate -m -O bam - $dir/mapping/uniq/${sample}_uniq.fixmate.bam

		# convert to bam file and sort
		samtools sort -O bam -o $dir/mapping/uniq/${sample}_uniq.sorted.bam $dir/mapping/uniq/${sample}_uniq.fixmate.bam

		# Once it successfully finished, delete the fixmate file and the sam file to save space
		rm $dir/mapping/uniq/${sample}_uniq.fixmate.bam
		rm $dir/mapping/uniq/${sample}_uniq.aln_pe.sam

		# get median read coverage for each gene:
		samtools index $dir/mapping/uniq/${sample}_uniq.sorted.bam
		cd $dir/mapping/uniq/
		mosdepth ${sample} $dir/mapping/uniq/${sample}_uniq.sorted.bam --use-median -t 128
	done
done



# ---------------------
# setup mapping for homo genes
# ---------------------

#!/bin/bash -l
#SBATCH -J mapping_h
#SBATCH -o mapping_h_%j.log
#SBATCH -N 1
#SBATCH -c 16
#SBATCH -n 88
#SBATCH --time=14-00:00:00
#SBATCH --partition=batch
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

# index the reference
bwa index $dir/proteins_homo_n.fa

cat $dir/must_family_ids.tsv | while read family; do
	echo $family
	cat $dir/${family}.tsv | while read sample; do
		echo $sample

		reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
		reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

		mkdir -p $dir/mapping/homo/

		# paired-end mapping, general command structure, adjust to your case
		bwa mem $dir/proteins_homo_n.fa $reads1 $reads2 > $dir/mapping/homo/${sample}.aln_pe.sam

		# fix mates and compress
		samtools sort -n -O sam $dir/mapping/homo/${sample}.aln_pe.sam | samtools fixmate -m -O bam - $dir/mapping/homo/${sample}.fixmate.bam

		# convert to bam file and sort
		samtools sort -O bam -o $dir/mapping/homo/${sample}.sorted.bam $dir/mapping/homo/${sample}_homo.fixmate.bam

		# Once it successfully finished, delete the fixmate file and the sam file to save space
		rm $dir/mapping/homo/${sample}.fixmate.bam
		rm $dir/mapping/homo/${sample}.aln_pe.sam

		# get median read coverage for each gene:
		samtools index $dir/mapping/homo/${sample}.sorted.bam
		cd $dir/mapping/homo/
		mosdepth ${sample} $dir/mapping/homo/${sample}.sorted.bam --use-median -t 128
	done
done


# ---------------------
# HGT analysis
# ---------------------


/work/projects/archaea_neurodeg/host_association/prodigal_genes/uniq/GUT_GENOME066427*

/scratch/users/pnovikova/archaea/data/gut/genomes/GUT_GENOME066427.fasta  




# HOW TO CHECK FROM WHICH CONTIG THE GENE COMES FROM

conda activate blast 

# db is where i will be lookig for
makeblastdb -in /scratch/users/pnovikova/archaea/data/gut/genomes/GUT_GENOME066427.fasta -dbtype nucl
blastn -query u1.seq.fna -db /scratch/users/pnovikova/archaea/data/gut/genomes/GUT_GENOME066427.fasta -out u1.seq.txt

# to see which contig it is:
grep ">" u1.seq.txt 



# ################
# JUNE 22nd 2022
# ################

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis

# collect M.smithii genomes

# from GUT dataset:
cat /scratch/users/pnovikova/archaea/data/raw/gut/genomes-all_metadata.tsv | cut -f1,19 | grep "smithii" > $dir/taxonomy_m.smithii_gut.tsv

# from GEM dataset:
cat /scratch/users/pnovikova/archaea/data/raw/gem/genome_metadata.tsv | cut -f1,15 | grep "GCF_000016525.1\|GCF_002252585.1" > $dir/taxonomy_m.smithii_gem.tsv

cut -f1 $dir/taxonomy_m.smithii_gem.tsv | while read genome; do
	cp /scratch/users/pnovikova/archaea/data/gem/consistent_genomes/$genome.fasta /work/projects/archaea_neurodeg/host_association/genomes_m.smithii/
done


conda activate metachip

input_genomes=/work/projects/archaea_neurodeg/host_association/genomes_m.smithii
output=/work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis/metachip
taxon=/work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis/taxonomy_m.smithii_gem.tsv

MetaCHIP PI -i $input_genomes -o $output -r s -x fasta -taxon $taxon -t 8 -p gem
MetaCHIP BP -p gem -r s -t 8

# makeblastdb,blastn,prodigal,hmmsearch,hmmfetch,hmmalign,hmmstat,FastTree not detected, program exited!
conda install -c bioconda blast
conda install -c bioconda prodigal
conda install -c bioconda hmmer 
conda install -c bioconda fasttree 
conda install -c bioconda mafft 



# try for GUT genomes 

cut -f1 $dir/taxonomy_m.smithii_gut.tsv | while read genome; do
	cp /scratch/users/pnovikova/archaea/data/gut/genomes/$genome.fasta /work/projects/archaea_neurodeg/host_association/genomes_m.smithii_gut/
done

input_genomes=/work/projects/archaea_neurodeg/host_association/genomes_m.smithii_gut/
output=/work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis/metachip/
taxon=/work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis/taxonomy_m.smithii_gut.tsv

MetaCHIP PI -i $input_genomes -o $output -r s -x fasta -taxon $taxon -t 8 -p GUT
MetaCHIP BP -p GUT -r s -t 8


/home/users/pnovikova/miniconda3/yaml/METACHIP.yaml

conda env update --file METACHIP.yaml


conda env update -n metachip_susheel --file METACHIP.yaml


conda install -c conda-forge biopython=1.79
conda install -c biocore blast-plus=2.2.31
conda install -c conda-forge ete3=3.1.2
conda install -c bioconda fasttree=2.1.10
conda install -c bioconda hmmer=3.3
conda install -c bioconda mafft=7.467
conda install -c conda-forge matplotlib=3.2.1
conda install -c conda-forge matplotlib-base=3.2.1
conda install -c anaconda numpy
conda install -c anaconda pango=1.42.4
conda install -c anaconda pip=20.1.1
conda install -c bioconda prodigal=2.6.3
conda install -c conda-forge python=3.8.3
conda install -c conda-forge r-ape=5.4
conda install -c conda-forge r-base=4.0.0
conda install -c conda-forge r-circlize=0.4.9
conda install -c r r-colorspace=1.4_1
conda install -c conda-forge r-getopt=1.20.3
conda install -c bioconda/label/cf201901 r-globaloptions
conda install -c r r-lattice=0.20_41
conda install -c conda-forge r-nlme=3.1_148
conda install -c conda-forge r-optparse=1.6.6
conda install -c conda-forge r-rcpp=1.0.4.6
conda install -c conda-forge r-shape=1.4.4
conda install -c anaconda readline=8.0
conda install -c anaconda scipy=1.4.1
conda install -c conda-forge sed=4.7


sed -i '1 i\user_genome\tclassification' /work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis/taxonomy_m.smithii_gut.tsv

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis

MetaCHIP PI -i genomes_msmithii_gut/ -o output/ -x fasta -g $dir/taxonomy_msmithii_gut_groupping.txt -p msmithii
MetaCHIP PI -i genomes_msmithii_gut/ -x fasta -g taxonomy_msmithii_gut_groupping.txt -p gut
MetaCHIP BP -p gut 

# ################
# JUNE 24th 2022
# ################

conda env create --name envname --file=environments.yml
conda env create --name mapping --file=/work/projects/archaea_neurodeg/conda_envs/mapping.yml --


conda config --append channels bioconda
conda config --append channels defaults





conda create -n metachip python=3.8


# Susheel's verion doesnt work :( 
genomes = "genomes"

rule all:
    input:
        expand("output/genomes_MetaCHIP_wd/genomes_g_blastn_commands.txt", genome=genomes)
rule metachip:
    input:
        gen = "genomes",  
        taxonomy = "taxonomy_msmithii_gem_gtdbtk_test.tsv"
    output:
        metachip="output/genomes_MetaCHIP_wd/genomes_g_blastn_commands.txt"
    shell:
        """
        date &&
        MetaCHIP PI -p genomes -r s -i {input.gen} -x fasta -taxon {input.taxonomy} -o $(dirname {output.metachip}) -force &&
        MetaCHIP BP -p genomes -r s -o $(dirname {output.metachip}) -force &&
        date
        """

# output
Building DAG of jobs...
Job stats:
job         count    min threads    max threads
--------  -------  -------------  -------------
all             1              1              1
metachip        1              1              1
total           2              1              1


[Fri Jun 24 23:51:38 2022]
rule metachip:
    input: genomes_msmithii_gem, taxonomy/taxonomy_msmithii_gem_gtdbtk.tsv
    output: output/genomes_msmithii_gem_MetaCHIP_wd/genomes_msmithii_gem_g_blastn_commands.txt
    jobid: 1
    reason: Missing output files: output/genomes_msmithii_gem_MetaCHIP_wd/genomes_msmithii_gem_g_blastn_commands.t
xt
    wildcards: genome=genomes_msmithii_gem
    resources: tmpdir=/tmp

WorkflowError:
Error when formatting '
        date &&
        MetaCHIP PI -p {wildcards.genomes} -r s -i {input.gen} -x fasta -taxon {input.taxonomy} -o $(dirname {outp
ut.metachip}) -force &&
        MetaCHIP BP -p {wildcards.genomes} -r s -o $(dirname {output.metachip}) -force &&
        date
        ' for rule metachip. 'Wildcards' object has no attribute 'genomes'



# ################
# JUNE 27th 2022
# ################

# The correct Snakemake file for running MetaCHIP is here:

genomes = "genomes"                                                                                                                  
                         
rule all:    
    input:                    
        expand("output/genomes_MetaCHIP_wd/genomes_g_blastn_commands.txt", genome=genomes)
rule metachip:
    input:
        gen = "genomes",                                                         
        taxonomy = "taxonomy_msmithii_gem_gtdbtk_test.tsv"                 
    output:                                                                                  
        metachip="output/genomes_MetaCHIP_wd/genomes_g_blastn_commands.txt"
    shell:                         
        """                                                                                  
        date &&                                              
        MetaCHIP PI -p genomes -r s -i {input.gen} -x fasta -taxon {input.taxonomy} -o $(dirname {output.metachip}) -force &&
        MetaCHIP BP -p genomes -r s -o $(dirname {output.metachip}) -force &&
        date                                                                               
        """      


# set up a job

#!/bin/bash -l
#SBATCH -J hgt
#SBATCH -o hgt_%j.log
#SBATCH -N 1
#SBATCH -c 128
#SBATCH --time=14-00:00:00
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate metachip

cd /work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis

snakemake -rp --cores 128

# Snakefile:
genomes = "genomes"                                

rule all:    
    input:                    
        expand("output/genomes_MetaCHIP_wd/genomes_g_blastn_commands.txt", genome=genomes)
rule metachip:
    input:
        gen = "genomes",                                                         
        taxonomy = "taxonomy_msmithii_gem_gtdbtk_test.tsv"                 
    output:                                                                                  
        metachip="output/genomes_MetaCHIP_wd/genomes_g_blastn_commands.txt"
    shell:                         
        """                                                                                  
        date &&                                              
        MetaCHIP PI -p genomes -r s -i {input.gen} -x fasta -taxon {input.taxonomy} -o $(dirname {output.metachip}) -force -t 128 &&
        MetaCHIP BP -p genomes -r s -o $(dirname {output.metachip}) -force -t 128 &&
        date                                                                               
        """      


# ################
# JUNE 28th 2022
# ################

# reschedule script_mapping_homo.sh

#!/bin/bash -l
#SBATCH -J mapping_h
#SBATCH -o mapping_h_%j.log
#SBATCH -N 1
#SBATCH -c 128
#SBATCH --time=14-00:00:00
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
temp=/work/projects/archaeome/coevolution/mapping_h
cd $dir

# index the reference
bwa index $dir/proteins_homo_n.fa

cat $dir/must_family_ids.tsv | while read family; do
        echo $family
        cat $dir/${family}.tsv | while read sample; do
                echo $sample

                reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
                reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

                mkdir -p $dir/mapping/homo/

                # paired-end mapping, general command structure, adjust to your case
                bwa mem $dir/proteins_homo_n.fa $reads1 $reads2 -t 128 > $temp/${sample}.aln_pe.sam

                # fix mates and compress
                samtools sort -n --threads 128 -O sam $temp/${sample}.aln_pe.sam | samtools fixmate -m --threads 128 -O bam - $temp/${sample}.fixmate.bam

                # convert to bam file and sort
                samtools sort --threads 128 -O bam -o $temp/${sample}.sorted.bam $temp/${sample}_homo.fixmate.bam

                # Once it successfully finished, delete the fixmate file and the sam file to save space
                rm $temp/${sample}.fixmate.bam
                rm $temp/${sample}.aln_pe.sam

                # get median read coverage for each gene:
                samtools index $temp/${sample}.sorted.bam
                cd $dir/mapping/homo/
                mosdepth ${sample} $temp/${sample}.sorted.bam --use-median -t 128
        done
done




/work/projects/archaea_neurodeg/host_association/hgt_analysis/output/genomes_MetaCHIP_wd
$ cp -R /work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis/output/genomes_MetaCHIP_wd/* ./                                      



done du -sh genomes_s_blastdb 1.9G                 
done du -sh genomes_s_blastn_results 70G 
done du -sh genomes_s_blastn_results_filtered_al200bp_cov75 56G
done du -sh genomes_s_get_SCG_tree_wd 559M
du -sh genomes_HGT_ip90_al200bp_c75_ei80_f10kbp_s6 1.2T






/work/projects/archaeome/coevolution/hgt_analysis

#!/bin/bash -l
#SBATCH -J copying
#SBATCH -o copying_%j.log
#SBATCH -N 1
#SBATCH -c 128
#SBATCH --time=2-00:00:00
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu


source=/work/projects/ecosystem_biology/archaea/coevolution/analysis/hgt_analysis/output/
dest=/work/projects/archaeome/coevolution/hgt_analysis/output/
rsync -a $source $dest


#!/bin/bash -l
#SBATCH -J mapping_h
#SBATCH -o mapping_h_%j.log
#SBATCH -N 1
#SBATCH -c 128
#SBATCH --time=14-00:00:00
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
temp=/work/projects/archaeome/coevolution/mapping_h

cd $temp

# index the reference
bwa index $dir/proteins_homo_n.fa

date 

cat $dir/must_family_ids.tsv | while read family; do
echo $family
cat $dir/${family}.tsv | while read sample; do
echo $sample
date

reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

mkdir -p $dir

# paired-end mapping, general command structure, adjust to your case
bwa mem $dir/proteins_homo_n.fa $reads1 $reads2 -t 128 > $temp/${sample}.aln_pe.sam

# fix mates and compress
samtools sort -n -O sam --threads 128 $temp/${sample}.aln_pe.sam | samtools fixmate -m --threads 128 --output-fmt bam - $temp/${sample}.fixmate.bam

# convert to bam file and sort
samtools sort --threads 128 -O bam -o $temp/${sample}.sorted.bam $temp/${sample}.fixmate.bam

# Once it successfully finished, delete the fixmate file and the sam file to save space
rm $temp/${sample}.fixmate.bam
rm $temp/${sample}.aln_pe.sam

# get median read coverage for each gene:
samtools index $temp/${sample}.sorted.bam
cd $dir/mapping/homo
mosdepth ${sample} $temp/${sample}.sorted.bam --use-median -t 128

date 

done
done



# ################
# JUNE 29th 2022
# ################



M11-04-V1
M11-04-V2
M11-04-V3
M11-05-V1
M11-05-V2
M11-05-V3
M11-06-V1
M11-06-V2
M11-06-V3
M11-07-V1
M11-07-V2
M11-07-V3

#!/bin/bash -l
#SBATCH -J mapping
#SBATCH -o mapping_%j.log
#SBATCH -N 4
#SBATCH -c 28
#SBATCH --time=6-00:00:00
#SBATCH --partition=batch
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

# index the reference
bwa index $dir/proteins_uniq_n.fa

cat $dir/must_family_ids_v3.tsv | while read family; do
        echo $family
        cat $dir/${family}_continue11.tsv | while read sample; do
                echo $sample

                reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
                reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

                # paired-end mapping, general command structure, adjust to your case
                bwa mem $dir/proteins_uniq_n.fa $reads1 $reads2 -t 112 > $dir/mapping/uniq/${sample}_uniq.aln_pe.sam

                # fix mates and compress
                samtools sort -n --threads 112 -O sam $dir/mapping/uniq/${sample}_uniq.aln_pe.sam | samtools fixmate -m --threads 112 -O bam - $dir/mapping/uniq/${sample}_uniq.fixmate.bam

                # convert to bam file and sort
                samtools sort --threads 112 -O bam -o $dir/mapping/uniq/${sample}_uniq.sorted.bam $dir/mapping/uniq/${sample}_uniq.fixmate.bam

                # Once it successfully finished, delete the fixmate file and the sam file to save space
                rm $dir/mapping/uniq/${sample}_uniq.fixmate.bam
                rm $dir/mapping/uniq/${sample}_uniq.aln_pe.sam

                # get median read coverage for each gene:
                samtools index $dir/mapping/uniq/${sample}_uniq.sorted.bam
                cd $dir/mapping/uniq/
                mosdepth ${sample} $dir/mapping/uniq/${sample}_uniq.sorted.bam --use-median -t 112
        done
done



# Fetch expressed genes from MT reads, uniq 

for file in *.mosdepth.summary.txt; do 
	# echo $file | cut -d. -f1-2  
	echo $(basename "${file}" .mosdepth.summary.txt)
	tail -n +2 $file | head -n -1
done



conda install -c jmcmurray os

conda install -c conda-forge matplotlib
conda install -c anaconda scipy natsort


# ################
# JUNE 30th 2022
# ################


srun -N 1 -c 128 -p interactive -t 2:00:00 --pty bash 


ls -lht /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/homo/
ls -lht /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/uniq/ | head
ls -lht /work/projects/archaea_neurodeg/host_association/hgt_analysis/output/genomes_MetaCHIP_wd/
cat condahgt_282349.log 


output/genomes_MetaCHIP_wd/genomes_g_blastn_commands.txt - CHANGE TO S genomes_s_blastn_commands.txt

ls /scratch/users/pnovikova/test_metachip/container/metachip.sif

module load tools/Singularity

singularity exec /scratch/users/pnovikova/test_metachip/container/metachip.sif snakemake --cores 128 -pr


singularity exec -B .:/ container/metachip.sif snakemake --cores 128 -prn



singularity run --home /scratch/users/pnovikova/test_metachip/ /scratch/users/pnovikova/test_metachip/container/metachip.sif snakemake --cores 128 -pr -s Snakefile



# ################
# JULY 4TH 2022
# ################

# setup metachip running on 1k genomes using conda env 

cd /work/projects/archaea_neurodeg/host_association/hgt_analysis/

# MT STUFF

# Fetch expressed genes from MT reads, uniq 

for file in *.mosdepth.summary.txt; do 
# echo $file | cut -d. -f1-2  
# echo $(basename "${file}" .mosdepth.summary.txt)
sample=$(basename "${file}" .mosdepth.summary.txt)
tail -n +2 $file | head -n -1 | awk '$1="$(basename "${file}" .mosdepth.summary.txt)""\t"$1'
done



# ################
# JULY 8TH 2022
# ################

/scratch/users/pnovikova/archaea/data/raw/gut/genomes-all_metadata.tsv
/scratch/users/pnovikova/archaea/data/raw/gem/genome_metadata.tsv

/work/projects/archaeome/coevolution/GtoThree/genomes_list.tsv - full path

/work/projects/archaea_neurodeg/coevolution/hgt_analysis/ - only names 

/work/projects/archaea_neurodeg/coevolution/hgt_analysis/output_new_aion/genomes_MetaCHIP_wd/genomes_s6_HGTs_only_id_groupped_parsed.txt

/work/projects/archaea_neurodeg/coevolution/hgt_analysis/adjacency_matrix_4_circos_by_groups.tsv


# ################
# JULY 11TH 2022
# ################

# fetch nucleotide sequences by protein name
cat /work/projects/archaea_neurodeg/host_association/prodigal_genes/proteins_homo.tsv | while read protein; do
	genome=$(echo $protein | cut -d '.' -f1)
	grep -w -A 30 $protein ${genome}.fasta.genes
done

# for some do manually
protein=GUT_GENOME079980.fasta_1452
genome=$(echo $protein | cut -d '.' -f1)
grep -w  $protein ${genome}*


/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/gut - 'prodigal files'
/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta - 'uniq proteins'


cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta | grep ">" | sed 's/>//g' | while read protein; do 
  genome=$(echo $protein | cut -d '.' -f1)
  echo $genome 
	grep -w -A 30 $protein /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/gut/${genome}.fasta.proteins | grep ">" 
done

/work/projects/archaeome/coevolution/prodigal/uniq

genome=GUT_GENOME079980.fasta
prodigal -i $genome -f sco -o ${genome}.sco
prodigal -i $genome -f gbk -o ${genome}.gbk

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta | grep ">" | sed 's/>//g' | while read protein; do 
  genome=$(echo $protein | cut -d '.' -f1)
  echo $genome
  exact_protein=$(echo $protein | cut -d '_' -f3)
  echo $exact_protein
  prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta -f gbk -o ${genome}.gbk
  grep -B 11 -A 10 --color _$exact_protein ${genome}.gbk > selective_${genome}.gbk
done

head -2 file1 && cat file2 && tail -1 file1

# JUPYTER

head -2 GUT_GENOME068037.gbk && cat selective_GUT_GENOME068037.gbk && tail -1 GUT_GENOME068037.gbk  > tmp && mv tmp selective_GUT_GENOME068037.gbk

echo -e "${top}\n$(cat selective_GUT_GENOME068037.gbk)" | sed -e '$a//' > tmp 

# manual check
grep -A 5 -B 5 --color "1452" GUT_GENOME079980.fasta.sco | sed 's/_/\t/g' | sed 's/>//g' | grep -A 5 -B 5 --color "1452"


# ################
# JULY 12TH 2022
# ################

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta | grep ">" | sed 's/>//g' | while read protein; do 
  genome=$(echo $protein | cut -d '.' -f1)
  echo $genome
  exact_protein=$(echo $protein | cut -d '_' -f3)
  echo $exact_protein
  # prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta -f sco -o ${genome}.sco
  grep -A 5 -B 5 ">$exact_protein" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | grep -A 5 -B 5 "$exact_protein" | sed 's/+/1/g' | sed 's/-/-1/g' > df_${genome}.tsv 
done

# create gbk files

genome=/scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME079980.fasta

prokka $genome --kingdom Archaea

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta | grep ">" | sed 's/>//g' | while read protein; do 
  genome=$(echo $protein | cut -d '.' -f1)
  echo $genome
  prokka /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta --kingdom Archaea --prefix ${genome}_
done


grep -A 50 -B 50 ">565" GUT_GENOME088140.sco
grep -A 50 -B 50 ">1878" GUT_GENOME086893.sco
grep -A 50 -B 50 ">1452" GUT_GENOME079980.sco
grep -A 50 -B 50 ">1194" GUT_GENOME068037.sco

GUT_GENOME079980
1452
GUT_GENOME068037
1194
GUT_GENOME088140
565
GUT_GENOME086893
1878

# metadata 
/scratch/users/pnovikova/archaea/data/raw/gut/genomes-all_metadata.tsv

grep "GUT_GENOME079980\|GUT_GENOME068037\|GUT_GENOME088140\|GUT_GENOME086893"  genomes-all_metadata.tsv | cut -f1,8-10,22-23 | sed '1 i\Genome\tGC_content\tCompleteness\tContamination\tCountry\tContinent'

# run prokka to produce GBK files 
/work/projects/archaeome/coevolution/prodigal/homo

conda activate prokka 

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_homological_clusters/h32.fasta | grep ">" | sed 's/>//g' | while read protein; do 
  genome=$(echo $protein | cut -d '.' -f1)
  echo $genome
  prokka /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta --kingdom Archaea --prefix ${genome}
done

# fix the first line 
for f in *gbk; do
  head $f
  sed -i 's/.fasta/.fasta /g' $f 
  head $f
done

# produce the .sco files to check for coordinates
conda activate prodigal 

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_homological_clusters/h32.fasta | grep ">" | sed 's/>//g' | while read protein; do 
  genome=$(echo $protein | cut -d '.' -f1)
  echo $genome
  exact_protein=$(echo $protein | cut -d '_' -f3)
  echo $exact_protein
  prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta -f sco -o ${genome}.sco
  grep -A 50 -B 50 ">${exact_protein}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | grep -A 50 -B 50 "$exact_protein" | sed 's/+/1/g' | sed 's/-/-1/g' > df_${genome}.tsv 
done


cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_homological_clusters/h32.fasta | grep ">" | sed 's/>//g' | while read protein; do
  genome=$(echo $protein | cut -d '.' -f1)
  echo $genome
  head -1 df_${genome}.tsv && tail -1 df_${genome}.tsv
done

# check the metadata 

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_homological_clusters/h32.fasta | grep ">" | sed 's/>//g' | while read protein; do
  genome=$(echo $protein | cut -d '.' -f1)
  grep "$genome" /scratch/users/pnovikova/archaea/data/raw/gut/genomes-all_metadata.tsv | cut -f1,8-10,22-23
done | sed '1 i\Genome\tGC_content\tCompleteness\tContamination\tCountry\tContinent'

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta | grep ">" | sed 's/>//g' | while read protein; do
  genome=$(echo $protein | cut -d '.' -f1)
  grep "$genome" /scratch/users/pnovikova/archaea/data/raw/gut/genomes-all_metadata.tsv | cut -f1,8-10,22-23
done | sed '1 i\Genome\tGC_content\tCompleteness\tContamination\tCountry\tContinent'



cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_homological_clusters/h32.fasta | grep ">" | sed 's/>//g' | while read protein; do
  genome=$(echo $protein | cut -d '.' -f1)
  awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta > ${genome}_lines.fasta
  cat ${genome}_lines.fasta >> h32.fasta 
done


# ################
# JULY 13TH 2022
# ################
cd /work/projects/archaeome/coevolution/prodigal/uniq/

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta | grep ">" | sed 's/>//g' | while read protein; do
genome=$(echo $protein | cut -d '.' -f1);   
echo $genome;   
head -1 df_${genome}.tsv && tail -1 df_${genome}.tsv; done
done

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_selective/arch_uncommon_kegg_ids/4.fasta | grep ">" | sed 's/>//g' | while read protein; do
  genome=$(echo $protein | cut -d '.' -f1);   
  echo $genome;
  exact_protein=$(echo $protein | cut -d '_' -f3)
  # echo $exact_protein
  grep ">${exact_protein}" $genome.sco 
done



grep -A 50 -B 50 ">565" GUT_GENOME088140.sco
grep -A 50 -B 50 ">1878" GUT_GENOME086893.sco
grep -A 50 -B 50 ">1452" GUT_GENOME079980.sco
grep -A 50 -B 50 ">1194" GUT_GENOME068037.sco

1. extract 5 genes around (produce .sco and then do grep -A 5 -B 5 around $exact_protein)
2. go to the jupyter notebook and run a script to read the data frame and depict the gene sequence
3. read the annotation file and extract annotations 

# # for the ease of computation
# cp 3300005692_2.fasta /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/
# cp 3300013744_1.fasta /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/
# cp 3300029340_36.fasta /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/
# cp 3300029381_47.fasta /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/
# cp 3300029719_42.fasta /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/

conda activate prodigal 

dir=/work/projects/archaeome/coevolution/synteny/uniq
mkdir -p $dir/sco/
for protein_cluster in /work/projects/archaeome/coevolution/synteny/uniq/protein_clusters/unc_1.fasta; do
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
    if [[ $protein == GUT* ]];
    then
      genome=$(echo $protein | cut -d '.' -f1)
      echo $genome
    else
      genome=$(echo $protein | cut -d '_' -f1,2)
      echo $genome
    fi
    exact_protein=$(echo $protein | cut -d '_' -f3)
    echo $exact_protein
    mkdir -p $dir/sco/$(basename "${protein_cluster}" .fasta)
    prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta -f sco -o $dir/sco/$(basename "${protein_cluster}" .fasta)/${genome}.sco
    sed -ie '1,2d' 
    cd $dir/sco/$(basename "${protein_cluster}" .fasta)
    grep -A 5 -B 5 ">${exact_protein}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | sed 's/+/1/g' | sed 's/-/-1/g' > df_${genome}.tsv 
    done
done
      

# ################
# JULY 14TH 2022
# ################

/work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv - `annotations of archaeal proteins`

dir=/work/projects/archaeome/coevolution/synteny/uniq
for protein_cluster in /work/projects/archaeome/coevolution/synteny/uniq/protein_clusters/unc_9.fasta; do
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
  if [[ $protein == GUT* ]];
    then
      genome=$(echo $protein | cut -d '.' -f1)
      echo $genome
    else
      genome=$(echo $protein | cut -d '_' -f1,2)
      echo $genome
  fi
  exact_protein=$(echo $protein | cut -d '_' -f3)
  # grep "${protein}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv
  grep -A 5 -B 5 ">${exact_protein}_" $dir/sco/$(basename "${protein_cluster}" .fasta)/${genome}.sco > tmp.tsv 
  cut -d '_' -f1 tmp.tsv | sed 's/>//g' > tmp && mv tmp tmp.tsv
  cat tmp.tsv | while read number ; do
    echo $()
  break
  done
done

# Extract KO annotations

dir=/work/projects/archaeome/coevolution/synteny/uniq
for protein_cluster in /work/projects/archaeome/coevolution/synteny/uniq/protein_clusters/*.fasta; do
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
  if [[ $protein == GUT* ]];
    then
      genome=$(echo $protein | cut -d '.' -f1)
      # echo $genome
    else
      genome=$(echo $protein | cut -d '_' -f1,2)
      # echo $genome
  fi
  exact_protein=$(echo $protein | cut -d '_' -f3)
  # echo $protein
  
  start=$(($exact_protein-5))
  end=$(($exact_protein+5))

  for i in $(seq $start $end); do
    if [[ $protein == GUT* ]];
      then
        if grep -q "${genome}.fasta_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv
          then 
            grep -m 1 "${genome}.fasta_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv | head -1
          else 
            printf "${genome}.fasta_${i}\tNone\tnot found\n"
        fi
      else
        if grep -q "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv
          then 
            grep -m 1 "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv | head -1
          else 
            printf "${genome}_${i}\tNone\tnot found\n"
        fi
      fi
    done | cut -d '_' -f3 > $dir/sco/$(basename "${protein_cluster}" .fasta)/KO_${genome}.tsv
  done
done 


# ################
# JULY 15TH 2022
# ################

cd /work/projects/archaeome/coevolution/synteny/uniq

dir=/work/projects/archaeome/coevolution/synteny/uniq
for protein_cluster in /work/projects/archaeome/coevolution/synteny/uniq/protein_clusters/*.fasta; do
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
  if [[ $protein == GUT* ]];
    then
      genome=$(echo $protein | cut -d '.' -f1)
      echo $genome
    else
      genome=$(echo $protein | cut -d '_' -f1,2)
      echo $genome
  fi
  exact_protein=$(echo $protein | cut -d '_' -f3)
  echo $protein
  
  start=$(($exact_protein-5))
  end=$(($exact_protein+5))

  for i in $(seq $start $end); do
    if [[ $protein == GUT* ]];
      then
        if grep -w -q "${genome}.fasta_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv
          then 
            grep -w -m 1 "${genome}.fasta_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv | head -1
          else 
            printf "${genome}.fasta_${i}\tNone\tnot found\n"
        fi
      else
        if grep -w -q "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv
          then 
            grep -w -m 1 "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv | head -1
          else 
            printf "${genome}_${i}\tNone\tnot found\n"
        fi
      fi
    done | cut -d '_' -f3 > $dir/sco/$(basename "${protein_cluster}" .fasta)/KO_${genome}.tsv
  done
done 







dir=/work/projects/archaeome/coevolution/synteny/homo
for protein_cluster in /work/projects/archaeome/coevolution/synteny/homo/protein_clusters/*.fasta; do
  echo $(basename "$protein_cluster" .fasta)
  protein=$(basename "$protein_cluster" .fasta)
  find $dir/sco/$protein -size 0 -print -delete
done



# ################
# AUGUST 24TH 2022
# ################

# check those genomes having many hgts:

cd /scratch/users/pnovikova/archaea/data/raw/gut
cat /home/users/pnovikova/list_gut | while read mag; do grep $mag /scratch/users/pnovikova/archaea/data/raw/gut/genomes-all_metadata.tsv; done | cut -f1,22,23
cat /home/users/pnovikova/list_gem | while read mag; do grep $mag /scratch/users/pnovikova/archaea/data/raw/gem/genome_metadata.tsv; done | cut -f1,19,20


# exchanging >=10 regions with >=100 other MAGs: 
GUT_GENOME087659
GUT_GENOME090306
GUT_GENOME090886
GUT_GENOME258262
GUT_GENOME284192	

3300029120_22
3300029340_36
3300029381_47
3300029385_28
3300029388_24
3300029435_39	
3300029494_32
3300029552_18
3300029594_29
3300029609_30
3300029684_32	
3300029688_21
3300029718_45
3300029719_42
3300029736_28
3300029738_29
3300029790_36
3300029840_29
3300029843_22
3300029851_24
3300029857_43
3300029881_35


# ################
# AUGUST 31st 2022
# ################

Redo MT read mapping for arch and bac sequences from protein clusters (for homo close homology only):

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis

# first run prodigal on 

conda activate prodigal

cat /work/projects/archaea_neurodeg/coevolution/prodigal_genes/genomes_homo_bac.tsv | while read genome; do 
for f in /scratch/users/pnovikova/bacteria/data/genomes_gut/${genome}; do
echo $f
prodigal -i $f -d /work/projects/archaea_neurodeg/coevolution/prodigal_genes/homo_bac/$(basename "$f").genes \
-a /work/projects/archaea_neurodeg/coevolution/prodigal_genes/homo_bac/$(basename "$f").proteins -f gff;
done
done

# fetch nucleotide sequences by protein name
cd /work/projects/archaea_neurodeg/coevolution/prodigal_genes/homo_bac/
cat /work/projects/archaea_neurodeg/coevolution/prodigal_genes/proteins_homo_bac.tsv | while read protein; do
	genome=$(echo $protein | cut -d_ -f1,2)
  echo $genome 
	grep -w -A 30 $protein ${genome}.fasta.genes
done

# run mapping


#!/bin/bash -l
#SBATCH -J mapping_h_f
#SBATCH -o mapping_h_filtered_%j.log
#SBATCH -N 2
#SBATCH -n 128
#SBATCH --time=14-00:00:00
#SBATCH --partition=batch
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

# index the reference
bwa index $dir/proteins_homo_arc_bac_n.fa

cat $dir/must_family_ids.tsv | while read family; do
	echo $family
	cat $dir/${family}.tsv | while read sample; do
		echo $sample

		reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
		reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

		mkdir -p $dir/mapping/homo_arc_bac_filtered/

		# paired-end mapping, general command structure, adjust to your case
		bwa mem -t 256 $dir/proteins_homo_arc_bac_n.fa $reads1 $reads2 > $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam

		# fix mates and compress
		samtools sort --threads 256 -n -O sam $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam | samtools fixmate --threads 256 -m -O bam - $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam

		# convert to bam file and sort
		samtools sort --threads 256 -O bam -o $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam

		# Once it successfully finished, delete the fixmate file and the sam file to save space
		rm $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam
		rm $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam

		# get median read coverage for each gene:
		samtools index $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam
		cd $dir/mapping/homo_arc_bac_filtered/
		mosdepth ${sample} $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam --use-median -t 256
	done
done



conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

family=must_m_08
sample=M08-01-V1

reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

mkdir -p $dir/mapping/homo_arc_bac_filtered/

# paired-end mapping, general command structure, adjust to your case
bwa mem $dir/proteins_homo_arc_bac_n.fa $reads1 $reads2 > $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam

# fix mates and compress
samtools sort --threads 256 -n -O sam $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam | samtools fixmate --threads 256 -m -O bam - $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam

# convert to bam file and sort
samtools sort --threads 256 -O bam -o $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam

# Once it successfully finished, delete the fixmate file and the sam file to save space
rm $dir/mapping/homo_arc_bac_filtered/${sample}.fixmate.bam
rm $dir/mapping/homo_arc_bac_filtered/${sample}.aln_pe.sam

# get median read coverage for each gene:
samtools index $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam
cd $dir/mapping/homo_arc_bac_filtered/
mosdepth ${sample} $dir/mapping/homo_arc_bac_filtered/${sample}.sorted.bam --use-median -t 256
done
done




# ################
# September 7th
# ################

# MT STUFF

# Fetch expressed genes from MT reads,  homo filtered

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/homo_arc_bac_filtered/
cd $dir

for file in *.mosdepth.summary.txt; do 
# echo $file | cut -d. -f1-2  
# echo $(basename "${file}" .mosdepth.summary.txt)
sample=$(basename "${file}" .mosdepth.summary.txt)
tail -n +2 $file | head -n -1 | awk -v var="${sample}" 'BEGIN{OFS="\t"}$1='var'"\t"$1'
done | grep "_a" | sed '1 i\sample\tchrom\tlength\tbases\tmean\tmin\tmax' >  /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/gene_expression/gene_expression_homo_arch_filtered.tsv

cd  /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/gene_expression/

# now go to MT_gene_expression_BWAMEM.ipynb and copy the expression for selective genes to /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/2plot_expression_homo_arch_filtered.tsv
# make a plot in MG + MT abundance.ipynb

# just check for bac gene expression
dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/homo_arc_bac_filtered/
cd $dir

for file in *.mosdepth.summary.txt; do 
# echo $file | cut -d. -f1-2  
# echo $(basename "${file}" .mosdepth.summary.txt)
sample=$(basename "${file}" .mosdepth.summary.txt)
tail -n +2 $file | head -n -1 | awk -v var="${sample}" 'BEGIN{OFS="\t"}$1='var'"\t"$1'
done | grep "_b" | sed '1 i\sample\tchrom\tlength\tbases\tmean\tmin\tmax' >  /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/gene_expression/gene_expression_homo_bac_filtered.tsv


# ################
# September 26th
# ################

# trying to find how many protein clusters have <2 (arc) and <10 (bac) annotated proteins




# ################
# September 28th?
# ################

/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1233_10/run1/Analysis/annotation/mg.KEGG.counts.tsv -> annotation
/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1233_10/run1/Analysis/taxonomy/kraken/* -> kraken
/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1233_10/run1/Analysis/mg.assembly.contig_depth.txt -> Analysis



sample=1237_18

mkdir -p /Users/polina.novikova/Documents/phd/mibipa/imp_output
mkdir -p /Users/polina.novikova/Documents/phd/mibipa/imp_output/$sample
mkdir -p /Users/polina.novikova/Documents/phd/mibipa/imp_output/$sample/kraken

source=pnovikova@access-iris.uni.lu:/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/annotation/mg.KEGG.counts.tsv
target=/Users/polina.novikova/Documents/phd/mibipa/imp_output/$sample/
scp -P 8022 $source $target

source=pnovikova@access-iris.uni.lu:/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/taxonomy/kraken/mgmt.kraken.parsed.tsv
target=/Users/polina.novikova/Documents/phd/mibipa/imp_output/$sample/kraken
scp -P 8022 $source $target

source=pnovikova@access-iris.uni.lu:/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/taxonomy/kraken/mgmt.kraken.contig.output
target=/Users/polina.novikova/Documents/phd/mibipa/imp_output/$sample/kraken
scp -P 8022 $source $target


sample=Batch3_20

source=pnovikova@access-iris.uni.lu:/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/mg.assembly.contig_depth.txt
target=/Users/polina.novikova/Documents/phd/mibipa/imp_output/$sample/
scp -P 8022 $source $target


# ################
# October 13th
# ################

dir=/work/projects/coevolution/expobiome_map
sample=1237_18

mkdir -p $dir/$sample/
 
cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/taxonomy/kraken/mgmt.kraken.parsed.tsv | cut -f1,11-17 > $dir/$sample/contig_taxonomy.tsv
cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/annotation/mg.KEGG.counts.tsv | cut -f1,2,7 | sed -e '1,2d' > $dir/$sample/KO_contigs.tsv


# ################
# October 14th
# ################


# Expobiome MAP script

dir=/work/projects/coevolution/expobiome_map

conda activate jupyter

head -1 /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/all.ids | while read sample; do
  mkdir -p $dir/$sample/
  cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/taxonomy/kraken/mgmt.kraken.parsed.tsv | cut -f1,11-17 > $dir/$sample/contig_taxonomy.tsv
  cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/$sample/run1/Analysis/annotation/mg.KEGG.counts.tsv | cut -f1,2,7 | sed -e '1,2d' > $dir/$sample/KO_contigs.tsv

  export sample 

  python $dir/script_merging.py

  echo ("sample $sample done")
done

# ################
# November 2nd
# ################

# folders with protein clusters:
/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs

type=uniq
cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$type/fastas
for file in *; do
  echo $(basename "${file}" .fasta)
  mkdir -p /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$type
  mkdir -p /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$type/lists/
  grep ">" $file | sed 's/>//g' > /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$type/lists/$(basename "${file}" .fasta).tsv 
done

# mantis annotations:
/work/projects/archaeome/coevolution/annotation_archaea




/work/projects/archaeome/coevolution/annotation_archaea/all_consensus_annotation_archaea.tsv

type=uniq
cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$type/lists
for file in *; do
  echo "-------------"
  echo $file
  echo "-------------"
  cat $file | while read protein; do
    grep -w $protein /work/projects/archaeome/coevolution/annotation_archaea/all_consensus_annotation_archaea.tsv
  done
done > /work/projects/ecosystem_biology/archaea/coevolution/analysis/annotation_of_SCs/$type/consensus.tsv 

  
type=uniq
cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/$type/lists
for file in *; do
  echo "-------------"
  echo $file
  echo "-------------"
  cat $file | while read protein; do
    grep -w $protein /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_ec.tsv
  done
done


# gut specific archaeal proteins:
/work/projects/ecosystem_biology/archaea/coevolution/analysis/unique_gut_proteins.csv

# annotations of archaeal proteins: 
archaea_kegg_unique_gut_functions_annotation.csv


# ################
# November 4th
# ################

# data:
/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/
# analysis:
/work/projects/ecosystem_biology/archaea/coevolution/analysis/annotation_of_SCs/


/work/projects/ecosystem_biology/archaea/coevolution/annotation_of_SCs.py

# ################
# November 8th
# ################

/work/projects/archaea_neurodeg/coevolution/hgt_analysis/

# ################
# November 11th
# ################

# redo the contig abundance column in ExpoBiome map script

/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1237_18/run1 # here

# contig abundance file: 
/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1237_18/run1/Analysis/mg.assembly.contig_depth.txt


# ################
# November 14th
# ################

# metachip gene coordinates, HGT: 
/scratch/users/sbusi/pn_metachip/intonate/data/test_out/genomes_merged_contigs_coordinates.txt


/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/gut/GUT_GENOME109037


# ################
# November 15th
# ################

# here:
/work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/



/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/uniq/lists/unc_15.tsv
/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv

cat /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/uniq/lists/unc_1.tsv | while read protein; do
grep $protein /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv | cut -f2 | sed 's/.fasta//g' > tmp.tsv 
# grep $protein /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/proteins_in_genomes_archaea.tsv | cut -f2 > tmp.tsv 
cat tmp.tsv | while read genome; do
# echo $genome
# grep $genome /scratch/users/sbusi/pn_metachip/intonate/data/test_out/genomes_merged_contigs_coordinates.txt
grep $genome /work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/consistent_genomes_classification_coords.txt
done
done


prodigal -i  GUT_GENOME260847.fasta -f gff

GUT_GENOME260847.fasta_1709  
prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta -f gbk -o ${genome}.gbk

/scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME260847.fasta 


prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME260847.fasta -f gff -o test_v2.gff


# ################
# November 21st
# ################

# here:
/mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1237_18/run1/Analysis/taxonomy/kraken/mgmt.kraken.contig.output

# cut out contigs and their taxids:
cat /mnt/isilon/projects/ecosystem_biology/MiBiPa/IMP/1237_18/run1/Analysis/taxonomy/kraken/mgmt.kraken.contig.output | sed 's/ (/\t/g' | sed 's/)/\t/g' | cut -f2,4 | sed 's/taxid //g' > /work/projects/coevolution/expobiome_map/1237_18/contig_taxid.tsv




cat uniprot_blast_h9_spoVAE.txt | while read line; do 
  if [[ $line = \>* ]] ; then
    cut -d_ -f1-4 $line; 
  fi; 
done


# ################
# November 30th
# ################

change names of blasted hits: # | -> _

sed 's/|/_/g' h20_blast_result.txt | sed 's/ /_/g' | awk '/^>/ {print $1,$2,$3,$4; next}1' FS=_ OFS=_ | awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' > h20_blast_result_processed.fasta 



# ################
# December 2nd
# ################

cd /work/projects/archaea_neurodeg/coevolution/hgt_analysis/hgt_genes/homo/decoded_metachip
for file in *tsv; do tail -n +2 $file ; done |  sed '1 i\Gene_1\tGene_2\tIdentity\tend_match\tfull_length_match\tdirection\tGene1_contig\tGene2_contig\tGene1_coordinate\tGene2_coordinate\tprotein'  > ../decoded_metachip_all_clusters_homo.tsv

# ################
# December 5th
# ################

/work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv 

2. extract up- and downstream genes (predict with prodigal, .sco format and then select only +-5 genes) from the gene of interest with: 

# a bit different from archaeal:
dir=/work/projects/archaeome/coevolution/synteny/dig_sporulation/bacterial
conda activate prodigal
mkdir -p $dir/sco/
for protein_cluster in $dir/protein_clusters/h20.fasta; do
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
    mkdir -p $dir/sco/$(basename "${protein_cluster}" .fasta)
    if [[ $protein != GUT* ]]; 
    then
    echo $protein
    genome=$(echo $protein | cut -d '_' -f1,2)
    exact_protein=$(echo $protein | cut -d '_' -f3)
    prodigal -i /scratch/users/pnovikova/bacteria/data/gem/genomes_gut/${genome}.fasta -f sco -o $dir/sco/$(basename "${protein_cluster}" .fasta)/${genome}.sco -q
    sed -i '/^#/d' $dir/sco/$(basename "${protein_cluster}" .fasta)/${genome}.sco
    cd $dir/sco/$(basename "${protein_cluster}" .fasta)
    grep -A 5 -B 5 ">${exact_protein}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | sed 's/+/1/g' | sed 's/-/-1/g' > df_${genome}.tsv 

    else 
    echo $protein
    genome=$(echo $protein | cut -d '_' -f1-2)
    appendix1=$(echo $protein | cut -d '_' -f3)
    appendix2=$(echo $protein | cut -d '_' -f4)
    to_search="$appendix1|$appendix2"

    cd $dir/sco/$(basename "${protein_cluster}" .fasta)
    prodigal -i /scratch/users/pnovikova/bacteria/data/genomes_gut/${genome}.fasta -f sco -o ${genome}.sco -q

    sed 's/^# Model Data.*//g' ${genome}.sco | sed -r '/^\s*$/d' > tmp && mv tmp ${genome}.sco
    awk '
      /^# Sequence Data/ {n++} 
      /^>/ {sub(/>/, ">" n "|")}
      1
    ' ${genome}.sco > tmp && mv tmp ${genome}.sco
    sed 's/^# Sequence Data.*//g' ${genome}.sco | sed -r '/^\s*$/d' > tmp && mv tmp ${genome}.sco 

    grep -A 5 -B 5 ">${to_search}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | sed 's/+/1/g' | sed 's/-/-1/g' | sed 's/|/_/g' > df_${genome}.tsv 
    fi
  done
done


# # should be a separate for GUT:
# for protein_cluster in $dir/protein_clusters/h9.fasta; do
# cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
# mkdir -p $dir/sco/$(basename "${protein_cluster}" .fasta)
# if [[ $protein == GUT* ]]; 
# then
# echo $protein
# genome=$(echo $protein | cut -d '_' -f1-2)
# appendix1=$(echo $protein | cut -d '_' -f3)
# appendix2=$(echo $protein | cut -d '_' -f4)
# to_search="$appendix1|$appendix2"

# cd $dir/sco/$(basename "${protein_cluster}" .fasta)
# prodigal -i /scratch/users/pnovikova/bacteria/data/genomes_gut/${genome}.fasta -f sco -o ${genome}.sco -q

# sed 's/^# Model Data.*//g' ${genome}.sco | sed -r '/^\s*$/d' > tmp && mv tmp ${genome}.sco
# awk '
#   /^# Sequence Data/ {n++} 
#   /^>/ {sub(/>/, ">" n "|")}
#   1
# ' ${genome}.sco > tmp && mv tmp ${genome}.sco
# sed 's/^# Sequence Data.*//g' ${genome}.sco | sed -r '/^\s*$/d' > tmp && mv tmp ${genome}.sco 

# grep -A 5 -B 5 ">${to_search}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | sed 's/+/1/g' | sed 's/-/-1/g' | sed 's/|/_/g' > df_${genome}.tsv 
# fi 
# done
# break
# done

3. add KO information

dir=/work/projects/archaeome/coevolution/synteny/dig_sporulation/bacterial
cd $dir 

for protein_cluster in $dir/protein_clusters/*.fasta; do
echo $protein_cluster
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
    echo $protein
    if [[ $protein != GUT* ]];
      then
      genome=$(echo $protein | cut -d '_' -f1,2)
      exact_protein=$(echo $protein | cut -d '_' -f3)
      start=$(($exact_protein-5))
      end=$(($exact_protein+5))

      for i in $(seq $start $end); do
        if grep -w -q "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv
        then
          grep -w -m 1 "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv  | head -1
        else
          printf "${genome}_${i}\tNone\tnot found\n"
        fi
      done | cut -d_ -f3 > $dir/sco/$(basename "${protein_cluster}" .fasta)/KO_${genome}.tsv

      else
      genome=$(echo $protein | cut -d '_' -f1-2)
      appendix1=$(echo $protein | cut -d '_' -f3)
      appendix2=$(echo $protein | cut -d '_' -f4)

      start=$(($appendix2-5))
      end=$(($appendix2+5))

      for i in $(seq $start $end); do
        if grep -w -q "${genome}_${appendix1}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv
        then
          grep -w -m 1 "${genome}_${appendix1}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv  | head -1
        else
          printf "${genome}_${appendix1}_${i}\tNone\tnot found\n"
        fi
      done | cut -d_ -f4 > $dir/sco/$(basename "${protein_cluster}" .fasta)/KO_${genome}.tsv
    fi 
  done
done 


for file in KO*; do echo $file; cat $file; done | grep -v "tsv" | cut -f2,3 | sort | uniq -c | sort -nr



# ################
# December 6th
# ################

GUT_GENOME007829

GUT_GENOME007829_7_104 - K05967

grep "K05967" KO* | cut -f1 | sed 's/KO_//g' | sed 's/.tsv//g' | sed 's/:.*//' > /work/projects/archaeome/coevolution/synteny/dig_sporulation/bacterial/sco/genomes_with_K05967.tsv

cat genomes_with_K05967.tsv | while read genome; do
  grep "$genome" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv | grep "K05967"
done | uniq -c > proteins_with_K05967.tsv


awk 'BEGIN{RS=">";ORS=""} /GUT_GENOME015871_32_17/' GUT_GENOME015871*

>GUT_GENOME007829_7_104 
MNIGVDIDGVLTDLERMALDFGTKMCVEENIPIELNLSKYWEIEKYNWTKEQEELFWNKN
LVPYVVESNPRKFAPQILEKLQEEGNKIFIITARNESGMPPEYEGKMQELTKKWLLDNNI
KYKKLIFTDDTNKLKNCIENNIDVMVEDSPINIKNISQKIKVIKFDCQYNKDIDGKNILT
AYSWYHIYDIIRKLNTR*
>GUT_GENOME015871_32_17 
MNIGVDIDGVLTDLERMALDFGTKMCVEENIPIELNLSKYWEIEKYNWTKEQEELFWNKN
LVPYVVESNPRKFAPQILEKLQEEGNKIFIITARNESGMPPEYEGKMQELTKKWLLDNNI
KYKKLIFTDDTNKLKNCIENNIDVMVEDSPINIKNISQKIKVIKFDCQYNKDIDGKNILT
AYSWYHIYDIIRKLNTR*

hub=/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria
cat proteins_with_K05967.tsv | while read protein; do
  if [[ $protein != GUT* ]]; then
    genome=$(echo $protein | cut -d '_' -f1,2)
    awk -v var="$protein" 'BEGIN{RS=">";ORS=""} /var/' $hub/$genome*
  else
    genome=$(echo $protein | cut -d '_' -f1-2)
    awk -v var="$protein" 'BEGIN{RS=">";ORS=""} /var/' $hub/$genome*
  fi
done > K05967_cluster.fasta

awk "BEGIN{RS=">";ORS=""} /${protein}/" $hub/$genome*


# ################
# December 13th
# ################

cd /work/projects/archaeome/coevolution/synteny/dig_sporulation

conda activate diamond

for file in cultures/*protein.faa; do
  database=$(basename "$file" .protein.faa)
  echo $database
  echo $file
  diamond makedb --in $file -d $database
  diamond blastx -d $database -q h20.fna -o ${database}_h20_matches.tsv
  rm $database.dmnd
done


diamond makedb --in cultures/DSM2375.fna -d DSM2375 --ignore-warnings
diamond blastx -d DSM2375 -q h20.fna  -o DSM2375_h20_n_matches.tsv

# blastp  Align amino acid query sequences against a protein reference database
# blastx  Align DNA query sequences against a protein reference database

# h9 

GUT_GENOME193092

1347    1360902 1361159 1
1348    1361154 1361714 -1
1349    1361727 1362380 -1
1350    1362405 1363556 -1
1351    1363582 1363788 1
1352    1363866 1364216 -1
1353    1364216 1365211 -1
1354    1365213 1365941 -1
1355    1366405 1366692 1
1356    1366718 1367956 -1
1357    1368070 1368597 1


conda activate diamond
for file in cultures/*protein.faa; do
  database=$(basename "$file" .protein.faa)
  echo $database
  echo $file
  diamond makedb --in $file -d $database
  diamond blastp -d $database -q h9_flanking.fasta -o ${database}_h9_flanking_matches.tsv
  rm $database.dmnd
done



# ################
# December 12nd
# ################
cat DSM861_feature_table.txt | cut -f8-11,14 > DSM861_coordinates.tsv

cat DSM861_h9_flanking_matches.tsv | cut -f1,2,3 > DSM861_h9_flanking_matches_reduced.tsv


# h20

GUT_GENOME048397.fasta_1606 , h20.1 -> h20.3
GUT_GENOME117833.fasta_1758 , h20.2-> h20.1
GUT_GENOME048380.fasta_1664 , h20.3 -> h20.2

cd /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_archaea/gut
grep "_1664" -A 50 -B 50 GUT_GENOME048380.fasta.proteins

conda activate diamond

for file in cultures/*protein.faa; do
  database=$(basename "$file" .protein.faa)
  echo $database
  echo $file
  diamond makedb --in $file -d $database
  diamond blastp -d $database -q h20_flanking.fasta -o ${database}_h20_flanking_matches.tsv
  rm $database.dmnd
done

cat DSM861_h20_flanking_matches.tsv | cut -f1,2,3 > DSM861_h20_flanking_matches_reduced.tsv

# ################
# January 10th
# ################

h9.1 is >GUT_GENOME193092.fasta_1352
h9.2 is >GUT_GENOME282155.fasta_1166

file=/scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME193092.fasta
prodigal -i $file -d h9.1.fna 

cat /work/projects/archaeome/coevolution/synteny/dig_sporulation/h9_flanking.fasta | grep ">" > /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/get_nucleotide_sequences/proteins_h9_n.fna

grep -A 150 -B 150 "_1352" h9.1.fna

grep -A 80 -B 80 "_1166" h9.2.fna

# select manually 

cd /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
# save nucleotide sequcnes to
proteins_h9_n.fna


h20.1 is GUT_GENOME048397.fasta_1606  3
h20.2 is GUT_GENOME117833.fasta_1758  1
h20.3 is GUT_GENOME048380.fasta_1664  2

file=/scratch/users/pnovikova/archaea/data/gut/consistent_genomes/GUT_GENOME048380.fasta
prodigal -i $file -d h20.3.fna 

cat /work/projects/archaeome/coevolution/synteny/dig_sporulation/h20_flanking.fasta | grep ">" > /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/get_nucleotide_sequences/proteins_h20_n.fna

grep -A 150 -B 150 "_1664" h20.1.fna
grep -A 80 -B 80 "_1758" h20.2.fna
grep -A 80 -B 80 "_1606" h20.3.fna


# ################
# January 11th
# ################

cd /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/spoV_flankings_genes/h20

for file in *.mosdepth.summary.txt; do 
sample=$(basename "${file}" .mosdepth.summary.txt)
tail -n +2 $file | head -n -1 | awk -v var="${sample}" 'BEGIN{OFS="\t"}$1='var'"\t"$1'
done | sed '1 i\sample\tchrom\tlength\tbases\tmean\tmin\tmax' >  /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/gene_expression/gene_expression_spoV_flanking_h20.tsv


# ################
# January 24th
# ################

cd /work/projects/archaea_neurodeg/coevolution/spoV_trees

bmge \
-i h9+blast_msa.fasta \
-t AA \
-m BLOSUM95 \
-of trimming/h9+blast_msa_trimmed.fasta 

bmge \
-i h20+blast_msa.fasta \
-t AA \
-m BLOSUM95 \
-of trimming/h20+blast_msa_trimmed.fasta 

BLOSUM95 for closely related sequences

/Users/polina.novikova/Downloads/iqtree-1.6-2.12-MacOSX/bin/iqtree -s /Users/polina.novikova/Documents/phd/host_association/variability_within_clusters/sporulation_genes/trimmed_alignments/ -m TEST -bb 1000 -alrt 1000
































