proteins to be mapped: 
/work/projects/archaeome/coevolution/synteny/dig_sporulation/h9_flanking.fasta
/work/projects/archaeome/coevolution/synteny/dig_sporulation/h20_flanking.fasta







#!/bin/bash -l
#SBATCH -J mapping_fl_h20
#SBATCH -o mapping_fl_h20_%j.log
#SBATCH -N 1
#SBATCH -n 112
#SBATCH --time=14-00:00:00
#SBATCH --partition=batch
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

date 

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
cd $dir

ref=/work/projects/archaeome/coevolution/synteny/dig_sporulation/h20_flanking.fasta

# index the reference
bwa index $ref

cat $dir/must_family_ids.tsv | while read family; do
        echo $family
        cat $dir/${family}.tsv | while read sample; do
                echo $sample

                reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
                reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

                # paired-end mapping, general command structure, adjust to your case
                bwa mem $ref $reads1 $reads2 -t 112 > $dir/mapping/spoV_flankings_genes/h20/${sample}.aln_pe.sam

                # fix mates and compress
                samtools sort -n --threads 112 -O sam $dir/mapping/spoV_flankings_genes/h20/${sample}.aln_pe.sam | samtools fixmate -m --threads 112 -O bam - $dir/mapping/spoV_flankings_genes/h20/${sample}.fixmate.bam

                # convert to bam file and sort
                samtools sort --threads 112 -O bam -o $dir/mapping/spoV_flankings_genes/h20/${sample}.sorted.bam $dir/mapping/spoV_flankings_genes/h20/${sample}.fixmate.bam

                # Once it successfully finished, delete the fixmate file and the sam file to save space
                rm $dir/mapping/spoV_flankings_genes/h20/${sample}.fixmate.bam
                rm $dir/mapping/spoV_flankings_genes/h20/${sample}.aln_pe.sam

                # get median read coverage for each gene:
                samtools index $dir/mapping/spoV_flankings_genes/h20/${sample}.sorted.bam
                cd $dir/mapping/spoV_flankings_genes/h20/
                mosdepth ${sample} $dir/mapping/spoV_flankings_genes/h20/${sample}.sorted.bam --use-median -t 112
        done
done

date

# ------------------
# ------------------
# DON'T DO YET 
# ------------------
# ------------------

# Fetch expressed genes from MT reads

for file in *.mosdepth.summary.txt; do 
sample=$(basename "${file}" .mosdepth.summary.txt)
tail -n +2 $file | head -n -1 | awk -v var="${sample}" 'BEGIN{OFS="\t"}$1='var'"\t"$1'
done | sed '1 i\sample\tchrom\tlength\tbases\tmean\tmin\tmax' >  /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/gene_expression/gene_expression_homo_arch_filtered.tsv

cd  /work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis/mapping/gene_expression/

# now go to MT_gene_expression_BWAMEM.ipynb and copy the expression for selective genes to /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/2plot_expression_homo_arch_filtered.tsv
# make a plot in MG + MT abundance.ipynb