# --------------------
# --------------------
# for archaeal genomes
# --------------------
# --------------------

1. move and rename clusters in /work/projects/archaeome/coevolution/synteny/archaea/homo
    this will be the main dir 
    
2. extract up- and downstream genes (predict with prodigal, .sco format and then select only +-5 genes) from the gene of interest with: 
    dir=/work/projects/archaeome/coevolution/synteny/archaea/homo
    conda activate prodigal
    mkdir -p $dir/sco/
    for protein_cluster in $dir/protein_clusters/*.fasta; do
    cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
        if [[ $protein == GUT* ]];
        then
        genome=$(echo $protein | cut -d '.' -f1)
        echo $genome
        else
        genome=$(echo $protein | cut -d '_' -f1,2)
        echo $genome
        fi
        exact_protein=$(echo $protein | cut -d '_' -f3)
        echo $exact_protein
        mkdir -p $dir/sco/$(basename "${protein_cluster}" .fasta)
        prodigal -i /scratch/users/pnovikova/archaea/data/gut/consistent_genomes/${genome}.fasta -f sco -o $dir/sco/$(basename "${protein_cluster}" .fasta)/${genome}.sco
        sed -ie '1,2d' 
        cd $dir/sco/$(basename "${protein_cluster}" .fasta)
        grep -A 5 -B 5 ">${exact_protein}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | sed 's/+/1/g' | sed 's/-/-1/g' > df_${genome}.tsv 
        done
    done
3. add KO information
    cd /work/projects/archaeome/coevolution/synteny/archaea/homo

    dir=/work/projects/archaeome/coevolution/synteny/archaea/homo
    for protein_cluster in /work/projects/archaeome/coevolution/synteny/archaea/homoprotein_clusters/*.fasta; do
    cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
    if [[ $protein == GUT* ]];
        then
        genome=$(echo $protein | cut -d '.' -f1)
        echo $genome
        else
        genome=$(echo $protein | cut -d '_' -f1,2)
        echo $genome
    fi
    exact_protein=$(echo $protein | cut -d '_' -f3)
    echo $protein
    
    start=$(($exact_protein-5))
    end=$(($exact_protein+5))

    for i in $(seq $start $end); do
        if [[ $protein == GUT* ]];
        then
            if grep -w -q "${genome}.fasta_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv
            then 
                grep -w -m 1 "${genome}.fasta_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv | head -1
            else 
                printf "${genome}.fasta_${i}\tNone\tnot found\n"
            fi
        else
            if grep -w -q "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_archaea/archaea_annot_kegg.tsv
            then     printf "${genome}_${i}\tNone\tnot found\n"
            fi
        fi
        done | cut -d '_' -f3 > $dir/sco/$(basename "${protein_cluster}" .fasta)/KO_${genome}.tsv
    done
    done 
4. run the synteny python script 

# --------------------
# --------------------
# for bacterial genomes
# --------------------
# --------------------

1. move and rename clusters in /work/projects/archaeome/coevolution/synteny/homo
    this will be the main dir 
    
2. extract up- and downstream genes (predict with prodigal, .sco format and then select only +-5 genes) from the gene of interest with: 
# a bit different from archaeal:
dir=/work/projects/archaeome/coevolution/synteny/dig_sporulation/bacterial
conda activate prodigal
mkdir -p $dir/sco/
for protein_cluster in $dir/protein_clusters/*.fasta; do
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
    mkdir -p $dir/sco/$(basename "${protein_cluster}" .fasta)
    if [[ $protein != GUT* ]]; 
        then
        echo $protein
        genome=$(echo $protein | cut -d '_' -f1,2)
        exact_protein=$(echo $protein | cut -d '_' -f3)
        # prodigal -i /scratch/users/pnovikova/bacteria/data/gem/genomes_gut/${genome}.fasta -f sco -o $dir/sco/$(basename "${protein_cluster}" .fasta)/${genome}.sco -q
        # sed -i '/^#/d' $dir/sco/$(basename "${protein_cluster}" .fasta)/${genome}.sco
        cd $dir/sco/$(basename "${protein_cluster}" .fasta)
        grep -A 5 -B 5 ">${exact_protein}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | sed 's/+/1/g' | sed 's/-/-1/g' > df_${genome}.tsv 

        else 
        echo $protein
        genome=$(echo $protein | cut -d '_' -f1-2)
        appendix1=$(echo $protein | cut -d '_' -f3)
        appendix2=$(echo $protein | cut -d '_' -f4)
        to_search="$appendix1|$appendix2"

        cd $dir/sco/$(basename "${protein_cluster}" .fasta)
        # prodigal -i /scratch/users/pnovikova/bacteria/data/genomes_gut/${genome}.fasta -f sco -o ${genome}.sco -q

        # sed 's/^# Model Data.*//g' ${genome}.sco | sed -r '/^\s*$/d' > tmp && mv tmp ${genome}.sco
        # awk '
        # /^# Sequence Data/ {n++} 
        # /^>/ {sub(/>/, ">" n "|")}
        # 1
        # ' ${genome}.sco > tmp && mv tmp ${genome}.sco
        # sed 's/^# Sequence Data.*//g' ${genome}.sco | sed -r '/^\s*$/d' > tmp && mv tmp ${genome}.sco 

        grep -A 5 -B 5 ">${to_search}_" ${genome}.sco | sed 's/_/\t/g' | sed 's/>//g' | sed 's/+/1/g' | sed 's/-/-1/g' | sed 's/|/_/g' | cut -d_ -f2 > df_${genome}.tsv

    fi
  done
done

3. add KO information

dir=/work/projects/archaeome/coevolution/synteny/dig_sporulation/bacterial
cd $dir 

for protein_cluster in $dir/protein_clusters/*.fasta; do
echo $protein_cluster
  cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
    echo $protein
    if [[ $protein != GUT* ]];
      then
      genome=$(echo $protein | cut -d '_' -f1,2)
      exact_protein=$(echo $protein | cut -d '_' -f3)
      start=$(($exact_protein-5))
      end=$(($exact_protein+5))

      for i in $(seq $start $end); do
        if grep -w -q "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv
        then
          grep -w -m 1 "${genome}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv  | head -1
        else
          printf "${genome}_${i}\tNone\tnot found\n"
        fi
      done | cut -d_ -f3 > $dir/sco/$(basename "${protein_cluster}" .fasta)/KO_${genome}.tsv

      else
      genome=$(echo $protein | cut -d '_' -f1-2)
      appendix1=$(echo $protein | cut -d '_' -f3)
      appendix2=$(echo $protein | cut -d '_' -f4)

      start=$(($appendix2-5))
      end=$(($appendix2+5))

      for i in $(seq $start $end); do
        if grep -w -q "${genome}_${appendix1}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv
        then
          grep -w -m 1 "${genome}_${appendix1}_${i}" /work/projects/archaeome/coevolution/annotation_bacteria/kegg.tsv  | head -1
        else
          printf "${genome}_${appendix1}_${i}\tNone\tnot found\n"
        fi
      done | cut -d_ -f4 > $dir/sco/$(basename "${protein_cluster}" .fasta)/KO_${genome}.tsv
    fi 
  done
done 

4. check which genes occur the most:
for file in KO*; do echo $file; cat $file; done | grep -v "tsv" | cut -f2,3 | sort | uniq -c | sort -nr

5. check taxonomies of those bacteria: 

dir=/work/projects/archaeome/coevolution/synteny/dig_sporulation/bacterial
cd $dir 

for protein_cluster in $dir/protein_clusters/h20.fasta; do
    cat $protein_cluster | grep ">" | sed 's/>//g' | while read protein; do
    # echo $protein
        if [[ $protein != GUT* ]];
            then
            genome=$(echo $protein | cut -d '_' -f1,2)
            # echo $genome
            grep "$genome" /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tax_all_bacteria.tsv | cut -f2-8 | uniq -c
        else
        genome=$(echo $protein | cut -d '_' -f1-2)
        # echo $genome
        grep "$genome" /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tax_all_bacteria.tsv | cut -f2-8 | uniq -c
        fi
    done
done

4. go to syntheny.ipynb to create plots