
#!/bin/bash -l
#SBATCH -J workflow
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --time=1-00:00:00
#SBATCH --partition=bigmem
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova.001@student.uni.lu

conda activate smash

lao=~/reserach_project/lao
export lao

declare samples=("2011-07-08")
# declare binners=("concoct")
declare binners=("maxbin" "metabat" "concoct" "dastool")

for sample in ${samples[@]}; do
        mkdir -p $lao/analysis/smash/$sample/mgmt/sig/

        for binner in ${binners[@]}; do
                for i in `cat $lao/analysis/smash/$sample/mgmt/$binner.ids.filtered.txt`
                do
                        cd $lao/binning.new/$sample/mgmt/$binner/bins/
                        sourmash compute --scaled 1000 -k 31 $i -o $lao/analysis/smash/$sample/mgmt/sig/$i.sig
                        cd $lao/analysis/smash/$sample/mgmt/
                done

        done
        sourmash index -k 31 $lao/analysis/smash/$sample/mgmt/sbt_08 $lao/analysis/smash/$sample/mgmt/sig/*.sig

        mkdir -p  $lao/analysis/smash/$sample/mgmt/out

        sourmash compare -p 8 $lao/analysis/smash/$sample/mgmt/sig/*.sig -o $lao/analysis/smash/$sample/mgmt/out/08_cmp --csv $lao/analysis/smash/$sample/mgmt/out/out.csv
done








conda activate smash 

work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'
dataset=gem
genome_folder=$scratch/data/$dataset/genomes

mkdir -p $scratch/analysis/smash/$dataset
mkdir -p $work/analysis/smash/$dataset
mkdir -p $scratch/analysis/smash/$dataset/sig
mkdir -p $scratch/analysis/smash/$dataset/out

cd $scratch/data/$dataset/genomes/
for f in *.fasta; do echo $f ; sourmash compute --scaled 1000 -k 31 $f -o $scratch/analysis/smash/$dataset/sig/$f.sig ; done

sourmash index -k 31 $scratch/analysis/smash/$dataset/sbt_08 $scratch/analysis/smash/$dataset/sig/*.sig

# to analyze in R too
sourmash compare -p 16 $scratch/analysis/smash/$dataset/sig/*.sig -o $scratch/analysis/smash/$dataset/out/08_cmp \
--csv $scratch/analysis/smash/$dataset/out.csv

cd $scratch/analysis/smash/$dataset/out

sourmash plot --pdf --labels $scratch/analysis/smash/$dataset/out/08_cmp \
--indices \
--output-dir $scratch/analysis/smash/$dataset





# protein signatures


conda activate smash

dir='/work/projects/coevolution/phylogeny/smash/arch_uncommon_kegg_ids'

mkdir -p $dir/analysis/
mkdir -p $dir/analysis/sig
mkdir -p $dir/analysis/out


cd $dir/PFs

# compute sigs
for f in *.fasta; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $dir/analysis/sig/$f.sig ; done
cd $dir/analysis/sig

# index
sourmash index $dir/analysis/sbt_08 $dir/analysis/sig/*.sig


# to analyze in R too
sourmash compare -p 16 $dir/analysis/sig/*.sig -o $dir/analysis/out/08_cmp \
--csv $dir/analysis/out.csv

sourmash plot --pdf --labels $dir/analysis/out/08_cmp \
--indices \
--output-dir $dir/analysis




# for common KEGG ids

#!/bin/bash -l
#SBATCH -J smash
#SBATCH -N 2
#SBATCH -c 16
#SBATCH --time=2-00:00:00
#SBATCH --partition=bigmem
#SBACTH --qos=qos-bigmem
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH --mail-type=start,end,fail

conda activate smash

dir='/work/projects/coevolution/phylogeny/smash/bac_VS_arch_common_kegg_ids_ALL'

mkdir -p $dir/analysis/
mkdir -p $dir/analysis/sig
mkdir -p $dir/analysis/out


cd $dir/PFs

# compute sigs
for f in *.fasta; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $dir/analysis/sig/$f.sig ; done
cd $dir/analysis/sig

# index
sourmash index $dir/analysis/sbt_08 $dir/analysis/sig/*.sig


# to analyze in R too
sourmash compare -p 32 $dir/analysis/sig/*.sig -o $dir/analysis/out/08_cmp \
--csv $dir/analysis/out.csv

sourmash plot --pdf --labels $dir/analysis/out/08_cmp \
--indices \
--output-dir $dir/analysis





















































