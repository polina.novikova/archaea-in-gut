# ------------------------------------------
# DOWNLOAD DATABASES
# ------------------------------------------ 
# 1. gut archaea http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v1.0/
# links to archaeal genomes are indicated in genomes-all_metadata.tsv
# 2. 2nd portion from https://www.ncbi.nlm.nih.gov/assembly (from website to local -> to iris)
# 3. 3rd portion from https://portal.nersc.gov/GEM/genomes/ (extract by metadata)
grep "d__Archaea" genome_metadata.tsv | cut -f1 > archaeal_genomes.txt
cat archaeal_genomes.txt | while read f; do echo $f.fna.gz; cp fna/$f.fna.gz archaea/; done

# ------------------------------------------ 
# PREPROCESS GENOMES
# ------------------------------------------ 
# check if these are assemblies or complete solid genomes
for f in *.fna; do echo $f && grep -o '>' $f | wc -l; done 
# Preprocess GFFs of GUT genomes
	# 1. get the sequences
	ls ./db/ | cat | sed 's/....$//' | while read f  ; do grep "##" -v db/$f.gff | grep "ID=" -v > genomes/$f.fasta ;  done
	# 2. get GFFs 
	ls ./db | cat | while read f ; do grep "##" -v db/$f | grep "ID=" > gff/$f ; done
# delete unwanted characters from GUT samples
for f in *.fasta; do sed -i '/>/,$!d' $f| head; done 

# Change names of NCBI genomes
# Each file contains seqeuences for the complete genome, as well as for separate plasmids. So we just remove it.

cd /scratch/users/pnovikova/archaea/data/raw/ncbi/ncbi-genomes-2020-12-18
for f in *.fna; do 
	name="$(echo ${f} | cut -d '_' -f1,2)"; 
	awk -v n=2 '/>/{n--; if (!n) exit}; {print}' $f > /scratch/users/pnovikova/archaea/data/ncbi/genomes/$name.fna;	
	sed -i "s/>.*/>${name}/" /scratch/users/pnovikova/archaea/data/ncbi/genomes/$name.fna; 
done 
cd /scratch/users/pnovikova/archaea/data/ncbi/genomes/
# change .fna to .fasta
for f in *.fna; do mv -- "$f" "${f%.fna}.fasta" ; done


# change names of GEM genomes
# these are assemblies 



# TURN ASSEMBLIES (GUT AND GEM, NCBI is fine) WITH CONTIG NAMES INTO CONSISTENT ASSEMBLIES 
# because, e.g. for GC content calculation, it calculates GC% for EACH contig, and I want for entire genomes

dataset=gut
cd /scratch/users/pnovikova/archaea/data/$dataset/genomes/
for f in GUT_GENOME079980.fasta; do
sed '/^>/d' $f | sed "1s/^/>${f}\n/" > /scratch/users/pnovikova/archaea/data/$dataset/consistent_genomes/$f
done
ls /scratch/users/pnovikova/archaea/data/$dataset/genomes/ | wc -l
ls /scratch/users/pnovikova/archaea/data/$dataset/consistent_genomes/ | wc -l 

# Exctract taxids/ taxonomies of annotated genomes:

# NCBI: 

cd /scratch/users/pnovikova/archaea/data/ncbi/taxonomy/assembly_reports
for f in *.txt; do 
  name="$(echo ${f} | cut -d '_' -f1,2)";
  taxid="$(grep "Taxid" ${f} | cut -d ' ' -f12)";
  echo -e "$name\t$taxid" 
done >> /scratch/users/pnovikova/archaea/data/ncbi/taxonomy/NCBI_taxonomy.csv

# RESULTS ARE HERE -> /scratch/users/pnovikova/archaea/data/ncbi/taxonomy/NCBI_taxonomy.csv


# GUT

cd /scratch/users/pnovikova/archaea/data/raw/gut
grep "d__Archaea" genomes-all_metadata.tsv | cut -f1,19 | sed 's/;/\t/g' | sed 's/.__//g' | tail -n +2 > \
/scratch/users/pnovikova/archaea/data/gut/taxonomy/gut_taxonomy.tsv

# RESULTS ARE HERE -> /scratch/users/pnovikova/archaea/data/gut/taxonomy/gut_taxonomy.tsv

# GEM
cd /scratch/users/pnovikova/archaea/data/raw/gem
grep "d__Archaea" genome_metadata.tsv | cut -f1,15 | sed 's/;/\t/g' | sed 's/.__//g' | tail -n +2 > \
/scratch/users/pnovikova/archaea/data/gem/taxonomy/gem_taxonomy.tsv

# RESULTS ARE HERE -> /scratch/users/pnovikova/archaea/data/gem/taxonomy/gem_taxonomy.tsv


# Curate protein files: 

# NCBI 
cd /scratch/users/pnovikova/archaea/data/ncbi/faa/
for f in *.faa; do
	name="$(echo ${f} | cut -d '_' -f1,2)"; 
	mv "$f" "${name}.faa"; 
done

# it doesn't have consistent names (genome names and protein names), so we rename them
cd /scratch/users/pnovikova/archaea/data/ncbi/faa/
for f in *.faa; do 
echo $f
awk 'BEGINFILE {fn=FILENAME; sub(/\..*$/, "", fn); i=0} $1 ~ /^>/{$1 = ">" fn "_" ++i} 1' $f > tmp && mv tmp $f
done

### remove residual after the protein name
#### NCBI
sed -i 's/ .*//g' /scratch/users/pnovikova/archaea/data/ncbi/faa/*faa

#### GUT 
sed -i 's/ .*//g' /work/projects/ecosystem_biology/archaea/analysis/prodigal/GUT*proteins 

#### GEM
sed -i 's/ .*//g' /scratch/users/pnovikova/archaea/data/gem/faa/*faa 



###  make lists of protein in genomes 
NCBI - "/scratch/users/pnovikova/archaea/data/ncbi/proteins/*faa" 
GEM - "/scratch/users/pnovikova/archaea/data/gem/proteins/*faa"
GUT - "/scratch/users/pnovikova/archaea/data/gut/proteins/*proteins"

declare paths=("/scratch/users/pnovikova/archaea/data/gem/faa_archaeal/")

for path in ${paths[@]}; do
cd $path
#for file in GUT*proteins; do
for file in *faa; do
awk -F'>' '
FNR==1{
  match(FILENAME,/.*\./)
  file=substr(FILENAME,RSTART,RLENGTH-1)
}
/^>/{
  printf ("%s\t%s\n", $2, file)
}
' $file
done 
done >> /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/proteins_in_genomes.tsv

sed -i 's/.fasta//g' /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/proteins_in_genomes.tsv
sed -i '1 i\protein\tgenome' /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/proteins_in_genomes.tsv


# -----------------------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------------------

# ------------------------------------------
# TAXONOMY 
# ------------------------------------------

conda activate catbat_new

work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'

dataset=ncbi
genome_folder=$scratch/data/$dataset/genomes
database_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20201123/2020-11-23_CAT_database
taxonomy_folder=/scratch/users/pnovikova/tools/cat/CAT_prepare_20201123/2020-11-23_taxonomy

mkdir -p $scratch/analysis/taxonomy/catbat/$dataset
cd $scratch/analysis/taxonomy/catbat/$dataset

CAT bins -b $genome_folder -d $database_folder -t $taxonomy_folder -s fasta -f 0.5 --force \
-o $scratch/analysis/taxonomy/catbat/$dataset/$dataset
CAT add_names -i $scratch/analysis/taxonomy/catbat/$dataset/$dataset.bin2classification.txt \
-o $scratch/analysis/taxonomy/catbat/$dataset/$dataset.taxonomy.txt \
-t $taxonomy_folder --exclude_scores --only_official
CAT summarise -i $scratch/analysis/taxonomy/catbat/$dataset/$dataset.taxonomy.txt \
-o $scratch/analysis/taxonomy/catbat/$dataset/$dataset.taxonomy.summarised.txt

cp $scratch/analysis/taxonomy/catbat/$dataset/$dataset.taxonomy* $work/analysis/taxonomy/catbat/$dataset/

# ------------------------------------------
# RUN SMASH
# ------------------------------------------

conda activate smash 

work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'
dataset=gem
genome_folder=$scratch/data/$dataset/genomes

mkdir -p $scratch/analysis/smash/$dataset
mkdir -p $work/analysis/smash/$dataset
mkdir -p $scratch/analysis/smash/$dataset/sig
mkdir -p $scratch/analysis/smash/$dataset/out

# create signatires separately for all datasets separately
declare datasets=("ncbi" "gut" "gem")

for dataset in ${datasets[@]}; do
	cd $scratch/data/$dataset/genomes/
	for f in *.fasta; do echo $f ; sourmash compute --scaled 1000 -k 31 $f -o $scratch/analysis/smash/$dataset/sig/$f.sig ; done
done

cd /scratch/users/pnovikova/archaea/analysis/smash
for FILE in $(find . -maxdepth 3 -type f -name "*.sig"); do cp $FILE all/sig/; done

# index them all together
sourmash index -k 31 $scratch/analysis/smash/all/sbt_08 $scratch/analysis/smash/all/sig/*.sig

# compare all together
sourmash compare -p 16 $scratch/analysis/smash/all/sig/*.sig -o $scratch/analysis/smash/all/out/08_cmp \
--csv $scratch/analysis/smash/all/out.csv

# plot
sourmash plot --pdf --labels $scratch/analysis/smash/all/out/08_cmp \
--indices \
--output-dir $scratch/analysis/smash/all/

cp $scratch/analysis/smash/$dataset/*pdf $work/analysis/smash/$dataset/
cp $scratch/analysis/smash/$dataset/out.csv $work/analysis/smash/$dataset/

# conda activate smash 

# work='/work/projects/ecosystem_biology/archaea'
# scratch='/scratch/users/pnovikova/archaea'
# dataset=gut
# genome_folder=$scratch/data/$dataset/genomes

# mkdir -p $scratch/analysis/smash/$dataset
# mkdir -p $work/analysis/smash/$dataset
# mkdir -p $scratch/analysis/smash/$dataset/sig
# mkdir -p $scratch/analysis/smash/$dataset/out

# # cd $scratch/data/$dataset/genomes/
# # for f in *.fasta; do echo $f ; sourmash compute --scaled 1000 -k 31 $f -o $scratch/analysis/smash/$dataset/sig/$f.sig ; done

# sourmash index -k 31 $scratch/analysis/smash/$dataset/sbt_08 $scratch/analysis/smash/$dataset/sig/*.sig

# # to analyze in R too
# sourmash compare -p 16 $scratch/analysis/smash/$dataset/sig/*.sig -o $scratch/analysis/smash/$dataset/out/08_cmp \
# --csv $scratch/analysis/smash/$dataset/out.csv

# cd $scratch/analysis/smash/$dataset/out

# sourmash plot --pdf --labels $scratch/analysis/smash/$dataset/out/08_cmp \
# --indices \
# --output-dir $scratch/analysis/smash/$dataset


# ------------------------------------------
# RUN CHECKM SEPARATELY FOR ALL DATASETS
# ------------------------------------------

conda activate checkm
dataset=gem                                                                                               
                                                                                                          
checkm lineage_wf --tab_table -t 64 -r -x fasta \
/scratch/users/pnovikova/archaea/data/$dataset/genomes \
/scratch/users/pnovikova/archaea/analysis/checkm/$dataset \
&> /work/projects/ecosystem_biology/archaea/analysis/checkm/$dataset_genomes_quality.log 
# output is here : /work/projects/ecosystem_biology/archaea/analysis/checkm/.log


# filter genomes by completemess > 50%, contamination < 5%
cut -f1,12,13 ChM_$dataset__2191134.log | awk '$2>50' | awk '$3<5' > HQ_genomes_$dataset.tsv
# add fasta extension for easycompare with smash reuslts
dataset='gut'
awk -v OFS=$'\t' '{ $1=$1".fasta"; print}' HQ_genomes_$dataset.tsv  > tmp && mv tmp HQ_genomes_$dataset.tsv

# ------------------------------------------
# SELECT HIGH QUALITY NON REDUNDANT GENOMES
# ------------------------------------------
conda activate jupyter
jupyter notebook --ip $(ip addr show em1 | grep 'inet ' | awk '{print $2}' | cut -d/ -f1) --no-browser
# HQ genomes:
/work/projects/ecosystem_biology/archaea/analysis/checkm/HQ_genomes_$dataset.tsv
# Smash result: 
/work/projects/ecosystem_biology/archaea/analysis/smash/$dataset/out.csv


# make up a folder of non-redundant hq genomes for further analysis
dataset=gut
cut -f1 /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/nr-hq-all-genomes.csv | tail -n +2 | cat | \
while read f; do echo $f; cp /scratch/users/pnovikova/archaea/data/$dataset/genomes/$f /scratch/users/pnovikova/archaea/data/nr_hq_genomes/genomes/; done

# ------------------------------------------
# VALIDATE NR GENOMES WITH SEQUENCE COMPOSITION
# ------------------------------------------
# calculate GC contents


dataset=ncbi

mkdir /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset

for f in /scratch/users/pnovikova/archaea/data/$dataset/genomes/*.fasta; do 
name="$(basename "$f")";
name="${name::(-6)}"; 
echo $name; 
perl ~/scripts/get_gc_content.pl $f /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp;
sed -i 's/>//g' /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp; 
tail -n +2 /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp > \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset/$name.txt;
done 

rm /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp

cat /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset/*.txt >> /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_gc_content.tsv

sed -i '1s/^/ids\tGCContent\ttotal_count\tG_count\tC_count\tA_count\tT_count\n/' \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_gc_content.tsv


# GEM and GUT 

dataset=gut

mkdir -p /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset

for f in /scratch/users/pnovikova/archaea/data/$dataset/consistent_genomes/*.fasta; do 
name="$(basename "$f")";
name="${name::(-6)}"; 
echo $name; 
perl ~/scripts/get_gc_content.pl $f /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp;
sed -i 's/>//g' /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp; 
tail -n +2 /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp > \
/work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset/$name.txt;
done 

rm /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/tmp

cat /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/$dataset/*.txt >> /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_gc_content.tsv

sed -i '1s/^/ids\tGCContent\ttotal_count\tG_count\tC_count\tA_count\tT_count\n/' /work/projects/ecosystem_biology/archaea/analysis/sequence_composition/${dataset}_gc_content.tsv





# -----------------------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------------------

# ------------------------------------------
# RUN PRODIGAL
# ------------------------------------------
conda activate prodigal

for f in /scratch/users/pnovikova/archaea/data/nr_hq_genomes/genomes/*fasta; do
echo $f
prodigal -i $f -o /work/projects/ecosystem_biology/archaea/analysis/prodigal/$(basename "$f").genes \
-a /work/projects/ecosystem_biology/archaea/analysis/prodigal/$(basename "$f").proteins -f gff; done


# a little bit kostil' create nr-hq dir
cat /work/projects/ecosystem_biology/archaea/analysis/checkm/nr_genomes_quality_selected.tsv | cut -f1 | while read name; do
for file in *; do
if [[ $file == *$name* ]]; then 
	cp $file quality/;
fi
done
done


# ------------------------------------------
## RUN MANTIS
# ------------------------------------------

### Compile input files for MANTIS: 
#### 1. protein paths:
	##### NCBI and GEM: 

work='/work/projects/ecosystem_biology/archaea'
scratch='/scratch/users/pnovikova/archaea'

# get genome id and path to faa (based on nr-hq-genomes)
dataset='ncbi' # or 'gem'
cut -f1 $work/analysis/intermediate_results/nr-hq-all-genomes.csv | tail -n +2 | cat | sed 's/......$//' |\
while read f; do \
echo -e "$f\t"$(ls $scratch/data/${dataset}/faa/${f}.faa)""
done > $work/analysis/intermediate_results/tmp

grep "/scratch" $work/analysis/intermediate_results/tmp > \
$work/analysis/mantis/input_files/${dataset}.nr-hq.proteins.csv


	##### GUT

get genome id and path to faa (based on nr-hq-genomes)
dataset='gut'
for f in $work/analysis/prodigal/GUT*proteins; do
	name="$(basename "$f" | cut -d. -f1)"
	echo -e "$name\t$f"
done > $work/analysis/mantis/input_files/${dataset}.nr-hq.proteins.csv

#### 2. genome to taxonomy:

python /work/projects/ecosystem_biology/archaea/analysis/convert_gtdb_2_ncbi.py - eats full taxonomy and converts it into NCBI names/taxids



#### 3. combine genome id, path to faa, taxid in jup notebook
/work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/merge_genome_paths_tax.ipynb

#### Manual curation (some taxa were not converted, so do it manually and then replace wrong with right names in the input_file.tsv)
##### unrecognized2ncbi.tsv is a table of how actual names must be translated to ncbi

cd /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files
sed '1i\\' unrecognized2ncbi.tsv | sed -e '$a\' | cat | while read f1 f2 ; do
	wrong=${f1}
	right=${f2}
	echo -e "$wrong\t$right"
	grep "$wrong" input_file.tshead v | sed "s/$wrong/$right/g"
	sed -i "s/$wrong/$right/g" input_file.tsv
done 


add smth directly in bash:
sed -i -e '$aThermoplasmatota\t28890' unrecognized2ncbi.tsv



#### 4. split the input file into 10 part to get results quicker
split -n l/10 /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/input_file.tsv
for f in xa*; do mv "$f" "$f".tsv; done
mv xa*.tsv /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/split_input


### Run MANTIS

#!/bin/bash -l
#SBATCH -J mantis1
#SBATCH --time=2-00:00:00
#SBATCH --mem=100GB
#SBATCH -p batch

conda activate mantis_env

work='/work/projects/ecosystem_biology/archaea'

python /work/projects/ecosystem_biology/local_tools/mantis run_mantis \
-t $work/analysis/mantis/input_files/split_input/xaj.tsv \
-o work/projects/archaea/output_tsv

date



# ------------------------------------------
## SEQUENCE COMPOSITION CLUSTERING - MMSEQS
# ------------------------------------------

#### first copy all nr-hq proteins to one dir
cat /work/projects/ecosystem_biology/archaea/analysis/mantis/input_files/*nr-hq.proteins.csv | cut -f2 | while read f; do cp $f /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/$(basename "$f");  done 

cd /work/projects/ecosystem_biology/archaea/analysis/mmseqs

conda activate mmseqs2
mmseqs createdb /scratch/users/pnovikova/archaea/analysis/all.nr-hq.proteins/* proteinsDB
mmseqs linclust proteinsDB clust_0.9seqid_0.9c tmp --cov-mode 0 --min-seq-id 0.9 -c 0.9 --sort-results 1 --remove-tmp-files 1
mmseqs createtsv proteinsDB proteinsDB clust_0.9seqid_0.9c clustering_0.9seqid_0.9c.tsv
mmseqs createseqfiledb proteinsDB clust_0.9seqid_0.9c clust_0.9seqid_0.9c_seq
mmseqs result2flat proteinsDB proteinsDB clust_0.9seqid_0.9c_seq clustering_0.9seqid_0.9c.fasta

# ------------------------------------------
## FUNCTIONAL CLUSTERING - Mantis
# ------------------------------------------

#### combine functional annotations for each genome after mantis
for f in /work/projects/archaeome/outputs/*; do
cd $f;
cat $f/integrated_annotation.tsv >> /work/projects/archaeome/all_integrated_annotation.tsv;
done
cd ../../

#### extract pfam and ec values
grep "enzyme_ec:" all_integrated_annotation.tsv | cut -f1,14 | sed "s/enzyme_ec://" > tmp_ec
grep "enzyme_ec:" all_integrated_annotation.tsv | awk -F 'description:' '{print $2}' > tmp_description
paste tmp_ec tmp_description | sed '1 i\protein\tec\tdescription' > functional_annotation_ec.csv   

grep "pfam:" all_integrated_annotation.tsv | grep "description:"| grep -v "pfam:DUF" | grep -v "pfam:UPF" | grep -v "pfam:du" | cut -f1,12-20 | awk -F 'pfam:' '{print $2}' | cut -f1 > tmp_pfam
grep "pfam:" all_integrated_annotation.tsv | grep "description:"| grep -v "pfam:DUF" | grep -v "pfam:UPF" | grep -v "pfam:du" | cut -f1 > tmp_proteins
grep "pfam:" all_integrated_annotation.tsv | grep "description:"| grep -v "pfam:DUF" | grep -v "pfam:UPF" | grep -v "pfam:du" | awk -F 'description:' '{print $2}' > tmp_description
paste tmp_proteins tmp_pfam tmp_description | sed '1 i\protein\tpfam\tdescription' > functional_annotation_pfam.csv


###### not all the values are fetched with this script, use Pedro's annotation
grep "kegg_ko:" all_integrated_annotation.tsv | grep "description" | awk -F 'kegg_ko:' '{print $2}' | cut -f1 > tmp_ko
grep "kegg_ko:" all_integrated_annotation.tsv | grep "description" | cut -f1 > tmp_proteins
grep "kegg_ko:" all_integrated_annotation.tsv | grep "description" | awk -F 'description:' '{print $2}' | cut -f1 > tmp_description
paste tmp_proteins tmp_ko tmp_description | sed '1 i\protein\tko\tdescription' > functional_annotation_ko.csv
rm tmp_*

# ------------------------------------------
## PHYLOGENY
# ------------------------------------------

## Make phylogeny for each cluster separately

### process clustering for running MUSCLE

##### select clusters above the threshold (10+ proteins) in extract_selective_clusters.ipynb, instruction.csv is the output.
##### mmseqs produces a fasta file with clustered protein sequences. clean it up (awk removes duplicated lines):
sed 's/\s.*$//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/clustering_0.9seqid_0.9c.fasta | awk '$0==b{next}{b=$0;print}' | sed 's/*//g' > /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/all_proteins.fasta
##### clusters_above_threshold.csv - which cluster each protein belongs to; all_proteins.fasta - fasta file formatted to run the perl script
cd /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas
perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/instruction.csv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/analysis/intermediate_results/all_proteins.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins

##### give extension to all files
for f in *; do mv "$f" "$f".fasta; done

mkdir unclustered
mv unclustered_proteins.fasta unclustered



#### exclude short sequences (<30) from clusters (fasta files)
##### cut out short sequences
for f in /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*fasta; do
	awk '/^>/ {printf("\n%s\n",$0);next; } { printf("%s",$0);}  END {printf("\n");}' $f | awk '/^>/ { getline seq } length(seq) >= 30 { print $0 "\n" seq }' > tmp && mv tmp $f
done
##### delete empty clusters
cd /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/
find -type f -empty -delete

##### in total 4491 clusters above threshold 10+ proteins

### run MUSCLE

##### remove asterisks in protein sequences
sed -i 's/*//' /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*.fasta 
#### run muscle
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/afa
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln

conda activate muscle
for file in /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*.fasta; do 
muscle -in $file -fastaout /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/afa/$(basename "${file}" .fasta).afa -clwout /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/$(basename "${file}" .fasta).aln -clwstrict
done

### run CLUSTALW

conda activate clustalw
for file in /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*.aln; do 
clustalw2 -infile=$file -tree -outputtree=dist -clustering=NJ
done
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format/
mkdir -p /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format/dst
mv /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*ph /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format
mv /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/*dst /work/projects/ecosystem_biology/archaea/analysis/phylogeny/trees/dist_format/dst


## Calculate pairwise distance within clusters 

##### calculate distance matrices for proteins in each cluster

conda activate clustalw
for file in /work/projects/ecosystem_biology/archaea/analysis/mmseqs/protein_fastas/*.fasta; do 
	clustalo -i $file --distmat-out /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat --full
	awk -v OFS="\t" '$1=$1' /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat | tail -n +2  > tmp && mv tmp /work/projects/ecosystem_biology/archaea/analysis/phylogeny/distances/$(basename "${file}" .fasta).mat
done


##### generate a table of average pairwise distance for each cluster in average_pairwise_distance.ipynb, discard the ones with high values (for now I keep only those <0.2)


## run UniFunc to calculate annotation score within clusters
##### run Pedro's script cluster_quality.py to estimate annotation quality, merge it with average-pairwise distance, threshold >=0.9 both score and similarity.  
##### Ideally run this step before building HMMS , but in this script it was run after and above-threshold HMMs were put to separate folder (see below)


# ------------------------------------------
## BUILDING HMMS
# ------------------------------------------

### run HMMER

conda activate hmmer
cat 	`` | cut -f1 | tail -n +2 | while read f; do 
	hmmbuild /work/projects/ecosystem_biology/archaea/analysis/hmms/$f.hmm /work/projects/ecosystem_biology/archaea/analysis/phylogeny/alignment/aln/$f.aln 
done


##### Since UniFunc is done after building HMMS - which are build only based on average pairwise distance - (but should be before), copy HMMs (from `hmm/` folder) passing the annotation thresold from `clusters_filtered_annot_and_score.tsv` to the separate folder `hmm_filtered/`:

cat /work/projects/ecosystem_biology/archaea/analysis/intermediate_results/clusters_filtered_annot_and_score.tsv | while read sample; do
	cp /work/projects/ecosystem_biology/archaea/analysis/hmms/${sample}.hmm /work/projects/ecosystem_biology/archaea/analysis/hmm_filtered/${sample}.hmm
done




















































































































