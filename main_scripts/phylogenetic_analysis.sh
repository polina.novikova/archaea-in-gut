1. first get sequences 
2. alignment with MAFFT (all default, web server is ok)
3. trim sequences with BMGE 

bmge \
-i h20+blast_msa.fasta \
-t AA \
-m BLOSUM95 \
-of trimming/h20+blast_msa_trimmed.fasta 

# BLOSUM95 for closely related sequences

4. build trees with IQ-TREE (local)

/Users/polina.novikova/Downloads/iqtree-1.6-2.12-MacOSX/bin/iqtree -s /Users/polina.novikova/Documents/phd/host_association/variability_within_clusters/sporulation_genes/trimmed_alignments/msa.fasta -m TEST -bb 1000 -alrt 1000

5. visualize with ggtree (/Users/polina.novikova/Documents/phd/scripts/building_tree_w_MSA.R) 