#!/bin/bash -l                                                                                                                       
#SBATCH -J sort_bac_arch                                                                                
#SBATCH -o sort_bac_arch.log                                                                                                    
#SBATCH -N 8                                                                                                       
#SBATCH -c 16                                                                                                      
#SBATCH --time=10:00:00                                                                                                              
#SBATCH --begin=now                                                                       
#SBATCH --mail-type=begin,end,fail                                             
#SBATCH --mail-user=polina.novikova@uni.lu                                   
#SBATCH -p batch


cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs

perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/tmp_instructions_bac_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins

rm unclustered_proteins


perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_arch_GU_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_unique_gut_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
rm unclustered_proteins


for f in *; do mv "$f" "$f".fasta; done
