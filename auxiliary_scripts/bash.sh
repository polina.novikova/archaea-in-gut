#!/bin/bash -l
#SBATCH -J gunzip
#SBATCH -o gunzip.log
#SBATCH -N 4
#SBATCH -c 16
#SBATCH --time=48:00:00
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -p batch

cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs
tar -xvf /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PF_rprs_1st_1063079.tar
