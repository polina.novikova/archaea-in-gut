#!/bin/bash -l
#SBATCH -J mmseqs_arch_all_k
#SBATCH -o mmseqs_arch_all_k.log
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --time=48:00:00
#SBATCH -p bigmem

conda activate mmseqs2

date


conda activate mmseqs2

cd /work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/bacteria_gut/

mmseqs createdb /work/projects/ecosystem_biology/archaea/coevolution/data/proteins_bacteria/* proteinsDB
mmseqs linclust proteinsDB clust_0.9seqid_0.9c tmp --cov-mode 0 --min-seq-id 0.9 -c 0.9 --sort-results 1 --remove-tmp-files 1
mmseqs createtsv proteinsDB proteinsDB clust_0.9seqid_0.9c clustering_0.9seqid_0.9c.tsv
mmseqs createseqfiledb proteinsDB clust_0.9seqid_0.9c clust_0.9seqid_0.9c_seq
mmseqs result2flat proteinsDB proteinsDB clust_0.9seqid_0.9c_seq clustering_0.9seqid_0.9c.fasta


date 
