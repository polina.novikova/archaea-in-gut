import pandas as pd
import numpy as np
import scipy 
import seaborn as sns
from matplotlib import pyplot as plt
import re


analysis = '/work/projects/ecosystem_biology/archaea/coevolution/analysis'
pics = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/pics'

unique_gut = pd.read_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/unique_gut_proteins.csv')

ec = pd.read_csv("/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/archaea/archaea_annot_ec.tsv", 
            delimiter="\t", header=None, names=['protein', 'id', 'description'])
eggnog = pd.read_csv("/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/archaea/archaea_annot_eggnog.tsv", 
            delimiter="\t", header=None, names=['protein', 'id', 'description'])
pfam = pd.read_csv("/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/archaea/archaea_annot_pfam.tsv", 
            delimiter="\t", header=None, names=['protein', 'id', 'description'])
kegg = pd.read_csv("/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/archaea/archaea_annot_kegg.tsv", 
            delimiter="\t", header=None, names=['protein', 'id', 'description'])


# # uniq
 
# a = pd.DataFrame(columns=['SC', 'db', 'id', 'description'])

# for i in range(1, 29):
#     name = 'unc_'+str(i)

#     SC = pd.read_csv('/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/uniq/lists/'+name+'.tsv',
#            header=None, names=['protein'])

#     proteins = SC
    
#     name = name.replace('unc_', 'u')
#     print (name)

#     rslt_kegg = proteins.merge(kegg)
#     rslt_kegg['db'] = 'kegg'
#     rslt_kegg = rslt_kegg[['protein', 'db', 'id', 'description']]
    
#     rslt_ec = proteins.merge(ec)
#     rslt_ec['db'] = 'ec'
#     rslt_ec = rslt_ec[['protein', 'db', 'id', 'description']]
    
#     rslt_pfam = proteins.merge(pfam)
#     rslt_pfam['db'] = 'pfam'
#     rslt_pfam = rslt_pfam[['protein', 'db', 'id', 'description']]
    
#     rslt_eggnog = proteins.merge(eggnog)
#     rslt_eggnog['db'] = 'eggnog'
#     rslt_eggnog = rslt_eggnog[['protein', 'db', 'id', 'description']]
    
    
#     output = pd.concat([rslt_kegg, rslt_ec, rslt_pfam, rslt_eggnog])
    
#     output['SC'] = name
    
#     output = output[['SC', 'protein', 'db', 'id', 'description']]
    
#     groupped_output = output.groupby(['SC', 'db', 'description', 'id'], as_index=False)\
#     .agg({'protein':'count'})
    
#     groupped_output.rename(columns={'protein':'protein_count'}, inplace=True)

#     a = a.append(groupped_output)

# table_2save = a
# table_2save.to_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/annotation_of_SCs/kegg_pfam_ec_eggnog_uniq_SCs_v2.tsv',
#                  sep='\t', index=None)


    

#     clusters = unique_gut.merge(SC)[['cluster']].drop_duplicates()
#     proteins = unique_gut.merge(clusters)[['protein']]
    
#     rslt_kegg = proteins.merge(kegg).drop_duplicates(['id', 'description'])
#     rslt_kegg['db'] = 'kegg'
#     rslt_kegg = rslt_kegg[['protein', 'db', 'id', 'description']]
#     rslt_ec = proteins.merge(ec).drop_duplicates(['id', 'description'])
#     rslt_ec['db'] = 'ec'
#     rslt_ec = rslt_ec[['protein', 'db', 'id', 'description']]
#     rslt_pfam = proteins.merge(pfam).drop_duplicates(['id', 'description'])
#     rslt_pfam['db'] = 'pfam'
#     rslt_pfam = rslt_pfam[['protein', 'db', 'id', 'description']]
#     rslt_eggnog = proteins.merge(eggnog).drop_duplicates(['id', 'description'])
#     rslt_eggnog['db'] = 'eggnog'
#     rslt_eggnog = rslt_eggnog[['protein', 'db', 'id', 'description']]
    
#     output = pd.concat([rslt_kegg, rslt_ec, rslt_pfam, rslt_eggnog])
    
#     output['SC'] = name
    
#     output = output[['SC', 'db', 'id', 'description']]
       
#     a = a.append(output)    
    
# table_2save = a.drop_duplicates(['SC', 'db', 'id', 'description'])

# table_2save.to_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/annotation_of_SCs/kegg_pfam_ec_eggnog_uniq_SCs.tsv',
#                  sep='\t', index=None)


# homo

a = pd.DataFrame(columns=['SC', 'db', 'id', 'description'])

for i in range(1, 46):
    name = 'h'+str(i)
    SC = pd.read_csv('/work/projects/ecosystem_biology/archaea/coevolution/data/proteins_in_SCs/homo/lists/'+name+'.tsv',
           header=None, names=['protein'])
    
    proteins = SC

    print (name)
    
    rslt_kegg = proteins.merge(kegg)
    rslt_kegg['db'] = 'kegg'
    rslt_kegg = rslt_kegg[['protein', 'db', 'id', 'description']]
    
    rslt_ec = proteins.merge(ec)
    rslt_ec['db'] = 'ec'
    rslt_ec = rslt_ec[['protein', 'db', 'id', 'description']]
    
    rslt_pfam = proteins.merge(pfam)
    rslt_pfam['db'] = 'pfam'
    rslt_pfam = rslt_pfam[['protein', 'db', 'id', 'description']]
    
    rslt_eggnog = proteins.merge(eggnog)
    rslt_eggnog['db'] = 'eggnog'
    rslt_eggnog = rslt_eggnog[['protein', 'db', 'id', 'description']]
    
    
    output = pd.concat([rslt_kegg, rslt_ec, rslt_pfam, rslt_eggnog])
    
    output['SC'] = name
    
    output = output[['SC', 'protein', 'db', 'id', 'description']]
    
    groupped_output = output.groupby(['SC', 'db', 'description', 'id'], as_index=False)\
    .agg({'protein':'count'})
    
    groupped_output.rename(columns={'protein':'protein_count'}, inplace=True)
    
    a = a.append(groupped_output)  
    
print (a)

table_2save = a
table_2save.to_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/annotation_of_SCs/kegg_pfam_ec_eggnog_homo_SCs_v2.tsv',
                 sep='\t', index=None)