import urllib.request as request
import shutil
from contextlib import closing
import gzip
import re
import os
import subprocess
from pathlib import Path
import pandas as pd

def mantis_parser(mantis_integrated_annotation, outfile, db = {"kegg", "pfam", "ec"}):
    res = []
    if db == 'kegg': db_name = "kegg_ko:"
    if db == 'pfam': db_name = "pfam:"
    if db == 'ec': db_name = "enzyme_ec:"
    if db == 'eggnog': db_name = "eggnog:"
    with open(mantis_integrated_annotation, 'r') as file:
        with open(outfile, 'w') as f:
            strings = (db_name, "description:")
            for line in file:
                if all(s in line for s in strings):
                    sequence = line.split('\t')[0]
                    id = line.split(db_name)[1]
                    id = id.split('\t')[0]
                    description = line.split('description:')[1]
                    description = description.split('\t')[0]

                    outline = f'{sequence}\t{id}\t{description}\n'

                    f.write(outline)



# if __name__ == '__main__':
#     db = 'kegg'
#     integrated_annotation = '/Users/polina.novikova/Documents/phd/intermediate_results/all_integrated_annotation_archaea_eggnog.tsv'
#     outfile = '/Users/polina.novikova/Documents/phd/intermediate_results/archaea_annot_'+db+'.tsv'
#     mantis_parser(integrated_annotation, outfile, db = db)
# 
# if __name__ == '__main__':
#         db = 'pfam'
#         integrated_annotation = '/Users/polina.novikova/Documents/phd/intermediate_results/all_integrated_annotation_archaea_eggnog.tsv'
#         outfile = '/Users/polina.novikova/Documents/phd/intermediate_results/archaea_annot_' + db + '.tsv'
#         mantis_parser(integrated_annotation, outfile, db=db)
# 
# if __name__ == '__main__':
#     db = 'ec'
#     integrated_annotation = '/Users/polina.novikova/Documents/phd/intermediate_results/all_integrated_annotation_archaea_eggnog.tsv'
#     outfile = '/Users/polina.novikova/Documents/phd/intermediate_results/archaea_annot_'+db+'.tsv'
#     mantis_parser(integrated_annotation, outfile, db = db)

if __name__ == '__main__':
    db = 'eggnog'
    integrated_annotation = '/work/projects/archaeome/coevolution/all_integrated_annotation_bacteria_same_kegg_proteins.tsv'
    outfile = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/bacteria_annot_'+db+'same_kegg_proteins.tsv'
    mantis_parser(integrated_annotation, outfile, db=db)
