#!/bin/bash -l
#SBATCH -J mapping_h
#SBATCH -o mapping_h_%j.log
#SBATCH -N 1
#SBATCH -c 128
#SBATCH --time=14-00:00:00
#SBATCH --qos=long
#SBATCH --begin=now
#SBATCH --mail-type=start,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu

conda activate mapping

dir=/work/projects/ecosystem_biology/archaea/coevolution/analysis/mt_analysis
temp=/work/projects/archaeome/coevolution/mapping_h

cd $temp

# index the reference
bwa index $dir/proteins_homo_n.fa

date 

cat $dir/must_family_ids.tsv | while read family; do
echo $family
cat $dir/${family}.tsv | while read sample; do
echo $sample
date

reads1=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R1.fq
reads2=/mnt/isilon/projects/ecosystem_biology/MUST/IMP/stool/${family}/${sample}/Reads/MT.R2.fq

mkdir -p $dir/

# paired-end mapping, general command structure, adjust to your case
bwa mem $dir/proteins_homo_n.fa $reads1 $reads2 -t 128 > $temp/${sample}.aln_pe.sam

# fix mates and compress
samtools sort -n -O sam --threads 128 $temp/${sample}.aln_pe.sam | samtools fixmate -m --threads 128 --output-fmt bam - $temp/${sample}.fixmate.bam

# convert to bam file and sort
samtools sort --threads 128 -O bam -o $temp/${sample}.sorted.bam $temp/${sample}.fixmate.bam

# Once it successfully finished, delete the fixmate file and the sam file to save space
rm $temp/${sample}.fixmate.bam
rm $temp/${sample}.aln_pe.sam

# get median read coverage for each gene:
samtools index $temp/${sample}.sorted.bam
cd $dir/mapping/homo
mosdepth ${sample} $temp/${sample}.sorted.bam --use-median -t 128

date 

done
done
