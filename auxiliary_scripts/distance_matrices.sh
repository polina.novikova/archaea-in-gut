#!/bin/bash -l
#SBATCH -J distance_matrices
#SBATCH -o distance_matrices.log
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --time=48:00:00
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -p bigmem

pfs=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs
main_dir=/work/projects/coevolution/phylogeny/distance_matrices/all_bac_VS_GU_arch

conda activate clustalw

for file in $pfs/*.fasta; do 
clustalo -i $file --distmat-out $main_dir/$(basename "${file}" .fasta).mat --full
awk -v OFS="\t" '$1=$1' $main_dir/$(basename "${file}" .fasta).mat | tail -n +2  > tmp && mv tmp $main_dir/$(basename "${file}" .fasta).mat
done
