#!/bin/bash -l
#SBATCH -J smash
#SBATCH --time=7-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=long
#SBATCH -o smash_common_kegg_ids_only_sigs_%j.log
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -N 1
#SBATCH -n 88
#SBATCH --mem=1TB


conda activate smash

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch

# create signatures
cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs

cat /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/whats.left.txt | while read f; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $main_dir/sig/$f.sig; done
