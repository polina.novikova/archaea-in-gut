import pandas as pd
import numpy as np
import scipy 
import seaborn as sns
from matplotlib import pyplot as plt
import re
from matplotlib.ticker import MaxNLocator

print ("ARCHAEA")

db = 'kegg'
### Loop the data lines
with open("/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/archaea/archaea_annot_"+db+".tsv", 'r') as temp_f:
    # get No of columns in each line
    col_count = [ len(l.split("\t")) for l in temp_f.readlines() ]

### Generate column names  (names will be 0, 1, 2, ..., maximum columns - 1)
column_names = [i for i in range(0, max(col_count))]

### Read csv
df = pd.read_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/archaea/archaea_annot_'+db+'.tsv', delimiter="\t", names=column_names)
df.dropna(axis=1, how='all', inplace=True)
df.columns=['protein', 'id', 'description']
annot_kegg = df

df = pd.read_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/unique_gut_proteins.csv', index_col=False)[['protein', 'cluster']]

original_counts = df.groupby('cluster', as_index=False).agg('count').sort_values('protein', ascending=False)

annot_clusters_outer = df.merge(annot_kegg, on='protein', how='outer').drop_duplicates()
annot_clusters_inner = df.merge(annot_kegg, on='protein', how='inner').drop_duplicates()

annot_clusters_counts_nans = annot_clusters_outer\
.groupby(['cluster'], as_index=False)\
.agg({'id': lambda x: x.isnull().sum()})\
.rename(columns={'id':'count'})\
.sort_values('count', ascending=False)

annot_clusters_counts = annot_clusters_inner\
.groupby(['cluster'], as_index=False)\
.agg({'protein':'count'})\
.rename(columns={'protein':'count'})\
.sort_values('count', ascending=False)


annotation_stats = annot_clusters_counts.merge(annot_clusters_counts_nans, on='cluster')\
.rename(columns={'count_x':'amount_of_annotated',
                'count_y':'amount_of_unannotated'})\
.sort_values('amount_of_unannotated', ascending=False)




print ("total number of clusters =", df.cluster.nunique())
print ("original number of clusters with >=2 proteins =", original_counts[original_counts['protein']>=2].shape[0])
print ("out of all clusters:")
print ("number of clusters with all proteins not annotated =", annot_clusters_outer.cluster.nunique()-annot_clusters_inner.cluster.nunique())
print ("number of cluster with at least 1 annotated protein =", annot_clusters_inner.cluster.nunique())
print ("out of these:")
print ("number of annotated clusters with only 1 protein =", annot_clusters_counts[annot_clusters_counts['count']==1].shape[0])
print ("number of annotated clusters with >=2 proteins =", annot_clusters_counts[annot_clusters_counts['count']>=2].shape[0])



# print ("/n")
# print ("BACTERIA")

# db = 'kegg'
# ### Loop the data lines
# with open("/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/bacteria_annot_"+db+".tsv", 'r') as temp_f:
#     # get No of columns in each line
#     col_count = [ len(l.split("\t")) for l in temp_f.readlines() ]

# ### Generate column names  (names will be 0, 1, 2, ..., maximum columns - 1)
# column_names = [i for i in range(0, max(col_count))]

# ### Read csv
# df = pd.read_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/mantis/bacteria/bacteria_annot_'+db+'.tsv', delimiter="\t", names=column_names)
# df.dropna(axis=1, how='all', inplace=True)
# df.columns=['protein', 'id', 'description']
# annot_kegg = df

# dg = pd.read_csv('/work/projects/ecosystem_biology/archaea/coevolution/analysis/bac_gut_proteins_all_clusters.csv', sep='\t')

# original_counts = dg.groupby('cluster', as_index=False).agg('count').sort_values('protein', ascending=False)

# annot_clusters_outer = dg.merge(annot_kegg, on='protein', how='outer').drop_duplicates()
# annot_clusters_inner = dg.merge(annot_kegg, on='protein', how='inner').drop_duplicates()

# annot_clusters_counts_nans = annot_clusters_outer\
# .groupby(['cluster'], as_index=False)\
# .agg({'id': lambda x: x.isnull().sum()})\
# .rename(columns={'id':'count'})\
# .sort_values('count', ascending=False)

# annot_clusters_counts = annot_clusters_inner\
# .groupby(['cluster'], as_index=False)\
# .agg({'protein':'count'})\
# .rename(columns={'protein':'count'})\
# .sort_values('count', ascending=False)

# print ("total number of clusters =", dg.cluster.nunique())
# print ("original number of clusters with >=10 proteins =", original_counts[original_counts['protein']>=10].shape[0])
# print ("out of all clusters:")
# print ("number of clusters with all proteins not annotated =", annot_clusters_outer.cluster.nunique()-annot_clusters_inner.cluster.nunique())
# print ("number of cluster with at least 1 annotated protein =", annot_clusters_inner.cluster.nunique())
# print ("out of these:")
# print ("number of annotated clusters with only 1 protein =", annot_clusters_counts[annot_clusters_counts['count']==1].shape[0])
# print ("number of annotated clusters with >= 10 proteins =", annot_clusters_counts[annot_clusters_counts['count']>=10].shape[0])

