import pandas as pd
import numpy as np
import os
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy import stats
import natsort
from statsmodels.stats.multitest import multipletests
from pygenomeviz import Genbank, GenomeViz, load_dataset

# pd.set_option('display.max_colwidth', 500)
pd.set_option('display.max_colwidth', 0)

from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "last"

for i in range(10, 46):
    cluster_name = 'h'+ str(i)
    print (cluster_name)

    a = []

    dir = '/work/projects/archaeome/coevolution/synteny/homo/sco/'+cluster_name+'/'


    files = [filename for filename in os.listdir(dir) \
             if filename.startswith("df_")]

    for file in files:
#         print (file)
        name = file.split('.')[0].replace('df_', '')
#         print (dir+file)
        try:
            df = pd.read_csv(dir+file, sep='\t', header=None, names=['protein', 'start', 'end', 'direction'])
        except ValueError:
            continue
        else:
    #     print (df)
            dt = pd.DataFrame()
            dt['start'] = (df['start']-df.iloc[0][1])/(df['end']-df.iloc[0][1])[10]
            dt['start'] = round(dt['start']*100000)
            dt['end'] = (df['end']-df.iloc[0][1])/(df['end']-df.iloc[0][1])[10]
            dt['end'] = round(dt['end']*100000)
            dt['protein'] = df['protein']
            dt['direction'] = df['direction']
            dt = dt.astype(np.int64)
        #     print (dt)

            ko = pd.read_csv(dir+'KO_'+name+'.tsv', sep='\t',
                   header=None, names=['protein', 'id', 'description'])

            dp = dt.merge(ko[['protein', 'id']])

        #     print(dt)
        #     print (ko)
        #     print(dp)
        #     break 
            tmp = {"name": name, 
             "size": dp.iloc[10]['end'], 
             "cds_list": ((dp.iloc[0]['start'], dp.iloc[0]['end'], dp.iloc[0]['direction']), 
                         (dp.iloc[1]['start'], dp.iloc[1]['end'], dp.iloc[1]['direction']), 
                         (dp.iloc[2]['start'], dp.iloc[2]['end'], dp.iloc[2]['direction']),
                         (dp.iloc[3]['start'], dp.iloc[3]['end'], dp.iloc[3]['direction']),
                         (dp.iloc[4]['start'], dp.iloc[4]['end'], dp.iloc[4]['direction']),
                         (dp.iloc[5]['start'], dp.iloc[5]['end'], dp.iloc[5]['direction']),
                         (dp.iloc[6]['start'], dp.iloc[6]['end'], dp.iloc[6]['direction']),
                         (dp.iloc[7]['start'], dp.iloc[7]['end'], dp.iloc[7]['direction']),
                         (dp.iloc[8]['start'], dp.iloc[8]['end'], dp.iloc[8]['direction']),
                         (dp.iloc[9]['start'], dp.iloc[9]['end'], dp.iloc[9]['direction']),
                         (dp.iloc[10]['start'], dp.iloc[10]['end'], dp.iloc[10]['direction'])),
            "protein" : (dp.iloc[0]['protein'], dp.iloc[1]['protein'], dp.iloc[2]['protein'], dp.iloc[3]['protein'], 
                         dp.iloc[4]['protein'], dp.iloc[5]['protein'], dp.iloc[6]['protein'], dp.iloc[7]['protein'], 
                         dp.iloc[8]['protein'], dp.iloc[9]['protein'], dp.iloc[10]['protein']),
            "id" :      (dp.iloc[0]['id'], dp.iloc[1]['id'], dp.iloc[2]['id'], dp.iloc[3]['id'], 
                         dp.iloc[4]['id'], dp.iloc[5]['id'], dp.iloc[6]['id'], dp.iloc[7]['id'], 
                         dp.iloc[8]['id'], dp.iloc[9]['id'], dp.iloc[10]['id'])
            }


            a.append(tmp)

        genome_list = tuple(a)



        # original
        gv = GenomeViz(tick_style="axis")

        for genome in genome_list:
            name, size, cds_list, protein_name, ko_id = genome["name"], genome["size"], genome["cds_list"], genome["protein"], genome["id"]
            track = gv.add_feature_track(name, size)
        #     colors = ['darkblue', 'blue', 'lightblue', 'turquoise', 'teal', 'red', 'khaki', 'gold', 'goldenrod', 'orange', 'chocolate']
        #     colors = ['darkblue', 'blue', 'lightblue', 'turquoise', 'teal', 'gold', 'pink', 'salmon', 'orangered', 'red', 'crimson']
        #     colors = ['darkblue', 'blue', 'lightblue', 'turquoise', 'teal', 'gold', 'pink', 'plum', 'violet', 'orchid', 'purple']
            colors = ['darkblue', 'blue', 'lightblue', 'turquoise', 'teal', 'crimson', 'goldenrod', 'gold', 'orange', 'chocolate', 'sienna']
            for idx, cds in enumerate(cds_list, 0):
                if idx != 5:
                    start, end, strand = cds
                    track.add_feature(
                        start,
                        end,
                        strand,
                        label=ko_id[idx],
                        facecolor=colors[idx],
                        linewidth=1,
                        labelrotation=45,
                        labelvpos="top",
                        labelhpos="left",
                        labelha="left",
                        arrow_shaft_ratio=1,
                        size_ratio=0.6
                    )
                else:
                    start, end, strand = cds
                    track.add_feature(
                        start,
                        end,
                        strand,
                        plotstyle="bigarrow",
                        label=ko_id[idx],
                        facecolor=colors[idx],
                        labelcolor=colors[idx],
                        linewidth=1,
                        labelrotation=0,
                        labelvpos="bottom",
                        labelhpos="center",
                        labelha="center",
                        size_ratio=1
                    )


        fig = gv.plotfig()

        # Parent Directory path
        pics_dir = "/work/projects/archaeome/coevolution/synteny/homo/pics/"
        fig.savefig(pics_dir+'synteny_'+cluster_name+'.pdf')
