import urllib.request as request
import shutil
from contextlib import closing
import gzip
import re
import os
import subprocess
from pathlib import Path
import pandas as pd


def extract_clusters(clustering_file, outfile):
    repres = pd.read_csv(clustering_file, sep='\t', header=None, names=['rpr', 'protein'])
    repres.rpr = pd.Categorical(repres.rpr)
    repres['cluster'] = repres.rpr.cat.codes
    clusters = repres[['cluster', 'protein']].sort_values('cluster')
    cluster_counts = clusters.groupby('cluster', as_index=False).agg({'protein': 'count'}) \
        .rename(columns={'protein': 'count'}) \
        .sort_values('count', ascending=False)
    cluster_counts = cluster_counts[(cluster_counts['count'] >= 10)]
    clusters = cluster_counts.merge(clusters, on='cluster')
    instruction = clusters[['protein', 'cluster']]
    instruction['cluster'] = 'b_' + instruction['cluster'].astype(str)
    instruction.to_csv(outfile, header=False, index=False, sep='\t')


if __name__ == '__main__':
    clustering_file = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/bacteria_gut/clustering_0.9seqid_0.9c.tsv'
    save_result2 = '/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv'
    extract_clusters(clustering_file, save_result2)
