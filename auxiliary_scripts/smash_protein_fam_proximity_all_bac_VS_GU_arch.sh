#!/bin/bash -l
#SBATCH -J smash
#SBATCH -o smash.log
#SBATCH -N 4
#SBATCH -c 16
#SBATCH --time=48:00:00
#SBATCH -p batch

date


cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs

perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_bac_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_gut_bacteria.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins

rm unclustered_proteins



perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_arch_GU_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_unique_gut_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
rm unclustered_proteins


for f in *; do mv "$f" "$f".fasta; done




conda activate smash 

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/

mkdir -p $main_dir/sig
mkdir -p $main_dir/out

# create signatures
cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs
for f in *.fasta; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $main_dir/sig/$f.sig; done

# index 
sourmash index -k 31 $main_dir/sbt_08 $main_dir/sig/*.sig

# compare
sourmash compare -p 16 $main_dir/sig/*.sig -o $main_dir/out/08_cmp --csv $main_dir/out.csv


cd $main_dir

rm -r sig/
rm -r out/
