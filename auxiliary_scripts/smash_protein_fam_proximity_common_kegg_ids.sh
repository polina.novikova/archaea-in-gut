#!/bin/bash -l
#SBATCH -J smash
#SBATCH --time=14-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=long
#SBATCH -o smash_common_kegg_ids_%j.log
#SBATCH --begin=now
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=polina.novikova@uni.lu
#SBATCH -N 1
#SBATCH -n 88
#SBATCH --mem=1TB


conda activate smash

main_dir=/work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch


mkdir -p $main_dir/sig
mkdir -p $main_dir/out

# create signatures
cd /work/projects/coevolution/phylogeny/smash/all_bac_VS_GU_arch/PFs_rprs
for f in *.fasta; do echo $f ; sourmash compute --protein --input-is-protein -k 51 $f -o $main_dir/sig/$f.sig; done

# index                                                                                                                             
sourmash index -k 31 $main_dir/sbt_08 $main_dir/sig/*.sig
                                                                                                                                    
# compare                                                                                                                           
sourmash compare -p 16 $main_dir/sig/*.sig -o $main_dir/out/08_cmp --csv $main_dir/out.csv
                                                                                                                                    
                                                                                                                                    
cd $main_dir                                                                                                                        
                                                                                                                        
