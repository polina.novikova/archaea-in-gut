#!/bin/bash -l 
#SBATCH -J sort_arch
#SBATCH -o sort_arch.log
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --time=24:00:00
#SBATCH -p batch


sed 's/\s.*$//' /work/projects/ecosystem_biology/archaea/coevolution/analysis/mmseqs2/archaea_gut/clustering_0.9seqid_0.9c.fasta | awk '$0==b{next}{b=$0;print}' | sed 's/*//g' > /work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_unique_gut_archaea.fasta

cd work/projects/ecosystem_biology/archaea/coevolution/analysis/phylogenetic_analysis/input/archaea/PFs_unique_gut_archaea

perl -we '
    open my $instruction, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/instructions_arch_GU_clusters.tsv" or die $!;
    my %where = map split, <$instruction>;
    open my $contents, "<", "/work/projects/ecosystem_biology/archaea/coevolution/analysis/intermediate_results/all_proteins_unique_gut_archaea.fasta" or die $!;
    my $out;
    my $unknown = "file_unknown";
    my %created;
    while (<$contents>) {
        open $out, $created{ $where{$1} // $unknown }++ ? ">>" : ">",
             $where{$1} // $unknown if /^>(.*)/;
        print {$out} $_;
    }' 
mv file_unknown unclustered_proteins
for f in *; do mv "$f" "$f".fasta; done

mkdir unclustered
mv unclustered_proteins.fasta unclustered
